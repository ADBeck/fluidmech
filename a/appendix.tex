\appendix

\renewcommand{\lastedityear}{2021}
 \renewcommand{\lasteditmonth}{02}
   \renewcommand{\lasteditday}{12}


{\newgeometry{inner=3cm, outer=3cm, top=2cm, bottom=2cm}
\thispagestyle{empty}
	\phantomsection
	\addcontentsline{toc}{chapter}{Appendix}

	\vspace*{\stretch{1}}
			{\center \normalsize \QuiteHuge Appendix\par}
			
			{\center \footnotesize last edited \lasteditdate\par}

			\setcounter{mtc}{14} % Help minitoc find the right file			
			\uneminitoc
	\vspace*{\stretch{1}}\restoregeometry
}
\clearpage

\renewcommand\thesection{A\arabic{section}}
\renewcommand{\theequation}{A/\arabic{equation}}
\renewcommand{\thefigure}{A.\arabic{figure}}
\setcounter{equation}{0}
\setcounter{figure}{0}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Notation}


	\TabPositions{2cm}


	\begin{description}
		\item[$\equiv$] 	\tab By definition. The $\equiv$ symbol sets the definition of the term on its left (which does not depend on previous equations).
		\item[$\dot~$]		\tab (dot above symbol) Time rate: $\dot~ \equiv \frac{\diff}{\diff t}$. For example, $\dot Q$ is the rate of heat (in \si{watts}) representing a heat quantity $Q$ (in \si{joules}) every \si{second}.
		\item[$\overline~$] \tab (bar above symbol) Time-average: $\overline A \equiv \text{avg}(A) = \text{avg}(\overline A + A')$. The prime symbol indicates the instantaneous fluctuation around the average.
		\item[$\Delta$]		\tab Indicates a net difference between two values: $(\Delta X)_\fromatob \equiv X_\B - X_\A$. Can be negative.
		\item[italics] 		Physical properties (\eg mass $m$, temperature $T$).
		\item[straight subscripts]%
							Points in space or in time (temperature~$T_\A$ at point~A).\\
							Subscripts “cst” indicate a constant property, “in” indicates “incoming” and “out” is “outgoing”.\\
							Subscript “av.” indicates “average”.
		\item[operators]%
							Differential $\diff$, partial differential $\partial$, finite differential $\delta$, total (alt.: substantial) derivative $\text{D}/\text{D}t$ (def. eq.~\ref{eq_totaltimederivative} p.~\pageref{eq_totaltimederivative}), exponential $\exp x \equiv e^x $, natural logarithm $\ln x \equiv \log_e x$.
		\item[vectors]		Vectors are written with an arrow. Velocity is $\vec V \equiv (u, v, w)$, alternatively written $u_i \equiv (u, v, w)$. The norm of a vector $\vec A$ (positive or negative) is $|\vec A|$, its length (always positive) is $||\vec A||$.
		\item[vector calculus] ~\\
							Dot product $\vec A \cdot \vec B$ (see \S\ref{ch_vector_dot_product} p.~\pageref{ch_vector_dot_product});\\
							Cross product $\vec A \wedge \vec B$ (see \S\ref{ch_vector_cross_product} p.~\pageref{ch_vector_cross_product});\\
							Gradient $\gradient{A}$ (def. eq.~\ref{eq_def_gradient} p.~\pageref{eq_def_gradient}, see also \S\ref{ch_appendix_gradient} p.~\pageref{ch_appendix_gradient});\\
							Divergent $\divergent{\vec A}$ (def. eq.~\ref{eq_def_divergent} p.~\pageref{eq_def_divergent}, see also \S\ref{ch_appendix_divergent} p.~\pageref{ch_appendix_divergent});\\
							Laplacian $\laplacian{\vec A}$ (def. eq.~\ref{eq_def_laplacian} p.~\pageref{eq_def_laplacian}, see also \S\ref{ch_appendix_laplacian} p.~\pageref{ch_appendix_laplacian});\\
							Curl $\curl{\vec A}$ (def. eq.~\ref{eq_def_curl} p.~\pageref{eq_def_curl}, see also \S\ref{ch_appendix_curl} p.~\pageref{ch_appendix_curl}).
		\item[units]		Units are typed in roman (normal) font and colored gray (\SI{1}{\kilogram}). In sentences units are fully-spelled and conjugated (one hundred \si{watts}). The \si{liter} is noted \si{\liter} to increase readability ($\SI{1}{\liter} \equiv \SI{e-3}{\metre\cubed}$). Units in equations are those from \textit{système international} (\textsc{si}) unless otherwise indicated.
		\item[numbers]		The decimal separator is a comma, the decimal exponent is preceded by a dot, integers are written in groups of three ($\SI{1,234e3} ~=~ \num{1234}$). Numbers are rounded up as late as possible and never in series. Leading and trailing zeros are never indicated.
	\end{description}



\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Vector operations}
\label{appendix_vector_operations}

For a step-by-step revision of those notions and many more, written in a progressive, nonjudgmental way, with plenty of worked-out exercises, you can try John Bird’s Higher Engineering Mathematics~\cite{bird2004higher}.


\subsection{Vector dot product}
\label{ch_vector_dot_product}

		The \vocab{dot product}\wupen{Dot product} of two vectors is \textbf{a number} defined as:
		\begin{equation}
			\vec a \cdot \vec b = |\vec a| \ |\vec b| \ \cos \theta
			\label{eq_def_scalar_product}
		\end{equation}

		\begin{description}
			\item where $\theta$ is the angle separating the two vectors $\vec a$ and $\vec b$.		
		\end{description}
		
		\begin{figure}
			\begin{center}
				\includegraphics[width=8cm]{images/produit_scalaire}
			\end{center}
			\caption{Two vectors $\vec a$ and $\vec b$.}
		\end{figure}
		
		In this document, the dot product is always written with a median dot ($\vec a \cdot \vec b$), but in other literature, it is sometimes written with the $\times$ symbol. Take care not to confuse it with the \vocab{vector cross product} (see \S\ref{ch_vector_cross_product} p.~\pageref{ch_vector_cross_product}).
		
		It can be shown that the dot product of two vectors $\vec a \{x_a, y_a, z_a\}$ and $\vec b \{x_b, y_b, z_b\}$ can be quantified as:
		\begin{equation}
			\vec a \cdot \vec b = x_a x_b + y_a y_b + z_a z_b
			\label{eq_quantification_produit_scalaire}
		\end{equation}
		
		The dot product of two vectors is the same regardless of the order in which they are multiplied:
		\begin{equation}
			\vec a \cdot \vec b = \vec b \cdot \vec a
		\end{equation}
		
		Its is easily shown using \cref{eq_def_scalar_product} that:		
		\begin{equation}
			( \lvec a \cdot \lvec{-b}) = - (\vec a \cdot \vec b)
		\end{equation}


\subsection{Vector cross product}
\label{ch_vector_cross_product}

	The \vocab{cross product}\wupen{Cross product} of two vectors is \textbf{a vector} written as:
		\begin{equation}
			\vec a \wedge \vec b = \vec c
		\end{equation}

		The vector $\vec c$ is so that:
		
		\begin{description}
			\item[its length] is equal to
				\begin{equation}
					c = a \ b \ \sin \theta
				\end{equation}
			\item[its direction] is perpendicular to $\vec a$ and $\vec b$;
			\item[its orientation] is so that if $\vec b$ is positioned at the end of $\vec a$, then $\vec c$ points away from a point from which the rotation generated $\vec b$ is in the clockwise direction.
		\end{description}
		
		\begin{figure}
			\begin{center}
				\includegraphics[width=8cm]{images/produit_vectoriel}
			\end{center}
			\caption{Two vectors $\vec a$ and $\vec b$. The vector product $\vec a \wedge \vec b$ has length the product of the lengths $b_\perp$ et $a$. In the case shown here, the vector $\vec c = \vec a \wedge \vec b$ is going into through the document plane, going away from the reader.}
		\end{figure}
		
		Describing $\vec c$ requires a third dimension, even if $\vec a$ et $\vec b$ have only two dimensions.
		
		In this document, the cross product is written with a wedge symbol ($\vec a \wedge \vec b$) but in the literature, it is often written with the symbol $\times$. Make sure you do not confuse it with the dot product (\S\ref{ch_vector_cross_product} p.~\pageref{ch_vector_cross_product}).
		
		It can be shown that the cross product $\vec c$ of two vectors $\vec a \{x_a, y_a, z_a\}$ et $\vec b \{x_b, y_b, z_b\}$ is:
		\begin{equation}
			\vec c = \begin{vmatrix}
					\vec i 	&\vec j	&\vec k		\\
					x_a	&y_a	&z_a		\\
					x_b	&y_b	&z_b		
				\end{vmatrix}
			\label{eq:coordonnées_produit_vectoriel}
		\end{equation}
		So that one obtains:
		\begin{IEEEeqnarray}{RcL}
			\vec c\  & =	&\begin{vmatrix}
						y_a	&z_a	\\
						y_b	&z_b	
					\end{vmatrix} \vec i \ \ - \ \ 
					\begin{vmatrix}
						x_a	&z_a	\\
						x_b	&z_b	
					\end{vmatrix} \vec j \ \ + \ \ 
					\begin{vmatrix}
						x_a	&y_a	\\
						x_b	&y_b	
					\end{vmatrix} \vec k \\
				& = 	&\  (y_a z_b - y_b z_a) \vec i \ \ - \ \ (x_a z_b - x_b z_a) \vec j \ \ + \ \ (x_a y_b - x_b y_a) \vec k
			\label{eq:détail_coordonnées_produit_vectoriel}
		\end{IEEEeqnarray}

	
	The two vectors $\vec a \wedge \vec b$ and $\vec b \wedge \vec a$ are pointing away one from the other (\cref{fig_produit_vectoriel_2}):
	\begin{equation}
		\vec b \wedge \vec a = -\ (\vec a \wedge \vec b)
	\end{equation}

	\begin{figure}
		\begin{center}
			\includegraphics[width=10cm]{images/produit_vectoriel_2a.png}
			\includegraphics[width=10cm]{images/produit_vectoriel_2b.png}
		\end{center}
		\caption{The vectors $\vec a \wedge \vec b$ and $\vec b \wedge \vec a$ have the same length but are pointing directions opposite one from the other (the first away from the reader, and the other towards the reader).}
		\label{fig_produit_vectoriel_2}
	\end{figure}
	
	If any vector changes direction, the cross product also changes direction (\cref{fig_produit_vectoriel_oppose}):
	\begin{equation}
		\vec a \wedge \lvec {-b} = -\ (\vec a \wedge \vec b)
	\end{equation}

	\begin{figure}
		\begin{center}
			\includegraphics[width=8cm]{images/produit_vectoriel_oppose.png}
		\end{center}
		\caption{The vector $\vec a \wedge \vec {-b}$ is pointing away from the vector $\vec a \wedge \vec b$.}
		\label{fig_produit_vectoriel_oppose}
	\end{figure}



\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Field operators}
\label{appendix_field_operators}

	Four operators which apply on vector or scalar fields are important in fluid mechanics: gradient, divergent, Laplacian and curl.

	\subsection{Gradient}
	\label{ch_appendix_gradient}

		The mathematical operator \vocab{gradient}\wupen{Gradient} (first introduced as eq.~\ref{eq_def_gradient} p.~\pageref{eq_def_gradient}) is written~$\gradient{}$~. It applies on a scalar field and produces a vector field. It is defined~as:
		\begin{IEEEeqnarray}{rCcCl}
			\gradient{} 	&\equiv&  \vec i \partialderivative{}{x} + \vec j \partialderivative{}{y} + \vec k \partialderivative{}{z}\\
			\gradient{A} 	&\equiv&  \partialderivative{A}{x} \vec i + \partialderivative{A}{y} \vec j + \partialderivative{A}{z} \vec k %
							&=&	\left(\begin{array}{c}%
											\partialderivative{A}{x}\\
											\partialderivative{A}{y}\\
											\partialderivative{A}{z}\\
									\end{array}\right)
		\end{IEEEeqnarray}

		For example, the gradient of a pressure field is the vector field $\gradient{p}$:
		\begin{IEEEeqnarray}{rCcCl}
			\gradient{p} &\equiv&  \partialderivative{p}{x} \vec i + \partialderivative{p}{y} \vec j + \partialderivative{p}{z} \vec k %
							&=&	\left(\begin{array}{c}%
											\partialderivative{p}{x}\\
											\partialderivative{p}{y}\\
											\partialderivative{p}{z}\\
									\end{array}\right)
		\end{IEEEeqnarray}
		
	\subsection{Divergent}
	\label{ch_appendix_divergent}

		The mathematical operator \vocab{divergent}\wupen{Divergence} (first introduced as eq.~\ref{eq_def_divergent} p.~\pageref{eq_def_divergent}) is written~$\divergent{}$~ and is defined~as:
		\begin{IEEEeqnarray}{rCl}
			\divergent{}	&\equiv& \partialderivative{}{x} \vec i \cdot \ + \ \partialderivative{}{y} \vec j \cdot \ + \ \partialderivative{}{z} \vec k \cdot
		\end{IEEEeqnarray}
		When applied on a vector field, it produces a scalar field:
		\begin{IEEEeqnarray}{rCl}
			\divergent{\vec A}	& \equiv & \partialderivative{}{x} \vec i \cdot \vec A \ + \ \partialderivative{}{y} \vec j \cdot \vec A \ + \ \partialderivative{}{z} \vec k \cdot \vec A\\
			&=& \partialderivative{A_x}{x} \ + \ \partialderivative{A_y}{y} \ + \ \partialderivative{A_z}{z}
		\end{IEEEeqnarray}
		When applied on a 2\up{nd} order tensor field, it produces a vector field:
		\begin{IEEEeqnarray}{rCcCl}
			\divergent{\vec A_{ij}}	&\equiv& \left(\begin{array}{c}%
					\partialderivative{A_{xx}}{x} + \partialderivative{A_{yx}}{y} + \partialderivative{A_{zx}}{z}\\
					\partialderivative{A_{xy}}{x} + \partialderivative{A_{yy}}{y} + \partialderivative{A_{zy}}{z}\\
					\partialderivative{A_{xz}}{x} + \partialderivative{A_{yz}}{y} + \partialderivative{A_{zz}}{z}\\
				\end{array}\right) &=&%
					\left(\begin{array}{c}%
						\divergent{\vec A_{ix}}\\
						\divergent{\vec A_{iy}}\\
						\divergent{\vec A_{iz}}\\
				\end{array}\right)
		\end{IEEEeqnarray}
		
		For example, the divergent of a velocity field is the scalar field $\divergent{\vec V}$:
		\begin{IEEEeqnarray}{rCl}
			\divergent{\vec V}	&\equiv& \partialderivative{u}{x} \ + \ \partialderivative{v}{y} \ + \ \partialderivative{w}{z}\label{eq_appendix_divergent_velocity}
		\end{IEEEeqnarray}
		
	\subsection{Advective}
	\label{ch_appendix_advective}
		The \vocab{advective operator},\wupen{Material derivative}  $\advective$ is defined as follows:
				\begin{IEEEeqnarray}{rCl}
					\vec V \cdot \vec \nabla & \equiv & u \partialderivative{}{x} + v \partialderivative{}{y} + w \partialderivative{}{z}
				\end{IEEEeqnarray}
				
		Do not confuse the advective operator with the divergent of velocity, $\divergent{\vec V}$ (see Appendix~\ref{ch_appendix_divergent} above, including eq.~\ref{eq_appendix_divergent_velocity}), which is a scalar field.

		The advective operator can be applied to a scalar field $A$:
			\eq{
				\advective{A} & = & u \partialderivative{A}{x} + v \partialderivative{A}{y} + w \partialderivative{A}{z}
			}
		
		It can also be applied to a vector field $\vec A$:
			\eq{
				\advective{\vec A} &=& u \partialderivative{\vec A}{x} + v \partialderivative{\vec A}{y} + w \partialderivative{\vec A}{z}\\
					&=& \left(\begin{array}{c}%
						u \partialderivative{A_x}{x} + v \partialderivative{A_x}{y} + w \partialderivative{A_x}{z}\\
						u \partialderivative{A_y}{x} + v \partialderivative{A_y}{y} + w \partialderivative{A_y}{z}\\
						u \partialderivative{A_z}{x} + v \partialderivative{A_z}{y} + w \partialderivative{A_z}{z}
						\end{array}\right)
				}


	\subsection{Laplacian}
	\label{ch_appendix_laplacian}

		The mathematical operator \vocab{Laplacian}\wupen{Laplace operator}\wupen{Vector Laplacian} (first introduced as eq.~\ref{eq_def_laplacian} p.~\pageref{eq_def_laplacian}) is written~$\laplacian{}$ and defined~as:
			\begin{IEEEeqnarray}{rCl}
				\laplacian{} &\equiv& \divergent{\gradient{}}\label{eq_def_laplacian_appendix}
			\end{IEEEeqnarray}
			
		When applied to a scalar field, it is equal to the divergent of the gradient of the field, and produces a scalar field:
			\begin{IEEEeqnarray}{rCl}
				\laplacian{A} 	&\equiv& \divergent{\gradient{A}}\\
								&=& \secondpartialderivative{A}{x} + \secondpartialderivative{A}{y} + \secondpartialderivative{A}{z}
		\end{IEEEeqnarray}
		
		When applied to a vector field, the general expression uses the curl operator (we never use this expression in this course), and produces a vector field:
		\eq{
			 \laplacian{\vec A} &\equiv& \left(\divergent{\gradient{}}\right) \vec A - \curl{\left(\curl{\vec A}\right)}
		}
		
		In Cartesian coordinates, this simplifies as:
		\begin{IEEEeqnarray}{rCl}
			 \laplacian{\vec A} = &\equiv& %
										\left(\begin{array}{c}%
											\laplacian{A_x}\\
											\laplacian{A_y}\\
											\laplacian{A_z}\\
									\end{array}\right) \ = \
										\left(\begin{array}{c}%
											\divergent{\gradient{A_x}}\\
											\divergent{\gradient{A_y}}\\
											\divergent{\gradient{A_z}}\\
									\end{array}\right)\\
								&=& \left(\begin{array}{c}%
											\secondpartialderivative{A_x}{x} + \secondpartialderivative{A_x}{y} + \secondpartialderivative{A_x}{z}\\
											\secondpartialderivative{A_y}{x} + \secondpartialderivative{A_y}{y} + \secondpartialderivative{A_y}{z}\\
											\secondpartialderivative{A_z}{x} + \secondpartialderivative{A_z}{y} + \secondpartialderivative{A_z}{z}\\
									\end{array}\right)
		\end{IEEEeqnarray}
		
		For example, the Laplacian of a velocity field is the vector field $\laplacian{\vec V}$:
			\begin{IEEEeqnarray}{rCcCl}
				 \laplacian{\vec V} &\equiv& %
											\left(\begin{array}{c}%
												\laplacian{u}\\
												\laplacian{v}\\
												\laplacian{w}\\
										\end{array}\right)
									&=& \left(\begin{array}{c}%
											\secondpartialderivative{u}{x} + \secondpartialderivative{u}{y} + \secondpartialderivative{u}{z}\\
											\secondpartialderivative{v}{x} + \secondpartialderivative{v}{y} + \secondpartialderivative{v}{z}\\
											\secondpartialderivative{w}{x} + \secondpartialderivative{w}{y} + \secondpartialderivative{w}{z}\\
									\end{array}\right)
		\end{IEEEeqnarray}
		


	\subsection{Curl}
	\label{ch_appendix_curl}

	The mathematical operator \vocab{curl}\wupen{Curl (mathematics)} (sometimes named \vocab{rotational}) is written~$\curl{}$~. It applies to a vector field and produces a vector field. It is defined~as:
		\begin{IEEEeqnarray}{rCCCl}
			\curl{} &\equiv& \begin{vmatrix}
					\vec i 	&\vec j	&\vec k		\\
					\partialderivative{}{x}	&\partialderivative{}{y}	&\partialderivative{}{z}		\\
						&	& 				\end{vmatrix}\\
			\curl{\vec A} &\equiv& \begin{vmatrix}
					\vec i 	&\vec j	&\vec k		\\
					\partialderivative{}{x}	&\partialderivative{}{y}	&\partialderivative{}{z}		\\
					A_x	&A_y	&A_z 				\end{vmatrix} &=& \left(\partialderivative{A_z}{y} - \partialderivative{A_y}{z}\right) \vec i \ \ + \ \ \left(-\partialderivative{A_z}{x} + \partialderivative{A_x}{z}\right) \vec j \ \ + \ \ \left(\partialderivative{A_y}{x} - \partialderivative{A_x}{y}\right) \vec k \nonumber\\\label{eq_def_curl}
		\end{IEEEeqnarray}

	For example, the curl of velocity is the vector field $\curl{\vec V}$:
		\begin{IEEEeqnarray}{rCCCl}
			\curl{\vec V} &=& \begin{vmatrix}
					\vec i 	&\vec j	&\vec k		\\
					\partialderivative{}{x}	&\partialderivative{}{y}	&\partialderivative{}{z}		\\
					u	&v	&w 				\end{vmatrix} &=& \left(\partialderivative{w}{y} - \partialderivative{v}{z}\right) \vec i \ \ + \ \ \left(-\partialderivative{w}{x} + \partialderivative{u}{z}\right) \vec j \ \ + \ \ \left(\partialderivative{v}{x} - \partialderivative{u}{y}\right) \vec k \nonumber\\
		\end{IEEEeqnarray}




\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Derivations of the Bernoulli equation}
\label{appendix_bernoulli}

	\subsection{The Bernoulli equation from the energy equation}
	\label{appendix_benoulli_energy}
	
		This is covered in section~\ref{ch_bernoulli} p.~\pageref{ch_bernoulli}.
	
	\subsection{The Bernoulli equation from the integral momentum equation}
	
		We begin with the integral linear momentum equation (eq.~\ref{eq_rtt_linearmom} p.~\pageref{eq_rtt_linearmom}):
		\begin{IEEEeqnarray*}{rCl}
			\vec F_\net & = & \timederivative{} \iiint_\cv \rho \vec V \diff \vol  + \iint_\cs \rho \vec V \ (\vec V_\rel \cdot \vec n) \diff A
		\end{IEEEeqnarray*}
		
		When considering a fixed, infinitely short control volume along a known streamline $s$ of the flow, this equation becomes:
		\begin{IEEEeqnarray*}{rCl}
			\diff \vec F_\text{pressure} + \diff \vec F_\text{shear} + \diff \vec F_\text{gravity} & = & \timederivative{} \iiint_\cv \rho \vec V \diff \vol  + \rho V A \diff \vec V
		\end{IEEEeqnarray*}
		\begin{equationterms}
			\item along a streamline, where the velocity $\vec V$ is aligned (by definition) with the streamline.
		\end{equationterms}

		Now, adding the restrictions of steady flow ($\diff/\diff{t}=0$) and no friction ($\diff \vec F_\text{shear} = \vec 0$), we already obtain:
		\begin{IEEEeqnarray*}{rCl}
			\diff \vec F_\text{pressure} + \diff \vec F_\text{gravity} & = &  \rho V A \diff \vec V
		\end{IEEEeqnarray*}
		
		The projection of the net force due to gravity $\diff \vec F_\text{gravity}$ on the streamline segment $\diff s$ has norm $\diff \vec F_\text{gravity} \cdot \diff \vec{s} = -g \rho A \diff z$, while the net force due to pressure is aligned with the streamline and has norm $\diff F_{\text{pressure}, s} = - A \diff p$. Along this streamline, we thus have the following scalar equation, which we integrate from points~1 to~2:
		\begin{IEEEeqnarray*}{rCl}
			 - A \diff p - \rho g A \diff z & = &  \rho V A \diff V \nonumber\\
			 -\frac{1}{\rho} \diff p - g \diff z & = &  V \diff V \nonumber\\
			 -\int_1^2 \frac{1}{\rho} \diff p - \int_1^2 g \diff z & = &  \int_1^2 V \diff V
		\end{IEEEeqnarray*}
		
		The last obstacle is removed when we consider flows without heat or work transfer, where, therefore, the density $\rho$ is constant. In this way, we arrive to equation.~\ref{eq_bernoulli} p.~\pageref{eq_bernoulli} again:
		\begin{IEEEeqnarray*}{rCl}
			\frac{p_1}{\rho} + \frac{1}{2} V_1^2 + g z_1 & = & \frac{p_2}{\rho} + \frac{1}{2} V_2^2 + g z_2\\
			\left(p + \frac{1}{2} \rho V^2 + \rho g z \right)_1 &=& \left(p + \frac{1}{2} \rho V^2 + \rho g z \right)_2
		\end{IEEEeqnarray*}

	\subsection{The Bernoulli equation from the Navier-Stokes equation}
	\label{appendix_benoulli_navier_stokes}
	
			We start by following a particle along its path in an arbitrary flow, as displayed in \cref{fig_integration_navierstokes_bernoulli}. The particle path is known (condition 5 in \S\ref{ch_bernoulli} p.~\pageref{ch_bernoulli}), but its speed $V$ is not.
				\begin{figure}
					\begin{center}
						\includegraphics[width=8cm]{integration_navierstokes_bernoulli}
					\end{center}
					\supercaption{Different pathlines in an arbitrary flow. We follow one particle as it travels from point 1 to point 2. An infinitesimal path segment is named $\diff \vec s$.}{\wcfile{Integration from Navier-Stokes to Bernoulli along path.svg}{Figure} \cczero \oc}
					\label{fig_integration_navierstokes_bernoulli}
				\end{figure}

			We are now going to project every component of the Navier-Stokes equation (eq.~\ref{eq_navierstokes} p.~\pageref{eq_navierstokes}) onto an infinitesimal portion of trajectory $\diff \vec s$.  Once all terms have been projected, the Navier-Stokes equation becomes a scalar equation:
			\begin{IEEEeqnarray}{rCl}
						\rho \partialtimederivative{\vec V} + \rho \advective{\vec V}  & = &	\rho \vec g - \gradient{p} + \mu \laplacian{\vec V}\nonumber\\
						\rho \partialtimederivative{\vec V} \cdot \diff \vec s + \rho \advective{\vec V} \cdot \diff \vec s  & = &	\rho \vec g \cdot \diff \vec s - \gradient{p} \cdot \diff \vec s + \mu \laplacian{\vec V} \cdot \diff \vec s \nonumber
			\end{IEEEeqnarray}
			
			Because the velocity vector $\vec V$ of the particle, by definition, is always aligned with the path, its projection is always equal to its norm: $\vec V \cdot \diff \vec s = V \diff s$. Also, the downward gravity $g$ and the upward altitude $z$ have opposite signs, so that $\vec g \cdot \diff \vec s = - g \diff z$; we thus obtain:
			\begin{IEEEeqnarray*}{rCl}
				\rho \partialtimederivative{V} \diff s + \rho \derivative{V}{s} V \diff s  & = & -\rho g \diff z - \derivative{p}{s} \diff s + \mu \laplacian{\vec V} \cdot \diff \vec s
			\end{IEEEeqnarray*}
			
			When we restrict ourselves to steady flow (condition 1 in \S\ref{ch_bernoulli}), the first left-hand term vanishes. Neglecting losses to friction (condition 4) alleviates us from the last right-hand term, and we obtain:
			\begin{IEEEeqnarray*}{rCl}
				\rho \derivative{V}{s} V \diff s  & = &	- \rho g \diff z - \derivative{p}{s} \diff s\\
				\rho V \diff V  & = & - \rho g \diff z - \diff p
			\end{IEEEeqnarray*}
		
			This equation can then be integrated from point 1 to point 2 along the pathline:
			\begin{IEEEeqnarray}{rCl}
				\rho \int_1^2 V \diff V  & = &	- \int_1^2 \rho g \diff z - \int_1^2 \diff p\nonumber
			\end{IEEEeqnarray}
			
			When no work or heat transfer occurs (condition 3) and the flow remains incompressible (condition 2), the density $\rho$ remains constant, so that we indeed have returned to eq.~\ref{eq_bernoulli} p.~\pageref{eq_bernoulli}:
			\begin{IEEEeqnarray}{rCl}
				\Delta \left(\frac{1}{2} V^2\right) + g \Delta z + \frac{1}{\rho} \Delta p &=& 0 \\
				\left(p + \frac{1}{2} \rho V^2 + \rho g z \right)_1 &=& \left(p + \frac{1}{2} \rho V^2 + \rho g z \right)_2
			\end{IEEEeqnarray}
			
			Thus, we can see that if we follow a particle along its path, in a steady, incompressible, frictionless flow with no heat or work transfer, its change in kinetic energy is due only to the result of gravity and pressure, in accordance with the Navier-Stokes equation.

\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Flow parameters as force ratios}
\label{appendix_flow_parameters}
\coveredin{Massey \cite{massey1983}}

	Instead of the mathematical approach covered in \S\ref{ch_non_dim_ns} p.~\pageref{ch_non_dim_ns}, the concept of \vocab{flow parameter} can be approached by \emph{comparing forces} in fluid flows.
	
	Fundamentally, understanding the movement of fluids requires applying Newton’s second law of motion: the sum of forces which act upon a fluid particle is equal to its mass times its acceleration. We have done this in an aggregated manner with integral analysis (in \chapterthreeshort, eq.~\ref{eq_rtt_linearmom} p.~\pageref{eq_rtt_linearmom}), and then in a precise and all-encompassing way with differential analysis (in \chaptersixshort, eq.~\ref{eq_navierstokes} p.~\pageref{eq_navierstokes}). With the latter method, we obtain complex mathematics suitable for numerical implementation, but it remains difficult to obtain rapidly a quantitative measure for what is happening in any given flow.
	
	In order to obtain this, an engineer or scientist can use force ratios. This involves comparing the magnitude of a type of force (pressure, viscous, gravity) either with another type of force, or with the mass-times-acceleration which a fluid particle is subjected to as it travels. We are not interested in the absolute value of the resulting ratios, but rather, in having a measure of the parameters that influence them, and being able to compare them across experiments.
	
	\subsection[Acceleration vs. viscous forces: the Reynolds number]{Acceleration vs. viscous forces:\\ the Reynolds number}
	
		The net sum of forces acting on a particle is equal to its mass times its acceleration. If a representative length for the particle is $L$, the particle mass grows proportionally to the product of its density $\rho$ and its volume $L^3$. Meanwhile, its acceleration relates how much its velocity $V$ will change over a time interval~$\Delta t$: it may be expressed as a ratio $\Delta V/ \Delta t$. In turn, the time interval~$\Delta t$ may be expressed as the representative length $L$ divided by the velocity $V$, so that the acceleration may be represented as proportional to the ratio $V \Delta V/L$. Thus we obtain:
			\begin{IEEEeqnarray*}{rCl}
				|\text{net force}| = |\text{mass} \times \text{acceleration}| &\sim& \rho L^3 \frac{V \Delta V}{L}\\
				|\vec{F}_\net| &\sim& \rho L^2 V \Delta V
			\end{IEEEeqnarray*}
		
		We now observe the viscous force acting on a particle: it is proportional to the shear effort and a representative acting surface $L^2$. The shear can be modeled as proportional to the viscosity $\mu$ and the rate of strain, which will grow proportionally to $\Delta V/L$. We thus obtain a crude measure for the magnitude of the shear force:
			\begin{IEEEeqnarray*}{rCl}
				|\text{viscous force}| &\sim& \mu \frac{\Delta V}{L} L^2\\
				|\vec{F}_\text{viscous}| &\sim& \mu \Delta V L
			\end{IEEEeqnarray*}
		
		The magnitude of the viscous force can now be compared to the net force:
			\begin{IEEEeqnarray}{rCl}
				\frac{|\text{net force}|}{|\text{viscous force}|} &\sim& \frac{\rho L^2 V \Delta V}{\mu \Delta V L} = \frac{\rho V L}{\mu} = \re \label{eq_re_forces}
			\end{IEEEeqnarray}
		and we recognize the ratio as the Reynolds number (\ref{eq_def_re} p.~\pageref{eq_def_re}). We thus see that the Reynolds number can be interpreted as the inverse of the influence of viscosity. The larger $\re$ is, and the smaller the influence of the viscous forces will be on the trajectory of fluid particles.


	\subsection{Acceleration vs. gravity force: the Froude number}
		
		The weight of a fluid particle is equal to its mass, which grows with $\rho L^3$, multiplied by gravity $g$:
			\begin{IEEEeqnarray*}{rCl}
				|\text{weight force}| = |\vec{F}_\text{W}| &\sim& \rho L^3 g
			\end{IEEEeqnarray*}
		
		The magnitude of this force can now be compared to the net force:
			\begin{IEEEeqnarray}{rCl}
				\frac{|\text{net force}|}{|\text{weight force}|} &\sim& \frac{\rho L^2 V^2}{\rho L^3 g} = \frac{V^2}{L g} = \fr^2
			\end{IEEEeqnarray}
		and here we recognize the square of the Froude number (\ref{eq_def_fr} p.~\pageref{eq_def_fr}). We thus see that the Froude number can be interpreted as the inverse of the influence of weight on the flow. The larger $\fr$ is, and the smaller the influence of gravity will be on the trajectory of fluid particles.


	\subsection{Acceleration vs. elastic forces: the Mach number}
	\label{ch_mach_number_force_ratios}
		
		In some flows called \vocab{compressible flows} the fluid can perform work on itself, and and the fluid particles then store and retrieve energy in the form of changes in their own volume. In such cases, fluid particles are subject to an \vocab{elastic force} in addition to the other forces. We can model the pressure resulting from this force as proportional to the bulk modulus of elasticity $K$ of the fluid (formally defined as $K \equiv \rho \ \partial{p}/\partial{\rho}$); the elastic force can therefore be modeled as proportional to $K L^2$:
			\begin{IEEEeqnarray*}{rCl}
				|\text{elasticity force}| = |\vec{F}_\text{elastic}| &\sim& K L^2
			\end{IEEEeqnarray*}
		
		The magnitude of this force can now be compared to the net force:
			\begin{IEEEeqnarray*}{rCl}
				\frac{|\text{net force}|}{|\text{elasticity force}|} &\sim& \frac{\rho L^2 V^2}{K L^2} = \frac{\rho V^2}{K}
			\end{IEEEeqnarray*}

		This ratio is known as the Cauchy number; it is not immediately useful because the value of $K$ in a given fluid varies considerably not only according to temperature, but also according to the type of compression undergone by the fluid: for example, it grows strongly during brutal compressions.
		
		During isentropic compressions and expansions (isentropic meaning that the process is fully reversible, \ie without losses to friction, and adiabatic, \ie without heat transfer), %
		%we will show in chapter~9 (with eq.~\ref{eq_speed_sound_elasticity_two} p.~\pageref{eq_speed_sound_elasticity_two}) 
		it can be shown that the bulk modulus of elasticity is proportional to the square of the speed of sound~$c$:
			\begin{IEEEeqnarray}{rCl}
				K|_\text{reversible} &=& c^2 \rho \label{eq_speed_sound_elasticity_one}
			\end{IEEEeqnarray}
		
		 The Cauchy number calibrated for isentropic evolutions is then
			\begin{IEEEeqnarray}{rCl}
				\frac{|\text{net force}|}{|\text{elasticity force}|_\text{reversible}} &\sim& \frac{\rho V^2}{K} = \frac{V^2}{c^2} = \ma^2
			\end{IEEEeqnarray}
		and here we recognize the square of the Mach number (\ref{eq_def_ma} p.~\pageref{eq_def_ma}). We thus see that the Mach number can be interpreted as the influence of elasticity on the flow. The larger $\ma$ is, and the smaller the influence of elastic forces will be on the trajectory of fluid particles.
		

	\subsection{Other force ratios}

		The same method can be applied to reach the definitions for the Strouhal and Euler numbers given in \S\ref{ch_scaling_flows} p.~\pageref{ch_scaling_flows}. Other numbers can also be used which relate forces that we have ignored in our study of fluid mechanics. For example, the relative importance of surface tension forces or of electromagnetic forces are quantified using similarly-constructed flow parameters.\\
		In some applications featuring rotative motion, such as flows in centrifugal pumps or planetary-scale atmospheric weather, it may be convenient to apply Newton’s second law in a rotating reference frame. This results in the appearance of new reference-frame forces, such as the Coriolis or centrifugal forces; their influence can then be studied using additional flow parameters.
		
		In none of those cases can flow parameters give enough information to predict solutions. They do, however, provide quantitative data to indicate which forces are relevant in which places: this not only helps us understand the mechanisms at work, but also distinguish the negligible from the influential, a key characteristic of efficient scientific and engineering work.



\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Details of the winter 2020-2021 final examination (updated February 2021)}
\label{ch_exam}

\youtubethumb{6vnK7-VsKXc}{The companion video to this exam briefing (2020)}{\oc (\ccby)}
The final examination for this course in the winter semester 2020-2021 is an \textbf{open-book, take-home exam} which will take place on February 18, 2021 from 14:00 to 16:00. The most important information is as follows:
\begin{itemize}
	%\item You must register through the university’s \textsc{lsf} system to attend the exam, well ahead of time.
	%\item The exam counts \SI{50}{\percent} towards your final mark. See the syllabus page~\pageref{ch_assessment} for details.
	\item This is a take-home exam: you may consult your notes, books, use software and resources offline or online.
	\item You must take this exam alone: you cannot interact with anyone online or offline during the exam.
	\item The exam lasts 90 minutes. Additionally, 30 minutes are provided for the the scanning and upload of your answer.
	\item You will receive the assignment per email shortly before the start. You must scan your answer with your student card on each page, and send it as a single-PDF file to fluidmech@ovgu.de using your academic email before the end of the exam.
	\item In the winter semester, this online examination grade is the only grade you receive in this course.
\end{itemize}

You can download the template of the exam at\\
\href{https://fluidmech.ninja/exams/exam\_20210218\_template.pdf}{https://fluidmech.ninja/exams/exam\_20210218\_template.pdf}.

\youtubethumb{GD0l_5XqVoc}{How to survive the examination (2020)}{\oc (\ccby)}


The complete list of examinable problems (unchanged from the 2020 exam) is as follows:
\begin{itemize}
	\item 2.2 2.3 2.4 2.5 2.6
	\item 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9
	\item 4.4 4.5 4.6\footnote{In exercise 4.6, only the calculation of the vertical force $F_\text{top}$ is examinable.} 4.7
	\item 5.2 5.3 5.4 5.5 5.6
	\item 6.2 6.3 6.4 6.5 6.6 6.7 6.9
	\item 7.3 7.4 7.5 7.6 7.7 7.8
	\item 8.2 8.4 8.5
	\item Chapter 9 is not examinable this semester
	\item 10.2 10.3 10.4 10.5 10.6
	\item Chapter 11 is not examinable this semester
\end{itemize}

Three problems will be given, all mandatory. The first problem (\SI{10}{pts} is exercise~\ref{exo_ns_revision} p.\pageref{exo_ns_revision}. The two other problems (\SI{45}{pts} each) are extracted from the list above, and modified slightly. Typically, the input data is changed, as well as the problem geometry. The method for solving the problems remains the same.

A formula sheet is provided. It is the sum of the preambles of every problem sheet in the lecture notes. It includes the Moody diagram and the viscosity diagram used in the problem sheets. You should definitely have a calculator with you, to facilitate calculations.

The criteria for grading your answers are:
\begin{itemize}
	\item You must show your work in all answers;
	\item Answers to questions starting with “show that” should be particularly well-developed and continuous;
	\item Illegible or ambiguous answers are always discarded.
\end{itemize}

Examinations from previous years, and their full solution, are available on the course website (\href{https://fluidmech.ninja/}{https://fluidmech.ninja/}). Since the course content has changed over time, you might find a few differences:
\begin{itemize}
	\item Problems involving calculating compressible air flow using tables are no longer examinable;
	\item A problem involving a ball fountain (“Kugel fountain”) is no longer examinable;
	\item Viscosity values were read in a different diagram, and may not match values read in the 2020 viscosity diagram.
\end{itemize}

You are welcome (and in fact encouraged!) to ask me questions of all sorts about the exam. You may contact me as described in the introduction, page~\pageref{ch_contact}. I wish you to have productive and joyful revisions!

Olivier\\
February 2021\\
(updated February 12, after cancellations following covid-19 restrictions)

\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Example of previous examinations}
\label{ch_previous_exam}

The following pages present the final examinations for this course in 2020 and 2021, and their full solutions.

\includepdf[pages=-,frame=true,nup=1x2,angle=270]{previousexam/exam_20190711.pdf}
\includepdf[pages=-,frame=true,nup=1x2,angle=270]{previousexam/exam_20190711_full_solution.pdf}
\includepdf[pages=-,frame=true,nup=1x2,angle=270]{previousexam/exam_20200921.pdf}
\includepdf[pages=-,frame=true,nup=1x2,angle=270]{previousexam/exam_20200921_full_solution.pdf}


\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{List of references}

	\mecafluboxen

	\printbibliography[heading=none]

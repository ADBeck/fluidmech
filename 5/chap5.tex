\renewcommand{\lastedityear}{2020}
 \renewcommand{\lasteditmonth}{09}
   \renewcommand{\lasteditday}{19}
\renewcommand{\numberofthischapter}{5}
\renewcommand{\titleofthischapter}{\namechapterfive}

\fluidmechchaptertitle
\label{chap_five}

\mecafluboxen

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Motivation}

	In fluid mechanics, only three types of forces apply to fluid particles: forces due to gravity, pressure, and shear. This chapter focuses on shear, and should allow us to answer two questions:
		\begin{itemize}
			\item How is the effect of shear described and quantified?
			\item What are the shear forces generated on walls by simple flows?
		\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Shear forces on walls}

	%%%%
	\subsection{Magnitude of the shear force}

		What is the force which which a fluid shears (\ie “rubs”) against a wall?
		
		When the shear $\tau$ exerted is uniform and the wall is flat, the resulting force $F$ in the direction $i$ is easily calculated:
			\begin{IEEEeqnarray}{rCcCl}
					F_{\text{shear, direction } i} & = & \tau_{\text{uniform, direction } i} \ S_\text{flat wall}
			\end{IEEEeqnarray}

		When the shear $\tau$ exerted by the fluid is not uniform (for example, because more friction is occurring on some parts of the surface than on others), the situation is more complex: the force must be obtained by integration. The surface is split in infinitesimal portions of area $\diff S$, and the corresponding forces are summed up as:
			\begin{IEEEeqnarray}{rCl}
					F_{\text{shear, direction } i} & = & \int_S \diff F_{\text{shear, direction } i}
			\end{IEEEeqnarray}
			
			\begin{mdframed}
				\eq{
					F_{\text{shear, direction } i} & = & \iint_S \tau_{\text{direction } i} \diff S \label{eq_shear_force_scalar}
				}
				\begin{equationterms}
					\item for a flat surface, 
					\item where the $S$-integral denotes an integration over the entire surface.
				\end{equationterms}
			\end{mdframed}

		What is required to calculate the scalar $F$ in eq.~\ref{eq_shear_force_scalar} is an expression of~$\tau$ as a function of~$S$. In a simple laminar flow, this expression will often be relatively easy to find, as we see later on. 

	%%%%
	\subsection{Direction and position of the shear force}
	
		The above equations work only for a flat surface, and in a chosen direction $i$. When we consider a two- or three-dimensional object immersed in a fluid with non-uniform shear, the integration must be carried out with vectors. We will not attempt this in this course, but the expression is worth writing out in order to understand how computational fluid dynamics (\cfd) software will proceed with the calculation.
		
		In a general case, the shear on any infinitesimal surface $\diff S$ needs to be expressed as a vector $\vec \tau_n$, where $n$ is the direction perpendicular to the surface. The net force due to shear on the surface is then:
		\begin{IEEEeqnarray}{rCcCl}
				\vec F_\text{shear} & = & \int_S \vec \tau_n \diff S  \label{eq_shear_force_vector}
		\end{IEEEeqnarray}
		

		Much like equation~\ref{eq_pressure_force_vector} in the previous chapter, eq.~\ref{eq_shear_force_vector} is not too hard to implement as a software algorithm to obtain numerically, for example, the force resulting from shear due to fluid flow around a body such as the body of a car. Its computation by hand, however, is far too tedious for us to even attempt.
		
		The position of the shear force is obtained with two moment vector equations, in a manner similar to that described in \S\ref{ch_position_pressure_force} p.~\pageref{ch_position_pressure_force} with pressure. This is outside of the scope of this course.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Shear fields in fluids}

		We approached the concept of shear in \chapterone with the notion that it represented force parallel to a given flat surface (eq.~\ref{eq_first_def_shear}), for example a flat plate of area $A$:
			\begin{IEEEeqnarray}{rCl}
				\tau &\equiv& \frac{F_\parallel}{A} \label{eq_first_def_shear_two}
			\end{IEEEeqnarray}
	
		Like we did with pressure, to appreciate the concept of shear in fluid mechanics, we need to go beyond this equation.


	%%%%
	\subsection{The direction of shear}

		\youtubethumb{LjWeYPEmCk8}{cloud movements in a time\--lapse video on an interesting day are evidence of a highly\--strained atmosphere: pilots and meteorologists refer to this as \vocab{wind shear}.}{Y:StormsFishingNMore (\styl)}
		Already from the definition in eq.~\ref{eq_first_def_shear_two} we can appreciate that “parallel to a flat plate” can mean a multitude of different directions, and so that we need more than one dimension to represent shear. Furthermore, much in the same way as we did for pressure, we do away with the flat plate and accept that shear is a \vocab{field}, \ie it is an effort applying not only upon solid objects but also upon and within fluids themselves. We replace eq.~\ref{eq_first_def_shear_two} with a more general definition:
		\begin{IEEEeqnarray}{rCl}
			\vec \tau &\equiv& \lim_{A \to 0} \frac{\vec F_\parallel}{A} \label{eq_def_shear}
		\end{IEEEeqnarray}

		Contrary to pressure, shear is not a scalar, \ie it can (and often does) take different values in different directions. At a given \emph{point} in space we represent it as a vector $\vec \tau = \left(\tau_x, \tau_y, \tau_y\right)$, and in a fluid, there is a shear \vocab{vector field}:
		\begin{IEEEeqnarray}{rCl}
		\vec \tau_{(x, y, z, t)} &\equiv& \left(\begin{array}{c}
								\tau_x\\
								\tau_y\\
								\tau_z
								\end{array}\right)_{(x, y, z, t)}
		\end{IEEEeqnarray}
	
	%%%%	
	\subsection{Shear on an infinitesimal volume}

		\begin{wrapfigure}{O}[\marginparwidth+\marginparsep]{\marginparwidth}\vspace{-1.75\intextsep}\begin{center}\href{http://www.wetherobots.com/2007-11-26-large/}{\includegraphics[width=0.45\marginparwidth]{wetherobots_2007-11-26_crop}}\end{center}\par%\vspace{-0.5\intextsep}
{\scriptsize\setlength{\baselineskip}{0.9\baselineskip}We the Robots \#20071126: using the right terminology is important (or not?).\par}\vspace{-0.4cm}
\begin{flushright}\tiny
by Chris Harding\\
\href{http://www.wetherobots.com/2007-11-26-large/}{http://www.wetherobots.com/2007-11-26-large/}
\end{flushright}\vspace{-1.5cm}\end{wrapfigure}
		Describing the changes in space of the shear vector field requires another mathematical dimension (called \vocab{order}).
		Instead of a flat plate, let us consider an infinitesimally small cube within the fluid, as shown in figure~\ref{fig_tau_cube}. Because the cube is immersed inside a vector field, it may have a different the shear vector exerting on each of its six faces.
			\begin{figure}
				\begin{center}
					\includegraphics[width=\textwidth]{particle_shear_tensor}
				\end{center}
				\supercaption{Shear efforts on a cubic fluid particle (with only the efforts on the visible faces 1 to 3 represented). The shear tensor $\vec \tau_{ij}$ has six members of three components each.}{\wcfile{Shear stress infinitesimal volume element.svg}{Figure} \cczero \oc}
				\label{fig_tau_cube}
			\end{figure}
		
		In order to express the efforts on any given face, we express a component of shear with two subscripts:
			\begin{itemize}
				\item The first subscript indicates the direction normal to the surface in which we are interested;
				\item The second subscript indicates the direction in which the shear is applying.
			\end{itemize}
		
		\youtubetopthumb{KBPmTCKLFHc}{the net effect of shear}{\oc (\ccby)}
		For example, $\vec \tau_{xy}$ represents the shear in the $y$-direction on a surface perpendicular to the $x$-direction. On this face, the shear vector would be:
		\begin{IEEEeqnarray}{rCl}
			\vec \tau_{xj} 	&=& \vec \tau_{xx} + \vec \tau_{xy} + \vec \tau_{xz} \label{eq_shear_x}\\
							&=& \tau_{xx} \vec i + \tau_{xy} \vec j + \tau_{xz} \vec k 
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item where the subscript $xj$ indicates all of the directions ($j = x, y, z$) on a face perpendicular to the $x$-direction.
		\end{equationterms}
		
		\youtubethumb{_3EYEySeG4g}{what is $\tau_{xx}$, perpendicular shear?}{\oc (\ccby)}
		In eq.~\ref{eq_shear_x}, a surprising term appears: $\tau_{xx}$. It is the shear effort perpendicular to the surface of interest. How is this possible? The answer is that the faces of the infinitesimal cube studied here are not solid. They are permeable, and the local velocity may include have a component \emph{through} the face of the cube (in fact, this must happen for any flow to occur at all). Therefore, there is no reason for the shear effort, which is three-dimensional, to be aligned along each flat surface. As the fluid travels across any face, it can be sheared and strained in any arbitrary direction, and therefore, shear can and most often does have a component ($\tau_{ii}$) perpendicular to an arbitrary surface inside a fluid.
		
		Now, the net shear effect on the cube will have \emph{eighteen} components: one tree-dimensional vector for each of the six faces. Each of those components may take a different value. The net shear could perhaps be represented as en entity —a \vocab{tensor}— containing six vectors $\vec \tau_1, \vec \tau_2, \vec \tau_3 … \vec \tau_6$. By convention, however, shear is notated using only three vector components: one for each pair of faces. Shear efforts on a volume are thus represented with a \vocab{tensor field} $\vec \tau_{ij}$:
			\begin{IEEEeqnarray}{rCl}
				\vec \tau_{ij} 	&\equiv& \left(\begin{array}{c}%
						\vec \tau_{xj} \\
						\vec \tau_{yj} \\
						\vec \tau_{zj} %
						\end{array}\right)%
					\equiv \left(\begin{array}{c}%
							\vec \tau_{xj\ \{1,4\}} \\
							\vec \tau_{yj\ \{2,5\}} \\
							\vec \tau_{zj\ \{3,6\}} %
						\end{array}\right)\nonumber\\
				\vec \tau_{ij} &\equiv& \left(\begin{array}{ccc}%
						\tau_{xx} & \tau_{xy} & \tau_{xz} \\
						\tau_{yx} & \tau_{yy} & \tau_{yz} \\
						\tau_{zx} & \tau_{zy} & \tau_{zz}%
					\end{array}\right)\label{eq_def_shear_tensor}
			\end{IEEEeqnarray}
		
		In this last equation~\ref{eq_def_shear_tensor}, each of the nine components of the tensor acts as the container for two contributions: one for each of the two faces perpendicular to the direction expressed in its first subscript.
		
		So much for the shear \emph{effort} on an element of fluid. What about the net \emph{force} due to shear on the fluid element? Not every element counts:  part of the shear will accelerate (change the velocity vector) the particle, while part of it will merely strain (deform) the particle. Quantifying this force thus requires making a careful selection within the eighteen components of $\vec \tau_{ij}$. We may start with the $x$-direction, which consists of the sum of the component of shear in the $x$-direction on each of the six cube faces:
			\begin{IEEEeqnarray}{rCl}
				\vec F_{\text{shear}\ x} 	&=&  S_3 \vec \tau_{zx\ 3} - S_6 \vec \tau_{zx\ 6}\nonumber\\
											&& + S_2 \vec \tau_{yx\ 2} - S_5 \vec \tau_{yx\ 5}\nonumber\\
											&& + S_1 \vec \tau_{xx\ 1} - S_4 \vec \tau_{xx\ 4}
			\end{IEEEeqnarray}
		Given that $S_3 = S_6 = \diff x \diff y$, that $S_2 = S_5 = \diff x \diff z$ and that $S_1 = S_4 = \diff z \diff y$, this is re-written as:
			\begin{IEEEeqnarray}{rCl}
				\vec F_{\text{shear}\ x} 	&=&  \diff x \diff y \ (\vec \tau_{zx\ 3} - \vec \tau_{zx\ 6})\nonumber\\
											&& + \diff x \diff z \ (\vec \tau_{yx\ 2} - \vec \tau_{yx\ 5})\nonumber\\
											&& + \diff z \diff y \ (\vec \tau_{xx\ 1} - \vec \tau_{xx\ 4})\label{eq_fshear_xdir}
			\end{IEEEeqnarray}
		In the same way we did with pressure in \chapterfourshort (\S\ref{ch_pressure_and_depth} p.~\pageref{ch_pressure_and_depth}), we express each pair of values as a derivative with respect to space multiplied by an infinitesimal distance:
			\begin{IEEEeqnarray}{rCl}
				\vec F_{\text{shear}\ x} 	&=&  \diff x \diff y \left(\tdiff z \partialderivative{\vec \tau_{zx}}{z}\right) + \diff x \diff z \left(\tdiff y \partialderivative{\vec \tau_{yx}}{y}\right) + \diff z \diff y \left(\tdiff x \partialderivative{\vec \tau_{xx}}{x}\right)\nonumber\\
											&=&  \diff \vol \left(\partialderivative{\vec \tau_{zx}}{z} + \partialderivative{\vec \tau_{yx}}{y} + \partialderivative{\vec \tau_{xx}}{x}\right)\label{eq_shear_force_x_tmp}
			\end{IEEEeqnarray}
			
		\youtubethumb{PvcT_E55Lgc}{the divergent of shear}{\oc (\ccby)}
		We can see with this equation~\ref{eq_shear_force_x_tmp} that shear in the $x$-direction has three contributors (one for each pair of cube faces). Each of the contributors is a derivative in space of a shear component which points in the $x$-direction.
		
		Now, we introduce the operator \vocab{divergent} (see also Appendix~\ref{appendix_field_operators} p.~\pageref{appendix_field_operators}), written~$\divergent{}$~:
			\begin{IEEEeqnarray}{rCcCl}
				\divergent{}			&\equiv& \partialderivative{}{x} \vec i \cdot \ + \ \partialderivative{}{y} \vec j \cdot \ + \ \partialderivative{}{z} \vec k \cdot \label{eq_def_divergent}\\
				\divergent{\vec A}		&\equiv& \partialderivative{A_x}{x} \ + \ \partialderivative{A_y}{y} \ + \ \partialderivative{A_z}{z}\\
				\divergent{\vec A_{ij}}	&\equiv& \left(\begin{array}{c}%
						\partialderivative{A_{xx}}{x} + \partialderivative{A_{yx}}{y} + \partialderivative{A_{zx}}{z}\\
						\partialderivative{A_{xy}}{x} + \partialderivative{A_{yy}}{y} + \partialderivative{A_{zy}}{z}\\
						\partialderivative{A_{xz}}{x} + \partialderivative{A_{yz}}{y} + \partialderivative{A_{zz}}{z}\\
					\end{array}\right) &=&%
						\left(\begin{array}{c}%
							\divergent{\vec A_{ix}}\\
							\divergent{\vec A_{iy}}\\
							\divergent{\vec A_{iz}}\\
					\end{array}\right)
			\end{IEEEeqnarray}
		
		With this new tool, we can go back to equation~\ref{eq_shear_force_x_tmp} to see that the net shear force in the $x$-direction is equal to the particle volume times the divergent of the shear in the $x$-direction:
			\begin{IEEEeqnarray}{rCl}
				\vec F_{\text{shear}\ x} 	&=&  \diff \vol \ \divergent{\vec \tau_{ix}} \label{eq_fshear_xdir_divergent}
			\end{IEEEeqnarray}
		
		So much for the $x$-direction. The $y$- and $z$-direction are taken care of in the same fashion, so that we can gather up our puzzle pieces and express \emph{the force per unit volume due to shear as the divergent of the shear tensor}:
			\begin{IEEEeqnarray}{rCcCcCl}
				\vec F_{\text{shear}} 	&=&\left(\begin{array}{c}%
						F_{\text{shear}\ x} \\
						F_{\text{shear}\ y} \\
						F_{\text{shear}\ z} %
						\end{array}\right)%
					&=& \diff \vol \left(\begin{array}{c}%
						|\divergent{\vec \tau_{ix}}| \\
						|\divergent{\vec \tau_{iy}}| \\
						|\divergent{\vec \tau_{iz}}| %
						\end{array}\right)%
					&=& \diff \vol \ \divergent{\vec \tau_{ij}}
			\end{IEEEeqnarray}
			\begin{mdframed}
			\begin{IEEEeqnarray}{rCl}			
				\frac{1}{\diff \vol} \vec F_\text{net, shear} & = & \divergent{\vec \tau_{ij}}\label{eq_shear_force_divergent_shear}
			\end{IEEEeqnarray}
			\end{mdframed}
		
		An example of a divergent of shear vector field is shown in figures~\ref{fig_cfd_divergent_shear} and~\ref{fig_cfd_divergent_shear_magnitude}.
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=\textwidth]{divergent_shear_vector_field}
			\end{center}
			\supercaption{The divergent of shear in the flow field of the computed flow described in figure~\ref{fig_cfd_velocity_pressure} p.~\pageref{fig_cfd_velocity_pressure}. Vectors with magnitude lower than \SI{1}{\pascal\per\meter} are not represented. The arrows indicate the local force per unit volume with which shear is acting on the fluid.}{Figure \ccby by Arjun Neyyathala}
			\label{fig_cfd_divergent_shear}
		\end{figure}
		
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=0.8\textwidth]{divergent_shear_magnitude1}
				\includegraphics[width=0.8\textwidth]{divergent_shear_magnitude2}
			\end{center}
			\supercaption{The magnitude of the divergent of shear in the flow field of the computed flow described in figure~\ref{fig_cfd_velocity_pressure} p.~\pageref{fig_cfd_velocity_pressure} (the magnitude of the vector field represented in figure~\ref{fig_cfd_divergent_shear} above). In the top image, the color scale is saturated at \SI{1e5}{\pascal\per\metre}, while on the bottom image, it reaches a value 20 times higher (\SI{2e6}{\pascal\per\metre}), showing the very high local values attained very close to the wall.}{Figures \ccby by Arjun Neyyathala}
			\label{fig_cfd_divergent_shear_magnitude}
		\end{figure}

		This equation~\ref{eq_shear_force_divergent_shear} is more than we really need to go through the problems in this chapter, but we will come back to it when we will want to calculate the dynamics of fluid particles in \chaptersix, where the divergent of shear be a building block of the glorious \vocab{Navier-Stokes equations}. For now, it is enough to sum up our findings as follows:
			\begin{itemize}
				\item Shear at a point in space has three components — it is a vector field;
				\item The effect of shear on a volume of fluid has eighteen components – it is a second-order tensor field;
				\item The net force due to shear on a volume of fluid, expressed using the divergent of the shear tensor, has three components — it is a vector field.
			\end{itemize}

		\protip{portrait29}{0.35}{0.35}{7}{
			Again, take a moment to consider the dimensions involved here. If you were to store information about shear in a flow in a file on a \textsc{usb} key, how many columns would you need? Shear is a \emph{vector field}, which means it has three components (each in \si{Pa}) at every point in space and time: that’s three values to store for each combination of $x$,$y$,$z$ and $t$.\\
			If you were to calculate the \emph{effect} of shear, then you would calculate its divergent, which is also a three-dimensional vector field. To record this information, you would, likewise, have to store three values (one for each of $|\divergent{\vec \tau_{ix}}|$, $|\divergent{\vec \tau_{iy}}|$, and $|\divergent{\vec \tau_{iz}}|$, all in \si{\pascal\per\metre}), for each combination of $x$,$y$,$z$ and $t$. Just like the negative of the gradient of pressure, the divergent of shear represents a force per unit volume, showing in which direction shear is “pushing” the fluid particles as they flow.
		}{}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Resistance to shear: viscosity}

	%%%%
	\subsection{Viscosity}
	\label{ch_viscosity_second_pass}
	
		In \chapteroneshort, we saw already that \vocab{viscosity} $\mu$ is a fluid property that quantifies its resistance to shear (see \S\ref{ch_viscosity_first_pass} p.~\pageref{ch_viscosity_first_pass}). More precisely, we quantified $\mu$ as the ratio of shear stress to strain rate with equation~\ref{eq_firstdef_viscosity}, reproduced here:
			\begin{IEEEeqnarray}{rCl}
				\mu &\equiv& \frac{\tau}{\left(\frac{\Delta v}{\Delta y}\right)}
			\end{IEEEeqnarray}
	
		Now, we generalize this equation: we shrink down the “brick” of fluid from \chapteroneshort down to an infinitesimal volume of fluid inside an arbitrary flow (\cref{fig_shear_velocity_gradient}). The strain rate is now $\inlinepartialderivative{V_j}{i}$, which is the rate of change in the $i$-direction of the velocity in the $j$-direction. Viscosity is the ratio between shear in the shear in the $j$-direction and this strain rate:
			\begin{IEEEeqnarray}{rCl}
				\mu		&\equiv& \frac{||\vec \tau_{ij}||}{\left(\partialderivative{V_j}{i}\right)} \label{eq_def_viscosity}
			\end{IEEEeqnarray}
		If we turn this equation around, we find that we can express the local shear by differentiating the local velocity with respect to distance:
		\begin{mdframed}
			\begin{IEEEeqnarray}{rCl}
				||\vec \tau_{ij}|| 	&=& \mu \partialderivative{V_j}{i} \label{eq_shear_velocity_gradient}
			\end{IEEEeqnarray}
			\begin{equationterms}
				\item in which the subscript $i$ is an arbitrary direction ($x$, $y$ or $z$) and $j$ is the direction following it in order (\eg $j=z$ when $i=y$);
				\item and where $\mu$ is the viscosity (or “dynamic viscosity”) (\si{\pascal\second}).
			\end{equationterms}
		\end{mdframed}

		
		\begin{figure}[ht]
			\begin{center}
				%\vspace{-0.5cm}
				\includegraphics[width=\textwidth]{shear_velocity_gradient}
				%\vspace{-1cm}
			\end{center}
			\supercaption{Any velocity gradient $\inlinepartialderivative{V_y}{x} = \inlinepartialderivative{v}{x}$ in the flow results in a shear force $F_\parallel$ in the direction~$y$. The ratio between the shear and the velocity gradient is called \vocab{viscosity}.}{\wcfile{Concept of shear in fluid.svg}{Figure} \cczero \oc}\vspace{-0.5cm}%handmade
			\label{fig_shear_velocity_gradient}
		\end{figure}	

		Viscosity $\mu$ is measured in \si{\pascal\second}, which is the same as~\si{\newton\second\per\metre\squared} or~\si{\kilogram\per\metre\per\second}. It has historically been measured in \si{poise} ($\SI{1}{poise} \equiv \SI{0,1}{\pascal\second}$).
		
		\youtubethumb{TR0R0aB_MP8}{viscosity for the engineer}{\oc (\ccby)}
		The values of viscosity vary very strongly from one fluid to another: for example, honey is roughly ten thousand times more viscous than water, which is roughly a hundred times more viscous than ambient air. The viscosities of four relevant fluids are quantified in figure~\ref{fig_viscosity_values} (where they can easily be quantified) and figure~\ref{fig_viscosity_linear} (where the relative values are better observed).
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=\textwidth]{viscosity_small_linear_short.pdf}
			\end{center}
			\supercaption{The viscosity of four fluids (crude oil, water, air, and C02) as a function of temperature, plotted on a linear scale. This makes clearly visible the difference in the order of magnitudes of the viscosities of the four fluids, but air and CO2 are indistinguishably close to the zero axis. A more useful version of this figure is shown as figure~\ref{fig_viscosity_values}.}{Figure \ccby by Arjun Neyyathala \& \olivier}
			\label{fig_viscosity_linear}
		\end{figure}
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=\textwidth]{viscosity_small.pdf}
			\end{center}
			\supercaption{The viscosity of four fluids (crude oil, water, air, and C02) as a function of temperature. The scale for liquids is logarithmic and displayed on the left; the scale for gases is linear and displayed on the right.}{Figure \ccby by Arjun Neyyathala \& \olivier}
			\label{fig_viscosity_values}
		\end{figure}

	%%%%
	\subsection{Kinematic viscosity}
	\label{ch_kinematic_viscosity}
	
		Sometimes, the concept of \vocab{kinematic viscosity} is used. Kinematic viscosity is written $\nu$: the Greek letter \textit{nu}, an unfortunate choice because it is easy to mis-read as the $y$-component of velocity, $v \equiv V_y$. Kinematic viscosity $\nu$ is defined as
		\begin{equation}
			\nu \equiv \frac{\mu}{\rho}
		\end{equation}
		\begin{equationterms}
			\item where $\nu$ is measured in \si{\metre\squared\per\second}.
		\end{equationterms}
		Kinematic viscosity is formulated that way because it is the part of the Reynolds number (see eq.~\ref{eq_def_reynolds_number} p.~\pageref{eq_def_reynolds_number}) that depends on the fluid properties. We do not make use of it in this course.


	%%%%
	\subsection{Turbulent viscosity}
	\label{ch_turbulent_viscosity}
	
		In computational fluid dynamics (\cfd) simulations, use is made of \vocab{turbulent viscosity} $\mu_\text{T}$ (in \si{\pascal\second} just like viscosity). This is because the bulk effect of turbulence is to increase dissipation, in a way that is similar to viscous dissipation. Turbulent viscosity is therefore a property of the \emph{flow}, with values that vary very strongly with space and time within the flow (unlike viscosity which is usually only a property of the fluid). We will encounter it in \chapternine.


	%%%%
	\subsection{Non-Newtonian fluids}
	\label{ch_newtonian_fluid}
	
		Viscosity is said to be a property of the \emph{fluid}, and not of the flow, because for most fluids, it has a value that varies only with temperature. Those fluids are formally called \vocab{Newtonian fluids}. Most fluids of interest in engineering fluid mechanics (air, water, exhaust gases, pure gases) are Newtonian fluids.
		
		Some fluids, however, feature a strain rate that depends on the value of shear stress: one could say that they have “variable viscosity”, depending on how they are strained. Those fluids are called \vocab{non-Newtonian fluids}. The study of the viscosity characteristics of such fluids is called \vocab{rheology}\wupen{Rheology}.
		
		Some fluids, like oil-based paint, jelly-based fluids, or tomato juice, are \vocab{shear-thinning}: they become progressively less viscous when the strain rate is increased.\\
		Some fluids, like blood, are \vocab{shear-thickening}: they become progressively thicker when the strain rate is increased. A few representative viscosity characteristics are displayed in figure~\ref{fig_viscosity_characteristics}.
		\begin{figure}
			\begin{center}\vspace{-0.5cm}
				\includegraphics[width=9cm]{viscosity_characteristics}
			\end{center}
			\supercaption{Various possible viscosity characteristics of fluids. Those for which the slope of the curve ($\mu$) varies with $\inlinepartialderivative{V_j}{i}$ (so, those which do \emph{not} feature straight lines on this diagram) are called \vocab{non-Newtonian}.}{\wcfile{Fluid viscosity relationships (rheology) 2.svg}{Figure} \cczero \oc}\vspace{-0.5cm}
			\label{fig_viscosity_characteristics}
		\end{figure}
		
		\protip{portrait32}{0.24}{0.24}{8}{
		“Non-Newtonian” does not mean “sticky”; it merely means that the fluid is “variably sticky”. Some, like plant resin, resist being strained too quickly, while others, like paint, resist being strained too slowly. It is the behavior which matters, and not the actual value of their viscosities (which may be generally high or low). \\
		Mix some starch and water in a bowl, and pour some paint in another: time spent playing with those two fluids in your kitchen totally counts as serious fluid mechanics study time.
		}{}


	\subsection{The no-slip condition}
	\label{ch_no_slip_condition}

		We observe that whenever we measure the velocity of a fluid flow along a solid wall, the speed tends to zero as we approach the wall surface. In other words, the fluid adheres to the surface regardless of the overall faraway flow velocity. This phenomenon, called the \vocab{no-slip condition}, is extremely important in fluid dynamics. One consequence of this is that fluid flows near walls are dominated by viscous effects (internal friction) due to the large strain rates there.
		
		\protip{portrait20}{0.21}{0.21}{7}{
		The no-slip condition is actually pretty freaky: no matter how fast you go, and no matter how smooth the surface, the fluid velocity on the surface of a solid object will always be zero! Engineers like to have polished, low-roughness surfaces on machines like aircraft, because this reduces the \emph{thickness} of the shear layer, and how turbulent it becomes, thus reducing shear — we will study this in \chapterten. But there is no escaping the fact that fluid particles next to the wall will always feature zero relative velocity.
		}{}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Special case: shear in simple laminar flows}

	In any ordinary fluid flow, the velocity field is complex, and it is difficult to express shear and its net effect on particles. Since this requires expressing three values at each point in (three-dimensional) space and time, this would require complex mathematics or large amounts of discrete data.
	
	In simple cases, however, it is possible to express and calculate shear relatively easily. This is especially true in simple, steady, laminar (smooth) flows —typically flows for which the Reynolds number (eq.\ref{eq_def_reynolds_number} p.~\pageref{eq_def_reynolds_number}) is low.
	
	In those cases, we can \emph{guess} a reasonably realistic velocity distribution, and then derive an expression for the distribution of shear from it.
	
	One such classical example is the \vocab{Couette flow}, where fluid is imprisoned between a static flat surface and another flat surface moving parallel to it, as illustrated in figure~\ref{fig_couette_flow}. In this case, the bottom wall and top wall velocities, as well as the spacing~$H$, are known.
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=0.6\textwidth]{couette_flow}
			\end{center}
			\supercaption{A simple flow. The bottom wall is stationary, while the top wall slides from left to right. In between the walls, fluid is strained uniformly.}{\wcfile{Couette flow.svg}{Figure} \ccbysa \wcu{Kulmalukko}}
			\label{fig_couette_flow}
		\end{figure}

	A reasonable guess for the velocity distribution in steady laminar regime is:
	\begin{IEEEeqnarray*}{rCl}
		\left\{ \begin{array}{rcl}
			V_x &=& V_\text{bottom wall} + k y\\
			V_y &=& 0
		\end{array}\right.
	\end{IEEEeqnarray*}
	By applying boundary conditions ($V_\text{bottom wall} = 0$ and $V_{x\ @\ y=H} = V_\text{top wall}$) we can re-write this as:
	\begin{IEEEeqnarray*}{rCl}
		\left\{ \begin{array}{rcl}
			V_x &=& 0 + \frac{V_\text{top wall}}{H} y\\
			V_y &=& 0
		\end{array}\right.
	\end{IEEEeqnarray*}

	And now that the velocity field is known, the shear everywhere in the fluid can be computed. The shear in the $x$-direction is proportional to the derivative in the $y$-direction of the velocity in the $x$-direction:
	\begin{IEEEeqnarray*}{rCl}
		\tau_{yx} 	&=& \mu \derivative{}{y} \left(0 + \frac{V_\text{top wall}}{H} y \right)\\
					&=& \frac{\mu \ V_\text{top wall}}{H}
	\end{IEEEeqnarray*}
	Thus, we see here that the shear applied in the fluid is the same everywhere (it is independent of $y$ and $x$). A few slightly more complex cases are waiting for us in the problem sheet; but to handle more realistic shear distributions, what is needed is a software able to compute the behavior of fluids. The basic but formidable equations to be solved for this are the topic of the upcoming \chaptersix.
	
	\protip{portrait26}{0.29}{0.29}{9}{
	Note that this method only works because we have \emph{guessed} the velocity field before we calculated shear. We can only do this when flows are trivially simple. What if we don’t know what the velocity is to begin with?\\
	Things will be much harder once we have to evaluate shear based on a calculated velocity field, so that $\vec \tau$ and $\vec V$ depend on one another. Not to say, but it looks like the math in \chaptersixshort will be off its kadoova.
	}{}

\clearpage
\section{Solved problems}


\begin{youtubesolution}
	\begin{center}
		\textbf{Shear force on a flat plate}
	\end{center}

	\begin{center}
	\includegraphics[width=0.75\textwidth]{shear_force_a4_sheet}
	\end{center}

	A flat plate with dimensions \SI{200}{\milli\metre}$\times$\SI{400}{\milli\metre} is moved \SI{1}{\milli\metre} above the ground at \SI{0,5}{\metre\per\second}, in a fluid with viscosity \SI{1e-5}{\pascal\second}. What is the shear force applying on the plate?
	
	\seesolution{aPIz4ASi6cQ}
\end{youtubesolution}

\begin{youtubesolution}
	\begin{center}
		\textbf{Cylindrical viscometer}
	\end{center}

	\begin{center}
	\includegraphics[width=0.45\textwidth]{viscometer2}
	\end{center}

	A moment of \SI{0,9}{\newton\metre} is required to turn the inner cylinder of a viscometer (a device designed to measure viscosity) is turned at~$\SI{150}{rpm}$.
	
	The inner cylinder has diameter \SI{20}{\centi\metre}, the height of the liquid is \SI{80}{\centi\metre}, and the spacing between the two cylinders (greatly exaggerated on the drawing) is \SI{2}{\milli\metre}.
	
	What is the viscosity of the fluid?
	
	~
	
	~
	
	~
	
	~
		
	\seesolution{jc9qIv-jzC4}
		{\small Note: Unfortunately Olivier made an error in this video: the height $H_1$ is forgotten in the last few lines. The final numerical result is nevertheless correct ($\mu = \SI{2,27e-2}{\pascal\second}$). Many thanks to the students who double-checked and reported the problem!}
\end{youtubesolution}
	
\atendofchapternotes

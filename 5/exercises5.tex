\renewcommand{\lastedityear}{2020}
 \renewcommand{\lasteditmonth}{06}
   \renewcommand{\lasteditday}{05}
\renewcommand{\numberofthischapter}{5}
\renewcommand{\titleofthischapter}{\namechapterfive}
\atstartofexercises

\fluidmechexercisestitle

\mecafluexboxen

\begin{boiboiboite}
Shear force on a flat solid surface:
		\begin{IEEEeqnarray}{rCl}
			F_{\text{shear, direction } i} & = & \iint_S \tau_{\text{direction } i} \diff S  \ztag{\ref{eq_shear_force_scalar}}
		\end{IEEEeqnarray}

Shear in the direction $j$, on a plane perpendicular to direction $i$:
		\begin{IEEEeqnarray}{rCl}
			||\vec \tau_{ij}|| 	&=& \mu \partialderivative{V_j}{i} \ztag{\ref{eq_shear_velocity_gradient}}
		\end{IEEEeqnarray}
\end{boiboiboite}
	\begin{figure}[ht]
		\begin{center}\vspace{-0.8cm}
			\includegraphics[width=0.97\textwidth]{viscosity_small.pdf}
		\end{center}\vspace{-0.8cm}
		\supercaption{The viscosity of four fluids (crude oil, water, air, and C02) as a function of temperature. The scale for liquids is logarithmic and displayed on the left; the scale for gases is linear and displayed on the right. This is a reproduction of figure~\ref{fig_viscosity_values} p.~\pageref{fig_viscosity_values}.}{Figure \ccby by Arjun Neyyathala \& \olivier}
		\label{fig_viscosity_values_ex1}
	\end{figure}


%%%%%% Quiz
\subsubsection{Quiz}

	\quiz

%%%%%%%%%%%

\subsubsection{Flow in between two plates}
\wherefrom{Munson \& al. \smallcite{munsonetal2013} Ex1.5}
\label{exo_shear_flow_plates}

	A fluid is forced to flow between two stationary plates (\cref{fig_plates}). We observe that the flow is laminar (smooth and fully steady), with a univorm velocity profile $u = f(y)$ which is linked to the average fluid velocity~$V_\text{average}$ by the relationship:
		\begin{equation}
			u = \frac{3}{2} V_\text{average} \left[1 - \left(\frac{y}{H}\right)^2\right]
		\end{equation}
		\begin{equationterms}
			\item where \tab $y$ \tab is measured from the middle of the gap;
			\item and  \tab $H$ \tab is half of the gap length.
		\end{equationterms}
	
		\begin{figure}[ht!]
			\begin{center}
				\includegraphics[width=0.8\textwidth]{velocity_distribution_couette_flow.png}
			\end{center}\vspace{-0.5cm}%handmade
			\supercaption{Velocity distribution for laminar flow in between two plates, also known as \vocab{Couette flow}.}{\wcfile{Couette flow flat plate laminar velocity distributions.svg}{Figure} \cczero \oc}\vspace{-0.5cm}%handmade
			\label{fig_plates}
		\end{figure}
	
	%Water at 40 degC : 6.4E-4 Pas
	The fluid is water at \SI{40}{\degreeCelsius}, the average velocity is~\SI{0,6}{\metre\per\second} and the two plates are~\SI{1,5}{\milli\metre} apart. 
	
	\begin{enumerate}
		\item What is the shear effort $\tau_{yx\text{ plate}}$ generated on the lower plate?
		\item What is the shear effort $\tau_{yx}$ in the middle plane of the flow?
	\end{enumerate}


\clearpage
\subsubsection{Friction on a plate}
%homemade
\label{exo_shear_friction_plate}

	A plate the size of an A4 sheet of paper ($\SI{210}{\milli\metre} \times \SI{297}{\milli\metre}$) is moved horizontally at constant speed above a large flat surface (\cref{fig_afour_sheet}). We assume that the velocity profile of the fluid betweeen the plate and the flat surface is entirely uniform, smooth, and steady.
		\begin{figure}[ht!]
			\begin{center}
				\includegraphics[width=0.7\textwidth]{shear_force_a4_sheet.png}
			\end{center}
			\supercaption{A plate moved horizontally across a flat surface.}{\wcfile{Concept of shear in fluid.svg}{Figure} \cczero \oc}
			\label{fig_afour_sheet}
		\end{figure}
	
	\begin{enumerate}
		\item Express the force $F_{yz}$ due to shear on the plate as a function of its velocity $U_\text{plate}$, the gap height~$H$, and the properties of the fluid.
		\item The plate speed is $U_\text{plate} = \SI{1}{\metre\per\second}$ and the gap height is $H = \SI{5}{\milli\meter}$. What is the shear force $F_{yz}$ when the fluid is air at \SI{40}{\degreeCelsius}, and when the fluid is crude oil at the same temperature?
		% Air at 40 degC: 1.91E-5 Pas
		% Crude oil at 40 deg C: 5.25E-3 Pas
		\item If a very long and thin plate with the same surface area was used instead of the A4-shaped plate, would the shear force be different? (briefly justify your answer, \eg in 30 words or less)
	\end{enumerate}


\subsubsection{Viscometer}
\wherefrom{Çengel \& al. \smallcite{cengelcimbala2010} 2-78}
\label{exo_viscometer}

	An instrument designed to measure the viscosity of fluids (named \vocab{viscometer}) is made of two coaxial cylinders (\cref{fig_viscometer}). The inner cylinder is immersed in a liquid, and it rotates within the stationary outer cylinder.
		\begin{figure}[ht!]
			\begin{center}
				\includegraphics[height=7cm]{viscometer}
			\end{center}
			\supercaption{Sketch of a cylinder viscometer. The width of the gap has been greatly exaggerated for clarity.}{\wcfile{Simple cylinder viscometer schematic.svg}{Figure} \cczero \oc}
			\label{fig_viscometer}
		\end{figure}

	The two cylinders are~\SI{75}{\centi\metre} tall. The inner cylinder diameter is~\SI{15}{\centi\metre} and the spacing is~\SI{1}{\milli\metre}.
	
	When the inner cylinder is rotated at~\SI{300}{rpm}, a friction-generated moment of~\SI{0,8}{\newton\metre} is measured. 
	
	\begin{enumerate}
		\item If the flow in between the cylinders corresponds to the simplest possible flow case (steady, uniform, fully-laminar), what is the viscosity of the fluid?
		\item Would a non-Newtonian fluid induce a higher moment? (briefly justify your answer, \eg in 30 words or less)
	\end{enumerate}
	
	\textit{[Note: in practice, when the inner cylinder is turned at high speed, the flow displays mesmerizing patterns called \wed{Taylor-Couette flow}{Taylor—Couette vortices}, the description of which is much more complex!]}
	


\subsubsection{Boundary layer}
\wherefrom{White \smallcite{white2008} P1.56}
\label{exo_shear_boundary_layer}
	
	A laminar fluid flow occurs along a wall (\cref{fig_bl}). Close to the wall ($y < \delta$), we observe that viscous effects dominate the mechanics of the flow. This zone is designated \vocab{boundary layer}. The speed $u_{(y)}$ can then be modeled with the relation:		
		\begin{equation}
			u = U \sin \left( \frac{\pi y}{2 \delta} \right)
		\end{equation}
	in which $U$ is the flow speed far away from the wall.
		
		\begin{figure}[h]
			\begin{center}
				\includegraphics[width=0.8\textwidth]{boundary_layer_velocity_distribution.png}
			\end{center}
			\supercaption{Velocity profile across the boundary layer.}{\wcfile{Couette flow flat plate laminar velocity distributions.svg}{Figure} \cczero \oc}
			\label{fig_bl}
		\end{figure}
	
	% CO2 at 20 degC: 1.48E-5 Pas
	The fluid is CO2 at~\SI{20}{\degreeCelsius}; measurements yield $U =~\SI{10,8}{\metre\per\second}$ and $\delta =~\SI{3}{\centi\metre}$.
	
	\begin{enumerate}
		\item What is the shear effort $\tau_{yx}$ on the wall?
		\item At which height $y_1$ above the surface will the shear effort be half of this value?
		% Water at 20 degC: 1e-3 Pas
		\item What would be the wall shear if the CO2 was replaced with water at the same temperature?
	\end{enumerate}


\subsubsection{Clutch}
\wherefrom{Çengel \& al. \smallcite{cengelcimbala2010} 2-74}
\label{exo_clutch}

	Two aligned metal shafts are linked by a clutch, which is made of two disks very close one to another, rotating in the same direction at similar (but not identical) speeds. The disk diameters are both~\SI{30}{\centi\metre} and the gap between them is~\SI{2}{\milli\metre}; they are submerged in crude oil with temperature~\SI{80}{\degreeCelsius}.
		% Crude oil at 80 degC: 3.3e-3 Pas
		
		\begin{figure}[ht!]
			\begin{center}
				\includegraphics[height=6cm]{clutch.png}
			\end{center}
			\supercaption{Sketch of the two disks constituting the clutch. The gap width has been exaggerated for clarity.}{\wcfile{Schematic drawing of a simple clutch.svg}{Figure} \cczero \oc}
			\label{fig_clutch}
		\end{figure}
	
	The power shaft rotates at~\SI{1450}{rpm}, while the powered shaft rotates at~\SI{1398}{rpm}. We consider the simplest possible flow case (steady, laminar) in between the two disks.
	\begin{enumerate}
		\item What is the moment imparted by one disk to the other?
		\item How would the moment change if the radius of each disk was doubled?
		\item What is the transmitted power and the clutch efficiency?
		\item Briefly (\eg in 30 words or less) propose one reason why in practice the flow in between the two disks may be different from the simplest-case flow used in this exercise.
	\end{enumerate}


\clearpage	
\subsubsection*{Answers}
\NumTabs{1}
\begin{description}
	\item [\ref{exo_shear_flow_plates}]%
				\tab 1) $\tau_{yx}|_{y=-H} = \SI{1,44}{\newton\per\metre\squared}$; 
				\tab 2) $\tau_{yx}|_	{y=0} = \SI{0}{\newton\per\metre\squared}$.
	\item [\ref{exo_shear_friction_plate}]%
				\tab 1) $F_{yz} = L_1 L_2 \mu \frac{U}{H} = \SI{2,38e-4}{\newton}$ for air, and \SI{6,55e-2}{\newton} for oil.
	\item [\ref{exo_viscometer}]%
				\tab $\mu = M L_{\Delta R} / \left(2 \pi \omega R_1^3 H\right) = \SI{1,281e-2}{\newton\second\per\metre\squared}$.
	\item [\ref{exo_shear_boundary_layer}]%
				\tab 1) $\tau_{yx, \text{wall CO2}} = \SI{8.369e-3}{\newton\per\meter\squared}$;
				\tab 2) $y_1 = \frac{2}{3} \delta = \SI{2}{\centi\metre}$;
				\tab 3)  and $\tau_{yx, \text{wall water}} = \SI{0,565}{\newton\per\meter\squared}$
	\item [\ref{exo_clutch}]%
				\tab 1) $M = \frac{\pi}{2} \frac{\mu \omega}{h} R^4 = \SI{7,145e-3}{\newton\metre}$; 
				\tab 3) $\dot W_2 = \omega_2 M = \SI{1,05}{\watt}$; $\eta_\text{clutch} = \frac{\dot W_2}{\dot W_1} = \SI{96,4}{\percent}$ (adequate for this very low-power, low-relative speed, laminar-flow case).
\end{description}


\atendofexercises

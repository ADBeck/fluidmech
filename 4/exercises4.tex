\renewcommand{\lastedityear}{2020}
 \renewcommand{\lasteditmonth}{09}
   \renewcommand{\lasteditday}{14}
\renewcommand{\numberofthischapter}{4}
\renewcommand{\titleofthischapter}{\namechapterfour}
\atstartofexercises

\fluidmechexercisestitle

\mecafluexboxen


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Reading quiz}

	\quiz


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Pressure in a static fluid}
\label{exo_basic_pressure}
%homemade

	A small water container whose geometry is described in \cref{fig_small_water_container} is filled with water. What is the pressure at the bottom of the container?
		\begin{figure}[h]
			\begin{center}
				\includegraphics[width=0.6\textwidth]{reservoir1}
				\vspace{-0.5cm}%handmade
			\end{center}
			\supercaption{A small water container.}{\wcfile{Simple fluid receptacle.svg}{Figure} \cczero \oc}
			\label{fig_small_water_container}
			\vspace{-1cm}%handmade
		\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Pressure measurement with a U-tube}
\label{exo_utube}
	
	A tube is connected to a pressurized air vessel as shown in \cref{fig_utube}. The U-tube is filled with water. What is the pressure $p_\text{int.}$ in the vessel?
		\begin{figure}[h]
			\begin{center}
				\includegraphics[height=3.6cm]{utube}\vspace{-0.5cm}%handmade
			\end{center}
			\supercaption{Working principle of a simple liquid tube manometer. The outlet is at atmospheric pressure $p_\atm$.\vspace{-0.5cm}}{\wcfile{A fluid tube manometer.svg}{Figure} \cczero \oc}
			\label{fig_utube}
		\end{figure}
		
	What would be the height difference shown for the same internal pressure if mercury ($\rho_\text{mercury} = \SI{13 600}{\kilogram\per\metre\cubed}$) was used instead of water?


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Straight water tank door}
\label{exo_straight_water_tank_door}

A water tank has a window on one of its straight walls, as shown in figure~\ref{fig_aquarium_straightwalls}. The window is \SI{3}{\metre} high, \SI{4}{\metre} wide, and is positioned \SI{0,4}{\metre} above the bottom of the tank.
	\begin{figure}[h]
		\begin{center}
			\includegraphics[width=12cm]{aquarium_straightwalls}
			\vspace{-0.5cm}%handmade
		\end{center}
		\supercaption{An aquarium tank with a window installed on one of its walls}{\wcfile{Aquarium tank with straight walls.svg}{Figure} \cczero \oc}
	\vspace{-1cm}%handmade
		\label{fig_aquarium_straightwalls}
	\end{figure}

\begin{enumerate}
	\item What is the magnitude of the force applying on the window due to the pressure exerted by the water?
	\item At which height does this force apply?
\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Access door on a water channel wall}
\label{exo_water_canal_access_door}

An open water channel used in a laboratory is filled with stationary water (\cref{fig_aquarium_inclinedwalls}). An observation window is installed on one of the walls of the channel, to enable observation and measurements. The window is hinged on its bottom face.\\
The hinge stands \SI{1,5}{\metre} below the water surface. The window has a length of~\SI{0,9}{\metre} and a width of \SI{2}{\metre}. The walls of the channel are inclined with an angle $\theta = \SI{60}{\degree}$ relative to horizontal.
		
		\begin{figure}[ht!]
			\begin{center}
				\includegraphics[width=0.9\textwidth]{aquarium_inclinedwalls}
			\end{center}
			\supercaption{A door installed on the wall of a water channel. The water in the canal is perfectly still.}{\wcfile{Aquarium tank with inclined walls.svg}{Figure} \cczero \oc}\vspace{-1cm}%handmade
			\label{fig_aquarium_inclinedwalls}
		\end{figure}
	\begin{enumerate}
		\item Represent graphically the pressure of the water and atmosphere on each side of the window.
		\item What is the magnitude of the moment exerted by the pressure of the water about the axis of the window hinge?
		\item If the same door was positioned at the same depth, but the angle $\theta$ was decreased, would the moment be modified? (briefly justify your answer, \eg in 30 words or less)
	\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Pressure force on a cylinder}
\label{exo_pressure_force_cylinder}

	An idealized flow over a cylinder is depicted in figure~\ref{fig_potential_flow_cylinder_coordinates}. This is a very primitive flow solution, obtained using a model (the \vocab{potential flow} model, which we mention in \chapterelevenshort) which cannot account for viscous effects or flow separation. Nevertheless, it provides a good first “ideal flow” situation to compute surface pressure forces in fluid flows.
	\begin{figure}[h]
		\begin{center}
			\includegraphics[width=0.7\textwidth]{potential_flow_cylinder_coordinates}
		\end{center}
		\supercaption{Idealized flow around a cylinder, as predicted by potential flow theory.}{\wcfile{Potential cylinder (colorless).svg}{Figure} \ccbysa by \wcu{Kraaiennest} \& \oc}
		\label{fig_potential_flow_cylinder_coordinates}
	\end{figure}
	
	It is possible to express the pressure distribution $p_s$ on the surface of a cylinder standing in a fluid flow with faraway velocity $V_\infty$ and pressure $p_\infty$ (see also problem \ref{exo_hangar_roof} p.~\pageref{exo_hangar_roof}). The pressure distribution is expressed in cylindrical coordinates as:
	\begin{IEEEeqnarray}{rCl}
		p_s &=& p_\infty + \frac{1}{2} \rho \left(V_\infty^2 - 4 V_\infty^2 \sin^2 \theta\right)
	\end{IEEEeqnarray}
	
	We would like to quantify the horizontal force $F_\text{upstream}$ exerted due to pressure on the upstream half of the cylinder, and then the vertical force $F_\text{top}$ exerted due to pressure on the top half of the cylinder. Those forces are displayed in figure~\ref{fig_potential_flow_cylinder_forces}.\\
	(a couple of hints to help with the algebra: $\int \sin x \diff x = -\cos x + k$ and $\int \sin^3 x \diff x = \frac{1}{3} \cos^3 x - \cos x + k$).
	\begin{figure}[h]
		\begin{center}\vspace{-0.5cm}%handmade
			\includegraphics[width=0.5\textwidth]{potential_flow_cylinder_forces}\vspace{-0.5cm}%handmade
		\end{center}
		\supercaption{Left: horizontal force on the upstream half of the cylinder. Right: vertical (lift) force on the top half of the cylinder.}{\cczero \oc}
		\label{fig_potential_flow_cylinder_forces}\vspace{-1cm}%handmade
	\end{figure}
		
	We consider a cylinder of diameter \SI{10}{\centi\metre}, spanning \SI{50}{\centi\metre} across the flow, in an atmospheric air flow coming in at \SI{50}{\kilo\metre\per\hour}.
	\begin{enumerate}
		\item {[\textit{non-examinable question}]} What is the magnitude of $\vec F_\text{upstream}$?
		\item What is the magnitude of $\vec F_\text{top}$?
	\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Buoyancy of a barge}
\label{exo_buoyancy_barge}

	A barge of very simple geometry is moored in a water reservoir (\cref{fig_simple_barge}).
		
		\begin{figure}[h]
			\begin{center}
				\includegraphics[width=12cm]{barge}
			\end{center}\vspace{-0.5cm}%handmade
			\supercaption{Basic layout of a barge floating in water.}{\wcfile{A conceptual drawing of a barge with very simple geometry.svg}{Figure} \cczero \oc}\vspace{-1cm}%handmade
			\label{fig_simple_barge}
		\end{figure}
	
		\begin{enumerate}
			\item Sketch the distribution of pressure on each of the immersed walls of the barge (left and right sides, rear	, bottom and slanted front).
			\item What is the magnitude of the force resulting from pressure efforts on each of these walls?
			\item What is the weight of the barge?
		\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Atmospheric pressure distribution}
\label{exo_burj_khalifa}
\wherefrom{non-examinable}

	The integration we carried out in with equation~\ref{eq_atmtemp} p.~\pageref{eq_atmtemp} to model the pressure distribution in the atmosphere was based on the hypothesis that the temperature was uniform and constant ($T = T_\cst$). In practice, this may not always be the case.
		
		\begin{enumerate}
			\item If the atmospheric temperature decreases with altitude at a constant rate (\eg of~\SI{-7}{\kelvin\per\kilo\metre}), how can the pressure distribution be expressed analytically?
		\end{enumerate}
	
	A successful fluid dynamics lecturer purchases an apartment at the top of the \we{Burj Khalifa} tower (\SI{800}{\metre} above the ground). Inside the tower, the temperature is controlled everywhere at \SI{18,5}{\degreeCelsius}. Outside, the ground temperature is~\SI{30}{\degreeCelsius} and it decreases linearly with altitude (gradient: \SI{-7}{\kelvin\per\kilo\metre}).
	
	A door is opened at the bottom of the tower, so that at zero altitude the air pressure (\SI{1}{\bar}) is identical inside and outside of the tower.\\
	For the purpose of the exercise, we pretend the tower is entirely hermetic (meaning air is prevented from flowing in or out of its windows).
		\begin{enumerate}
			\shift{1}
			\item What is the pressure difference between each side of the windows in the apartment at the top of the tower?
		\end{enumerate}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage	
\subsubsection*{Answers}
\NumTabs{1}
\begin{description}
	\item [\ref{exo_basic_pressure}]%
				\tab $p_\A = p_\atm + \SI{0,039}{\bar} \approx \SI{1,039}{\bar}$.
	\item [\ref{exo_utube}]%
					\tab 1) $p_\text{inside} = p_\atm + \SI{0,0157}{\bar} \approx \SI{1,0157}{\bar}$;
					\tab 2) $\Delta z_2 = \SI{1,1765}{\centi\metre}$. Is having both of those results right enough to call yourself a U-tube star?
	\item [\ref{exo_straight_water_tank_door}]%
					\tab 1) $F_\net = \rho g L \left(Z_\max L_\max - \frac{1}{2} L_\max^2\right) = \SI{176,6}{\kilo\newton}$;
					\tab 2) $M_\text{net, bottom hinge} = \rho g L \left(\frac{1}{2} Z_\max L_\max^2 - \frac{1}{3} L_\max^3 \right) = \SI{176,6}{\kilo\newton\metre}$ so the force exerts at $R = M_\net/F_\net = \SI{1}{\metre}$ above the bottom hinge.
	\item [\ref{exo_water_canal_access_door}]%
					\tab 2) $M_\net = \SI{7,79}{\kilo\newton\meter}$;
					\tab 3) observe the equation used to calculate $M_\net$ to answer this question. If needed, ask for help in class!
	\item [\ref{exo_pressure_force_cylinder}]%
					\tab 1) $F_\text{front} = \int_{\theta = \pi/2}^{\theta = 3\pi/2} R L p_s \cos \theta \diff \theta = \SI{4998}{\newton}$ in downstream direction;
					\tab 2) $F_\text{top} = \int_{\theta = 0}^{\theta = \pi} R L p_s \sin \theta \diff \theta = \SI{4990}{\newton}$ downwards (note that this is less than the force exerting due to $p_\infty$ only when there is no flow).
	\item [\ref{exo_buoyancy_barge}]%
					\tab 2) $F_\text{rear} = \SI{0,2453}{\mega\newton}$, $F_\text{side} = \SI{2,1714}{\mega\newton}$, $F_\text{bottom} = \SI{13,734}{\mega\newton}$, $F_\text{front} = \SI{0,3468}{\mega\newton}$;
					\tab 3) $F_\text{buoyancy} = \SI{13,979}{\mega\newton}$ (\SI{1425}{\tonne}).
	\item [\ref{exo_burj_khalifa}]
					\tab 1) $\frac{p_2}{p_1} = \left(1 + \frac{k z_2}{T_1}\right)^{\frac{g}{k R}}$\\
					\tab 2) $\Delta p = \SI{+247,4}{\pascal}$ inwards.
\end{description}

\atendofexercises

\renewcommand{\lastedityear}{2018}
 \renewcommand{\lasteditmonth}{04}
   \renewcommand{\lasteditday}{02}
\renewcommand{\numberofthischapter}{9}
\renewcommand{\titleofthischapter}{Compressible flow}
\atstartofexercises

\fluidmechexercisestitle

\mecafluboxen

\mecafluexboxen

\setcounter{subsubsection}{0} %unexplained bug in subsubsection numbering in last chapter (carries on from previous chapter)
\begin{boiboite}
	The speed of sound $c$ in a perfect gas is a local property expressed as:
		\begin{IEEEeqnarray}{rCl}
			c &=& \sqrt{\gamma R T} \ztag{\ref{eq_speed_sound_gamma_rt}}
		\end{IEEEeqnarray}

	In isentropic, one-dimensional flow, we accept that the mass flow~$\dot m$ is quantified as:
	\begin{IEEEeqnarray}{rCl}
		\dot m &=& A \ma p_0 \sqrt{\frac{\gamma}{R T_0}} \left[1 + \frac{(\gamma - 1) \ma^2}{2}\right]^\frac{-\gamma - 1}{2 (\gamma - 1)} \ztag{\ref{eq_compressible_massflow_general}}
	\end{IEEEeqnarray}
	This expression reaches a maximum~$\dot m_\max$ when the flow is choked:
	\begin{IEEEeqnarray}{rCl}
		\dot m_\max &=& \left[\frac{2}{\gamma + 1}\right]^\frac{\gamma + 1}{2 (\gamma - 1)} {A^{*}} p_0 \sqrt{\frac{\gamma}{R T_0}} \ztag{\ref{eq_compressible_massflow_maximum}}
	\end{IEEEeqnarray}

	The properties of air flowing through a converging-diverging nozzle are described in \cref{fig_cengel_isentropic_expansion_table}, and \cref{fig_cengel_shock_table} describes air property changes through a perpendicular shock wave.
\end{boiboite}

		\begin{figure}[h]
			\begin{center}
					%\vspace{-0.5cm}
				\includegraphics[width=0.9\textwidth]{cengel_isentropic_expansion_table}
					\vspace{-0.5cm}
			\end{center}
			\supercaption{Properties of air (modeled as a perfect gas) as it expands through a converging-diverging nozzle.\\
			These are numerical values for equations (\ref{eq_hor_m_one}) to (\ref{eq_hor_m_three}). In this figure, the perfect gas parameter $\gamma$ is noted $k$. Data also includes the parameter $\ma^* \equiv V/c^*$ (speed non-dimensionalized relative to the speed of sound at the throat).}{Figure \copyright\xspace Çengel \& Cimbala 2010 \cite{cengelcimbala2010}}
			\label{fig_cengel_isentropic_expansion_table}
		\end{figure}
		\begin{figure}[h]
			\begin{center}
					%\vspace{-0.5cm}
				\includegraphics[width=0.9\textwidth]{cengel_shock_table}
					\vspace{-0.5cm}
			\end{center}
			\supercaption{Properties of air (modeled as a perfect gas) as it passes through a perpendicular shock wave.\\
			These are numerical values calculated based on equation~\ref{eq_m_normal_shock}. In this figure the perfect gas parameter $\gamma$ is noted $k$.}{Figure \copyright\xspace Çengel \& Cimbala 2010 \cite{cengelcimbala2010}}
			\label{fig_cengel_shock_table}
		\end{figure}



\subsubsection{Flow in a converging nozzle}
\label{exo_tire_leak}
\wherefrom{Çengel \& al. \smallcite{cengelcimbala2010} E12-5}

	Air from a \SI{4}{\milli\metre} hole in a car tire leaks into the atmosphere. The pressure and temperature in the tire are \SI{314}{\kilo\pascal} and \SI{25}{\degreeCelsius}. The exterior air conditions are \SI{94}{\kilo\pascal} and~\SI{16}{\degreeCelsius}.
	
	If the flow can be approximated as isentropic, what is the air flow rate?

%%%%%
\subsubsection{Flow in a converging-diverging nozzle}
\label{exo_classical_conv_div}
\wherefrom{Exam \textsc{ws}2015-16, see also White \smallcite{white2008} E9.4}

	An experiment is conducted where compressed air from a large reservoir is expanded and accelerated through a nozzle (figure~\ref{fig_nozzle}). There is no external work transfer to or from the air, and the expansion can satisfactorily be modeled as a one-dimensional isentropic process (isentropic meaning that the process is fully reversible, i.e. without losses to friction, and adiabatic, i.e.  without heat transfer).
	
	\begin{figure}[h]
		\begin{center}
			\includegraphics[width=\textwidth]{converging_diverging}
		\end{center}
		\caption{A nozzle in which compressed air in a reservoir (left) is expanded as it accelerates towards the right. The back pressure $p_\text{b}$ is controlled by a valve (represented on the right). The flow conditions of interest in this exercise are labeled~1, 2~and~3.}
		\label{fig_nozzle}
	\end{figure}
	
	The air ($c_{p \text{air}} = \SI{1005}{\joule\per\kilogram\per\kelvin}$) flows through a converging-diverging nozzle. The entrance conditions into the nozzle are $A_1 = \SI{0,15}{\metre\squared}$, $p_1 = \SI{620}{\kilo\pascal}$, $V_1 = \SI{180}{\metre\per\second}$, and~$T_1 = \SI{430}{\kelvin}$.
	
	In the expanding section of the nozzle, we wish to know the conditions at a point~2 of cross-sectional area~$A_2 = A_1 = \SI{0,15}{\metre\squared}$.
	

	\begin{enumerate}
		\item What is the mass flow through the nozzle?
		\item If we wish the flow to become sonic (i.e. reach $\ma = 1$), what is approximately the required throat~area $A_\text{T}$?
		\item Which two flow regimes can be generated at section 2? What is approximately the pressure~$p_2$ required for each?
	\end{enumerate}
	
	%\oneline%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	The nozzle is now operated so that the flow becomes sonic at the throat, and accelerates to supersonic speeds in the expanding section of the nozzle.
	
	The nozzle cross-sectional area increases further downstream of point 2, up until it reaches an area~$A_3 = \SI{0,33}{\metre\squared}$. Downstream of point~3, the duct is of constant area and the back pressure $p_\text{b}$ is set to $p_\text{b} = p_3 = p_2$.
	
	\begin{enumerate}
		\shift{3}
		\item Describe qualitatively (i.e. without numerical data) the pressure distribution throughout the nozzle.
	\end{enumerate}

	%\oneline%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	Finally, we wish to expand the air with a fully-isentropic process, expanding the air from the reservoir to point $3$ without a shock wave.

	\begin{enumerate}
		\shift{4}
		\item Propose and quantify one modification to the experimental set-up that would allow an isentropic, supersonic expansion of the flow into the outlet of area $A_3$.
	\end{enumerate}

% converging-diverging
%given conditions, sonic throat, shock in expanding nozzle, 
%propose and quantify modification to the setup that would enable a fully-isentropic flow

%%%%
\subsubsection{Capsule re-entry}
\label{exo_soyuz}
\wherefrom{\cczero \oc}

	A \textit{Soyuz} capsule incoming from the international space station is passing through Mach~\num{3,5} at a pressure of \SI{0,2}{\bar} and temperature of \SI{-50}{\degreeCelsius}. A bow shock wave is generated in front of the bottom surface of the capsule, which is approximately normal to the flow for the particles directly in the path of the capsule.
	
	What are the conditions (temperature, velocity, density and pressure) in between the shock wave and the capsule shield?


%%%%
\subsubsection{Explosion shock wave}
\label{exo_exploding_tesla}
\wherefrom{\cczero \oc}

In an unfortunate coincidence, on the morning following the fluid dynamics exam, the red Tesla~S of the lecturer explodes on a parking lot (fortunately, no one was hurt). The explosion creates an spherical shock wave propagating radially into still air.

At a given moment, the pressure inside the spherical bubble is \SI{1380}{\kilo\pascal}. The ambient atmospheric conditions are \SI{17}{\degreeCelsius}.

At what speed does the shock wave propagate, and what is the air speed immediately behind the wave?


%%%
\subsubsection{Home-made supersonic tunnel}
\label{exo_home_made_supersonic_tunnel}
\wherefrom{\cczero \oc}

A PhD student is thinking about constructing his/her own supersonic wind tunnel. To this purpose, s/he designs a converging-diverging tube with variable square cross-section. The throat~T is $\num{0,1} \times \SI{0,1}{\metre}$; the tunnel expands progressively to an outlet nozzle of~$\num{0,13} \times \SI{0,13}{\metre}$ cross-section. The outlet exits directly in the ambient atmosphere. On the inlet side of the tunnel, the student hopes to install a very large concrete reservoir (\cref{fig_nozzle_two}).
	
	\begin{figure}
		\begin{center}
			\includegraphics[width=\textwidth]{converging_diverging_ex2}
		\end{center}
		\supercaption{A nozzle in which compressed air in a reservoir (left) is expanded as it accelerates towards the right. The back (outlet) pressure $p_\text{b}$ is always atmospheric.}{}
		\label{fig_nozzle_two}
	\end{figure}

\begin{enumerate}
	\item What is the maximum reservoir pressure for which the flow in the tunnel is subsonic everywhere?
	\item What reservoir pressure would be required to generate a shock-less expansion to supersonic flow in the tunnel?
	\item If the outlet temperature was to be ambient (\SI{11,3}{\degreeCelsius}), what would the outlet velocity be?
	\item Which mass flow rate would then be passing in the tunnel?
	\item Describe qualitatively (i.e. without numerical data) the pressure distribution throughout the nozzle for both flow cases above.
\end{enumerate}

	In order to impress his/her friends, the PhD student wishes to generate a perpendicular shock wave inside the tunnel at exactly one desired position (labeled 1 in \cref{fig_nozzle_two} above). At this position, the cross-section area is~\SI{0,01439}{\metre\squared}.
	
\begin{enumerate}
	\shift{5}
	\item Describe qualitatively (i.e. without numerical data) the pressure distribution throughout the nozzle for this case, along the two other pressure distributions.
	\item \textit{[difficult question]} What reservoir pressure would be required to generate the shock wave at exactly the desired position?
	\item What would the outlet velocity approximately be?
	%TODO One new or alternate question could be to ask for mass flow again (it is unchanged since nozzle is still choked)
\end{enumerate}



\clearpage
\subsubsection*{Answers}
\NumTabs{2}
\begin{description}
	\item [\ref{exo_tire_leak}]%
				\tab Since $p_2 / p_0 < \num{0,5283}$, the flow is choked. Since there is no nozzle expansion, the flow cannot accelerate beyond $\ma = 1$ and it will exit the hole with excess pressure. With equation~\ref{eq_compressible_massflow_maximum} we obtain $\dot m = \SI{9,232e-3}{\kilogram\per\second}$ (\SI{0,55}{\kilogram\per\minute}).
	\item [\ref{exo_classical_conv_div}]%
				\tab 1) $\dot m = \dot m_1 = \rho_1 V_1 A_1 = \SI{135,6}{\kilogram\per\second}$;
				\tab\tab 2) To position point 1 in \cref{fig_cengel_isentropic_expansion_table} we can either calculate $\ma_1 = V_1/a_1 = \num{0,433}$ or $T_1/T_0 = T_1 / \left(T_1 + \frac{1}{2} \frac{1}{c_p} V_1^2\right) = \num{0,9639}$. Either way we read $A_1/A^{*} \approx \num{1,5901}$, which gets us the throat area required for sonic flow: $A^* = \SI{9,433e-2}{\metre\squared}$;
				\tab 3) We know the flow is isentropic, so at 2 we can have either $p_2 = p_1$ (flow subsonic everywhere), or $p_2 \approx \SI{1,0446e5}{\pascal}$ (flow becomes supersonic, $p_2$ obtained from \cref{fig_cengel_isentropic_expansion_table} after inputing $A_2/A^*$);
				\tab\tab 4) This would be the third case in \cref{fig_converging_diverging_nozzle} p.~\pageref{fig_converging_diverging_nozzle};
				\tab 5) The easiest answer is to propose lowering $p_3$ to $\num{0,0368} p_0$. But we could also decrease $A_T$ so that $A^* = A_3 / \num{3,5}$ or increase  $p_0$ to $1/\num{0,0368} p_3$.
	%\item[\ref{exo_exploding_tesla}]
	\item[\ref{exo_soyuz}]%
				\tab All of the answers are listed in \cref{fig_cengel_shock_table}. In 1-D flow, there is only one possible downstream solution. Interpolating between $\ma_1 = \num{3}$ and $\ma_1 = \num{4}$, we get $\ma_2 = \num{0,4551}$, $p_2 = \SI{2,883}{\bar}$ (!!), $\rho_2 = \SI{1,316}{\kilogram\per\metre\cubed}$, $T_2 = \SI{750,4}{\kelvin}$ (\SI{477}{\degreeCelsius}!) and $V_2$ a mere \SI{249,9}{\metre\per\second} (\SI{900}{\kilo\metre\per\hour}), down from $V_1 = \SI{1048}{\metre\per\second}$. Re-entering from space flight is tough!
	\item[\ref{exo_home_made_supersonic_tunnel}]%
			\tab 1) Flow subsonic everywhere: $p_0 = \SI{1,103}{\bar}$;
			\tab 2) Flow supersonic in diverging section: $p_0 = \SI{7,8247}{\bar}$;
			\tab 3) We would have $\ma_\b = \num{2}$ and $V_\b = \SI{676,1}{\metre\per\second}$;
			\tab 4) $\dot m = \SI{13,98}{\kilogram\per\second}$;
			\tab\tab 7) Before shock, $\ma_1 = \num{1,8}$; after it, $\ma_2 = \num{0,6165}$. Now, expand the post-shock flow subsonically to outlet condition to find $p_\b/p_{02} \approx \num{0,843}$. From $p_{02}$, go back upstream through the shock: $p_{01} = p_\text{tank} = \SI{1,4596}{\bar}$;
			\tab 8) From $T_0$, find $T_1$ (isentropic expansion), then $T_2$ (through shock), then $T_\b$ (subsonic isentropic expansion): $T_\b \approx \SI{487,4}{\kelvin}$. Now, $V_\b = \ma_\b \sqrt{\gamma R T_\b} \approx \SI{221,3}{\metre\per\second}$.
\end{description}

\atendofexercises

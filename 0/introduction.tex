\newgeometry{left=3cm,right=3cm,top=3cm,bottom=2cm,marginparwidth=0cm,marginparsep=0cm}
\setlength{\fancyfootoffset}{1cm}%
\fancyfoot{}

\begin{center}
\phantomsection
\addcontentsline{toc}{chapter}{Contents}
	\includegraphics[width=\textwidth]{course_layout}
\end{center}

\clearpage

\dominitoc % not actually printing a minitoc, just preparing minitocs for later inclusion
\hypersetup{linkcolor=black}				% make links black
\tableofcontents
\hypersetup{linkcolor=externallinkcolor}	% back to normal link color
\clearpage

\phantomsection
\addcontentsline{toc}{chapter}{About this course (syllabus)}

\defaultfooter
\begin{center}
	{\LARGE About this course (syllabus)}\par
	
	Fluid dynamics for engineers by Olivier Cleynen\\
	\href{https://fluidmech.ninja/}{https://fluidmech.ninja/}\\
	Winter semester 2020-2021
	
	
\vspace{0em}
\end{center}

Welcome to the Fluid Dynamics course of the \textit{Chemical and Energy Engineering} program! My name is Olivier Cleynen and I am the teacher for this course.

\section*{Objectives}

	Starting with little or no experience with fluid mechanics, after taking this course:
	\begin{itemize}
		\item you should have a good understanding of what can, an cannot, be calculated with fluid mechanics in engineering: how we approach problems depending on how much information is available.
		\item you should be able to solve several real-world engineering fluid mechanics problems with confidence: calculating forces within fluids and on objects, predicting flow in pipes, near walls, at small and large scales.
	\end{itemize}

	My objective is to enable you to get there with the minimum amount of your time and energy (but not minimum power!).
	
	If all goes well, at the end of the semester, you should be well-prepared to begin a course in \vocab{Computational Fluid Dynamics}, where the knowledge and skills you acquire here can be used to solve applied problems in great detail.


\section*{This is a summer course}

	This course only takes place in the summer semester. You are welcome to work on the content and attempt the final examination in the winter semester. However, I am not able to provide support during this time, and there are no classes allocated to the course, either offline or online.

	There are two venues for the course during the winter:
	\begin{itemize}
		\itme The course’s homepage at \href{https://fluidmech.ninja/}{https://fluidmech.ninja/} contains the complete course material;
		\item You can use the course’s Moodle page at \href{https://elearning.ovgu.de/course/view.php?id=7199}{https://elearning.ovgu.de/course/view.php?id=7199} to take quizzes and communicate with other students.
	\end{itemize}
	
	\begin{comment}
	This course is run entirely online. I will release one chapter per week (one large \pdf document with theory, problems, and videos). We will go together through weekly quizzes, graded homework programs, and a final exam.
	
	I am new to online learning and teaching, as perhaps you are too. As I write these lines, we are in the middle of a global pandemic. I hope you and your loved ones are safe, and that taking part in this course enables you to remain so. I am convinced that online fluid dynamics can be a lot of fun — I’ll do my very best for this!
	
	We will have several venues to communicate:
	\begin{itemize}
		\item I write an email to every student every week, to announce course events;
		\item The course’s Moodle page at \href{https://elearning.ovgu.de/course/view.php?id=7199}{https://elearning.ovgu.de/course/view.php?id=7199} is used for weekly quizzes and forum discussions;
		\itme The course’s homepage at \href{https://fluidmech.ninja/}{https://fluidmech.ninja/} contains the course material, released every week;
		\item I am available for questions and answers every Friday on Zoom (room 959-0526-7229, password released by email)
	\end{itemize}
	\end{comment}

\clearpage
\section*{About Olivier and colleagues}

	\begin{wrapfigure}[7]{R}[0pt]{0.4\textwidth}\vspace{-\intextsep}\includegraphics[width=0.4\textwidth]{olivier}\vspace{-\intextsep}\end{wrapfigure}
	Hello! I am a PhD student here at the University Otto von Guericke of Magdeburg. Most of my work consists in calculating the performance of low-impact hydropower machines using computers. I obtained my Master’s in 2006, then went on to found and work for a non-profit organization, and then became a university teacher in France. I arrived in Magdeburg in 2015, and currently live here with my partner and her ten year-old child. It’s a pleasure and a privilege to be here!
	
	I am delivering this course on behalf of prof.\ Thévenin and the \href{http://www.lss.ovgu.de/}{fluid dynamics laboratory} (\textsc{isut-lss}) of the university. This year, I am lucky to be assisted by two former students of the course, Jochen König and Arjun Neyyathala.


\section*{Assessment (updated February 2021)}
\label{ch_assessment}

	The assessment consists of a final examination in February, the details of which are laid out in Appendix~\ref{ch_exam} p.~\pageref{ch_exam}.

	The examinations from 2019 and 2020 are presented in Appendix~\ref{ch_previous_exam} p.~\pageref{ch_previous_exam}, with their full solution. I encourage you to take a look through them, to see how they are built. The structure of the February 2021 oral exam is very similar to the previous written exams, and it is described in Appendix~\ref{ch_exam} p.~\pageref{ch_exam}, with an accompanying video.
			
	If you have received coursework grades in a previous edition of this course, your grades will be carried forward. Coursework (a peer-graded individualized homework program developed with colleagues and former students~\cite{cleynenetal2020peergraded}) may resume in the summer semester 2021.

	\begin{comment}
	I know that assessment is important to you, and I take it very seriously. I strive to make the grading fair, motivating, and aligned with the course objectives. Your grade at the end of the semester will be determined according to the following components:
	
	\begin{description}
		\item[Weekly quizzes (10\%)] Every time a chapter is released, you are requested to answer low-level questions about its content, through the Moodle course page. You can take the quiz at your convenience, within one week of the chapter release. The quiz is time-limited to one hour; you must take it alone, and you can use all the documentation you wish. The average grade resulting from all your quizzes will count \SI{10}{\percent} towards your final grade.
		\item[Homework (40\%)] Three times during the semester, you will receive one unique assignment by email; you will have one week to submit your answer. Once you do, you will be expected to grade anonymized assignments from two other students, with the help of a worked-out answer sheet. Your own work will be graded anonymously by two other students.\\
		This homework program was developed in collaboration with former student Germán Santa-Maria in 2019.~\cite{cleynenetal2020peergraded} The average grade resulting from your three homework answers will count \SI{50}{\percent} towards your final grade.
		\item[Final exam (50\%)] At the end of the semester, we will have a two-hour, closed-book examination (a formula sheet is provided, containing the formulas and data contained in the preamble of every problem sheet). The examination consists exclusively of lightly-modified exercise sheet problems. As reported by students in the previous years, “the exam is very hard if you haven’t done the exercises, and very easy if you have done them”.\\
			The examination from 2019 is presented in Appendix~\ref{ch_previous_exam} p.~\pageref{ch_previous_exam}, with its full solution. I encourage you to take a look through it, to see how it is built.\\
			The structure of the 2020 final exam is described in Appendix~\ref{ch_exam} p.~\pageref{ch_exam}, with an accompanying video.
	\end{description}
	
	You can miss one homework assignment and one quiz during the semester without penalty, no questions asked. Life happens. I too have a life outside of fluid dynamics~(!) and I will certainly goof up a least once during the semester. If you need more accommodations, you should absolutely contact me.
	
	\textbf{For additional credit}, optionally, you can help expand the video content of this course. I would love to see students making a 5- to 10-minute video about a specific aspect of a chapter, to complement my coverage. I would also welcome the addition of useful figures, conceptual maps, doodles, comics, diagrams etc.\ to the course script. Please contact me if interested.
	\end{comment}
	
		
\section*{Accessibility}

	I have little experience with accessibility, but I think about it a lot~\cite{cleynen2020accessibility}. I provide closed captions (subtitles) for all the videos embedded in the course notes. I do not know what else may be helpful. If you have difficulties accessing the audio, video, or text in this course, let me know what I can do to help.


\section*{What you need for this course}

	\begin{description}
		%\item[Bandwidth] A stable internet connection. There are many YouTube videos embedded in the course script, and reliable bandwidth helps with talking to me on Zoom, too.
		\item[A laptop] I understand you love your smartphone, but it’s much harder to take notes and do homework on it. If you need to purchase good-quality equipment on a budget, \href{https://ariadacapo.net/blog/2020-02-02-students-guide-to-buying-a-laptop-in-europe/}{here’s a guide I co-authored with my colleague Samuel Voß}~\cite{cleynenvoss2020laptop}.
		%\item[A Zoom client] To participate in Q\&A on Fridays with me, you need the Zoom app installed on your favorite device, and to connect with your university account.
		\item[A book?] \emph{You do not need books for this course}. The eleven chapters of the course script, together, are all you need to work and succeed through the entire course.\\
	If, nevertheless, you feel more comfortable working with a book, I recommend those of Crowe~et~al.~\cite{croweetal2001}, White~\cite{white2008}, Çengel et~al.~\cite{cengelcimbala2010}, Munson et~al.~\cite{munsonetal2013}, or (my favorite) de~Nevers~\cite{denevers2004}. All of them are solid, beautifully-illustrated, well-established textbooks, with plenty of worked-out problems. You will see that this course matches closely their content and style.
		\item[Self-discipline] The hardest part of learning online (especially in the winter semester, without any classes!) is that there is no social pressure to keep you going. I strongly recommend you assign a fixed weekly time schedule to work on this course, and communicate with peers in the Moodle course page, to stay in the loop.
	\end{description}

\begin{comment}
\section*{Names}

	By default, I will address you using the first name and gender given in your university record. If you wish to change that, I will be happy to use your preferred name and pronouns: just let me know.
	
	I like to be addressed as just “Olivier” (pronouns: he/him/his). You can use my last name if that makes you more comfortable. Note that I am neither a professor nor (yet) a doctor~:-)
\end{comment}

\section*{Contact}
\label{ch_contact}

	%You can join me on Zoom on Fridays from 13:00 to 15:00, in room 959-0526-7229 (password released by email).

	%My email is olivier.cleynen@ovgu.de. I strive to answer all requests within 48 hours. However, I am alone facing emails from 153 students, so if you need help with problems, the Moodle forum pages might be a better place to start (I check those out every weekday, and other students can help you there too). I am always willing to receive feedback about the course, and hear about technical and human problems, so don’t hesitate to get in touch.
	
	My email is olivier.cleynen@ovgu.de. I am available to answer queries about the course structure, the examination logistics, and personal matters. Unfortunately, I cannot provide assistance with the course itself (\eg how to solve the end-of-chapter problems) during the winter semester. If you would like such assistance, please come back in the summer semester!



\section*{Time plan}
\label{ch_timeplan}

\begin{comment}
\TabPositions{1.8cm, 11cm}

	Our time plan (updated June 25) should be as follows:

	April 23: 	\tab \chapterone\\
	April 30: 	\tab \chaptertwo, 		\tab \textcolor{vocabcolor}{Homework \#1 begins}\\
	May 7: 		\tab \chapterthree, 	\tab \textcolor{vocabcolor}{Homework \#1 due}\\
	May 14: 	\tab no new chapter\\
	May 21: 	\tab \chapterfour, 		\tab \textcolor{vocabcolor}{Homework \#2 begins}\\
	May 28: 	\tab \chapterfive, 		\tab \textcolor{vocabcolor}{Homework \#2 due}\\
	June 4: 	\tab no new chapter\\
	June 11: 	\tab \chaptersix\\
	June 18: 	\tab no new chapter\\
	June 25: 	\tab \chapterseven, 	\tab \textcolor{vocabcolor}{Homework \#3 begins}\\
	July 2: 	\tab \chaptereight, 	\tab \textcolor{vocabcolor}{Homework \#3 due}\\
	July 9: 	\tab \chapterten\\
	\chapternine and \chaptereleven are not part of the course this semester.\\
	September 21: Final Examination

\TabPositions{6cm} % default
\end{comment}

	The complete course content is already available online. The final exam will take place in February 2021.

\section*{Copyright, remixing, and authors}

	This document is mainly authored by Olivier Cleynen. Substantial contributions have been made by colleagues Germán Santa-Maria, Jochen König, and Arjun Neyyathala. Numerous improvements have been contributed by students over the years.\\
	Many figures from authors not associated with this course are included; the author, license, and a link to the source are indicated every time. A few figures still remain which are extracted from cited, fully-copyrighted works, as indicated.\\
	Most portrait illustrations are authored by Oksana Grivina; they are fully-copyrighted and used under a commercial license in this project. Other portrait illustrations are authored by Agustin Dede Prasetyo and Olivier Cleynen under a \ccby license.
	
	The text of this document is licensed under a Creative Commons \ccbync (attribution, non-commercial) license. The Latex sources can be downloaded from the git repository accessed from the course homepage.~\cite{cleynen2020fluidmechgit}
	
	If you use this document in other works, please cite it as “Olivier Cleynen. \textit{Fluid dynamics for engineers}. Under \ccbync license. 2020. \textsc{url}: \href{https://fluidmech.ninja/}{\textcolor{black}{https://fluidmech.ninja}}”.

\section*{Conclusion}

\begin{center}
	\href{https://youtu.be/NJfiIGOADK0}{\includegraphics[width=0.6\textwidth]{welcome_video}}

	\textit{\href{https://youtu.be/NJfiIGOADK0}{Welcome to this course!}}\\
	(I recorded this YouTube video in April 2020. Everything remains true for the winter semester 2020-2021, except for the part about coursework)\par
\end{center}


%It’s a pleasure to join you for this course this semester! Fluid mechanics is one of the most exciting disciplines out there. Now, let us begin!
I hope you have a great semester! Fluid mechanics is one of the most exciting disciplines out there. Now, let us begin!
	
	\begin{flushright}
	Olivier Cleynen\\
	October 2020
	\end{flushright}

\restoregeometry\restoredefaultfootoffset\cleardoublepage

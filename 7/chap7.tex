\renewcommand{\lastedityear}{2020}
 \renewcommand{\lasteditmonth}{09}
   \renewcommand{\lasteditday}{03}
\renewcommand{\numberofthischapter}{7}
\renewcommand{\titleofthischapter}{\namechapterseven}

\fluidmechchaptertitle
\label{chap_seven}

\mecafluboxen

\section{Motivation}

	In this chapter we focus on fluid flow in pipes. This topic allows us to explore several important phenomena with only very modest mathematical complexity. In particular, we are trying to answer two questions:
	\begin{enumerate}
		\item What does it take to describe fluid flow in ducts?
		\item How can we quantify pressure changes in pipes and the power necessary to overcome them?
	\end{enumerate}


\section{Frictionless flow in pipes}

	We begin with the simplest possible ducted flow case: a purely hypothetical fully-inviscid, incompressible, steady fluid flow in a one-dimensional pipe. Since there are no shear forces, the velocity profile across the duct remains uniform (flat) all along the flow, as shown in figure~\ref{fig_inviscid_flow_pipe}.

		\begin{figure}
			\begin{center}
				\includegraphics[width=0.9\textwidth]{fluid_flow_in_duct_inviscid}
			\end{center}
			\supercaption{Inviscid fluid flow in a one-dimensional duct. In this purely hypothetical case, the velocity distribution is uniform across a cross-section of the duct. The average velocity and pressure change with cross-section area, but the total pressure $p_0 = p + \frac{1}{2}\rho V_\av^2$ remains constant.}{\wcfile{Flow in pipes of changing section, static and total pressure, with and without friction losses.svg}{Figure} \cczero \oc}
			\label{fig_inviscid_flow_pipe}
		\end{figure}

	If the cross-sectional area $A$ is changed, then the principle of mass conservation (eqs.~\ref{eq_massconservation}, \ref{eq_rtt_mass}) is enough to allow us to compute the change in velocity $u = V$:
			\begin{IEEEeqnarray}{rCl}
				\rho V_1 A_1 &=& \rho V_2 A_2 \label{eq_mass_conservation_pipe_flow}
			\end{IEEEeqnarray}
			\begin{equationterms}
				\item in steady pipe flow.
			\end{equationterms}
	
	\agthumb{517}{Daniel Bernoulli was part of a big family\wupen{Bernoulli family}}{0.8}{}
	This information, in turn allows us to compute the pressure change between two sections of different areas by using the principle of energy conservation. We notice that the flow is so simple that the five conditions associated with the use of the Bernoulli equation (see \S\ref{ch_bernoulli} p.~\pageref{ch_bernoulli}) are fulfilled: the flow is steady, incompressible, one-dimensional, has known trajectory, and does not feature friction or energy transfer. A simple application of eq.~\ref{eq_bernoulli} p.~\pageref{eq_bernoulli} between any two points 1 and 2 gives us:
		\begin{IEEEeqnarray}{rCl}
			p_2 - p_1 &=& -\frac{1}{2} \rho \left[V_2^2 - V_1^2\right] -  \rho g (z_2 - z_1)
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item in steady, incompressible, inviscid pipe flow without heat or work transfer.
		\end{equationterms}
	Thus, in this kind of simple flow, pressure increases everywhere the velocity decreases, and vice-versa.
	
	Another way of writing this equation is by stating that at constant altitude, the \vocab{total} or \vocab{dynamic pressure} $p_\text{total} \equiv p_0 \equiv p + \frac{1}{2} \rho V^2$ remains constant:
		\begin{IEEEeqnarray}{rCl}
			p_0 &=& \cst
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item at constant altitude, in laminar inviscid straight pipe flow.
		\end{equationterms}

	Inviscid flows are nice, but real flows are more interesting. Real flows feature losses due to viscosity, and with viscous effects, one key assumption of the Bernoulli equation breaks down. An additional term will appear in the Bernoulli equation, as we have seen in \chaptertwoshort with equation~\ref{eq_bernoulli_losses} p.~\pageref{eq_bernoulli_losses}. What does this extra term $\Delta p_\text{loss}$ depend on, and how can we quantify it? This is what the rest of the chapter is about.


\section{Parameters to quantify losses in pipes}

	Hydraulics is the oldest branch of fluid dynamics, and much of the notation used to describe pressure losses predates modern applications. The most widely-used parameters for quantifying losses due to friction in a duct are the following:	
		\begin{description}
			\item[The pressure loss] is the most intuitive way of quantifying the net effect of friction in pipes. Engineers and physicists usually quantify $\Delta p_\text{loss}$ as a negative number (\ie $\Delta p_\text{loss} \equiv p_2 - p_1$ with $p_2<p_1$). However, in hydraulics, the historical precedent is to quantify pressure loss with a positive number. To make clear this convention, we will always refer to pressure loss as $|\Delta p_\text{loss}|$.
			
			\item[The elevation loss] which we note $|\Delta l|$ (in the literature, often noted $\Delta h$), is defined as
				\begin{IEEEeqnarray}{rCl}
					|\Delta l| &\equiv& \frac{|\Delta p_\text{loss}|}{\rho g}
				\end{IEEEeqnarray}
				It represents the hydrostatic height loss (with a positive number) caused by the fluid flow in the duct, and is measured in \si{meters}. The reference density $\rho$ in this definition is taken as the density of the fluid, the density of water, or the density of mercury, depending on cases. We do not use this definition in this course.
				
			\item[The Darcy friction factor] noted $f$ is defined as
				\begin{IEEEeqnarray}{rCl}
					f &\equiv& \frac{|\Delta p_\text{loss}|}{\frac{L}{D} \frac{1}{2} \rho V_\av^2} \label{eq_def_darcy}
				\end{IEEEeqnarray}
				\begin{equationterms}
					\item where $V_\av$ is the average flow velocity in the pipe.
				\end{equationterms}
				In pipe flows of relevance to the engineer, $f$ has values between \num{5e-5} and \num{5e-2}. Those values can be calculated or read from experimental data, as explained further down.
				
			\item[The loss coefficient] noted $K_L$ is defined as
				\begin{IEEEeqnarray}{rCl}
					K_L &\equiv& \frac{|\Delta p_\text{loss}|}{\frac{1}{2} \rho V_\av^2}	\label{eq_def_loss_coeff}
				\end{IEEEeqnarray}
				Components found in pipe networks, such as bends, filter screens, valves, or junction screens, all result in losses that remain roughly proportional to the square of the average velocity. Typically, $K_L$ values range between \num{0,3} (smooth bend) and \num{2} (partially-closed valve).
		\end{description}

	




\section{Laminar flow in pipes}

	\begin{comment}
	\subsection{The entry zone}

		Let us observe the velocity profile at the entrance of a symmetrical duct in a steady, laminar, viscous flow (\cref{fig_pipe_inlet}).
				\begin{figure}[ht!]
					\begin{center}
						\includegraphics[width=\textwidth]{Development_of_fluid_flow_in_the_entrance_region_of_a_pipe}
					\end{center}
					\supercaption{Fluid flow velocity distributions at the entrance of a duct. All throughout the entrance region, the core region of the flow is accelerated, and the outer region decelerated, even though the flow itself is steady.}{\wcfile{Development of fluid flow in the entrance region of a pipe.svg}{Diagram} \ccbysa by Wikimedia Commons user:Devender Kumar5908}
					\label{fig_pipe_inlet}
				\end{figure}
	
		The fluid enters the duct with a uniform (flat) velocity profile. Because of the no-slip condition at the wall (\S\ref{ch_no_slip_condition} p.~\pageref{ch_no_slip_condition}), the particles in contact with the duct walls are immediately stopped. Through viscosity, shear stress effects propagate progressively inwards. A layer appears in which viscosity effects are predominant, which we name \vocab{boundary layer}; this layer grows until it reaches the center of the duct. Past this point, the flow is entirely dictated by viscous effects and the velocity profile does not change with distance. The flow is then said to be \vocab{fully developed}.
	\end{comment}

	%%%%%%%
	\subsection{Laminar flow between plates}

		Before we study fluid flow in a \emph{circular} pipe, let us begin with a simpler case: flow between two parallel plates. This is a good place to start, because we can work with Cartesian coordinates, and focus on two dimensions only.
		
		Let us first try a qualitative description of the flow, as displayed in figure~\ref{fig_viscous_laminar_flow_in_duct}. Because of the no-slip condition at the walls, the velocity distribution within any cross-flow section cannot be uniform. Shear occurs, which translates into a pressure decrease along the flow. The faster the flow, and the higher the gradient of velocity. Thus, shear within the flow, and the resulting pressure loss, both increase when the cross-sectional area is decreased.
		\begin{figure}[ht!]
			\begin{center}
				\includegraphics[width=0.9\textwidth]{fluid_flow_in_duct_viscous_laminar}
			\end{center}
			\supercaption{Viscous laminar fluid flow in a one-dimensional pipe. This time, the no-slip condition at the wall creates a viscosity gradient across the duct cross-section. This in turn translates into pressure loss. Sudden duct geometry changes such as represented here would also disturb the flow further, but the effect was neglected here.}{\wcfile{Flow in pipes of changing section, static and total pressure, with and without friction losses.svg}{Figure} \cczero \oc}
			\label{fig_viscous_laminar_flow_in_duct}
		\end{figure}

		\clearfloats%handmade
		How can we now describe \emph{quantitatively} the velocity profile and the pressure loss? We need to clearly sketch the flow we are interested in, which we do in figure~\ref{fig_plates}. 
			\begin{figure}
				\begin{center}
					\includegraphics[width=0.8\textwidth]{velocity_distribution_couette_flow}
				\end{center}
				\supercaption{Two-dimensional laminar flow between two plates, also called \vocab{Poiseuille flow}. We already studied this flow case in \cref{fig_plates} p.~\pageref{fig_plates}; this time, we wish to derive an expression for the velocity distribution.}{\wcfile{Couette flow flat plate laminar velocity distributions.svg}{Figure} \cczero \oc}
				\label{fig_twoplates}
			\end{figure}
		
		We also need a powerful, extensive mathematical tool to describe the flow: we turn to the Navier-Stokes equation which we derived in the previous chapter as eq.~\ref{eq_navierstokes} p.~\pageref{eq_navierstokes}:
			\begin{IEEEeqnarray}{rCl}
				\rho \totaltimederivative{\vec V}  & = &	\rho \vec g - \gradient{p} + \mu \laplacian{\vec V}
			\end{IEEEeqnarray}

		\clearpage%handmade
		Since we are applying this tool to the simple case of fully-developed, two-dimensional incompressible fluid flow between two parallel plates (\cref{fig_twoplates}), we need only two Cartesian coordinates, so that the vector equation translates~to:
			\begin{IEEEeqnarray}{cCc}
				\rho \left[ \partialtimederivative{u} + u \partialderivative{u}{x} + v \partialderivative{u}{y} \right]  & = &	\rho g_x - \partialderivative{p}{x} + \mu \left[ \secondpartialderivative{u}{x} + \secondpartialderivative{u}{y} \right] \\
				\rho \left[ \partialtimederivative{v} + u \partialderivative{v}{x} + v \partialderivative{v}{y} \right]  & = &	\rho g_y - \partialderivative{p}{y} + \mu \left[ \secondpartialderivative{v}{x} + \secondpartialderivative{v}{y} \right]
			\end{IEEEeqnarray}

		In this particular flow, we have restricted ourselves to a fully-steady ($\inlinetimederivative{} = 0$), horizontal ($g = g_y$), one-directional flow ($v=0$). When the flow is fully developed, $\inlinepartialderivative{u}{x} = 0$ and $\inlinesecondpartialderivative{u}{x} = 0$, and the system above shrinks down~to:
			\begin{IEEEeqnarray}{rCl}
				0  & = &	- \partialderivative{p}{x} + \mu \left[\secondpartialderivative{u}{y} \right] \label{eq_tmp4} \\
				0	& = &	\rho g - \partialderivative{p}{y}
			\end{IEEEeqnarray}

		We only have to integrate equation~\ref{eq_tmp4} twice with respect to $y$ to come to the velocity profile across two plates separated by a height $2 H$:
			\begin{IEEEeqnarray}{rCl}
				u &=& \frac{1}{2 \mu} \left(\partialderivative{p}{x} \right) (y^2 - H^2)\label{eq_tmp5}
			\end{IEEEeqnarray}

		Now, the longitudinal pressure gradient $\inlinepartialderivative{p}{x}$ can be evaluated by working out the volume flow rate $\dot \vol$ for any given width~$Z$ with one further integration of equation~\ref{eq_tmp5}:
			\begin{IEEEeqnarray}{rCl}
				\frac{\dot \vol}{Z} &=& \frac{2}{Z} \int_0^H u Z \diff y  = -\frac{2 H^3}{3 \mu} \left(\partialderivative{p}{x} \right)\nonumber\\
				\partialderivative{p}{x} &=& -\frac{3}{2} \frac{\mu}{Z H^3} \dot \vol \label{eq_pressure_gradient_couette}
			\end{IEEEeqnarray}

		In this section, the overall process is more important than the result: by starting with the Navier-Stokes equations, and adding known constraints that describe the flow of interest, we can predict analytically all of the characteristics of a laminar~flow.


	\subsection{Laminar flow in pipes}

		We now turn to studying flow in \emph{cylindrical} pipes, which are widely used; first considering laminar flow, and then expanding to turbulent flow.

		The process is identical to above, only applied to cylindrical instead of Cartesian coordinates. We focus on the fully-developed laminar flow of a fluid in a cylindrical pipe without gravity (\cref{fig_cylinder_lam}).
		
			\begin{figure}
				\begin{center}
					\includegraphics[width=6cm]{pipe_flow_coordinates}
				\end{center}
				\supercaption{A cylindrical coordinate system to study laminar flow in a cylindrical duct.}{\wcfile{Pipe flow coordinate system.svg}{Figure} \cczero \oc}
				\label{fig_cylinder_lam}
			\end{figure}
		
		For this flow, we wish to work out the velocity profile and calculate the pressure loss related to the flow.
	
		\xkcdthumb{1230}{polar coordinates}{0.7}{}
		We once again start from the Navier-Stokes vector equation, choosing this time to develop it using \emph{cylindrical} coordinates:
			\begin{IEEEeqnarray}{lr}
				\rho \left[ \partialtimederivative{v_r} + v_r \partialderivative{v_r}{r} + \frac{v_\theta}{r} \partialderivative{v_r}{\theta} - \frac{v_\theta^2}{r} + v_z \partialderivative{v_r}{z} \right]  &\nonumber\\
				 \ \ \ \ \ = \	\rho g_r - \partialderivative{p}{r} + \mu \left[ \frac{1}{r} \partialderivative{}{r} \left(r \partialderivative{v_r}{r} \right) - \frac{v_r}{r^2} + \frac{1}{r^2} \secondpartialderivative{v_r}{\theta} -\frac{2}{r^2} \partialderivative{v_\theta}{\theta} + \secondpartialderivative{v_r}{z} \right] \nonumber \\
				\label{eq_ns_cyl_one}\\
				\rho \left[ \partialtimederivative{v_\theta} + v_r \partialderivative{v_\theta}{r} + \frac{v_\theta}{r} \partialderivative{v_\theta}{\theta} + \frac{v_r v_\theta}{r} + v_z \partialderivative{v_\theta}{z} \right]  &\nonumber\\
				 \ \ \ \ \ = \	\rho g_\theta - \frac{1}{r} \partialderivative{p}{\theta} + \mu \left[ \frac{1}{r} \partialderivative{}{r} \left(r \partialderivative{v_\theta}{r} \right) - \frac{v_\theta}{r^2} + \frac{1}{r^2} \secondpartialderivative{v_\theta}{\theta} + \frac{2}{r^2} \partialderivative{v_r}{\theta} + \secondpartialderivative{v_\theta}{z} \right] \nonumber \\
				\label{eq_ns_cyl_two}\\
				\rho \left[ \partialtimederivative{v_z} + v_r \partialderivative{v_z}{r} + \frac{v_\theta}{r} \partialderivative{v_r}{\theta} + v_z \partialderivative{v_z}{z} \right]  &\nonumber\\
				 \ \ \ \ \ = \	\rho g_z - \partialderivative{p}{z} + \mu \left[ \frac{1}{r} \partialderivative{}{r} \left(r \partialderivative{v_z}{r} \right) + \frac{1}{r^2} \secondpartialderivative{v_z}{\theta} + \secondpartialderivative{v_z}{z} \right] \nonumber\\\label{eq_ns_cyl_three}
			\end{IEEEeqnarray}
			
		This mathematical arsenal does not frighten us, for the simplicity of the flow we are studying allows us to bring in numerous simplifications. First, we have $g$ = 0. Second, we have $v_r = 0$ and $v_\theta = 0$ everywhere. Thus, by continuity, $\partial v_z / \partial z = 0$.\\
		Furthermore, since our flow is symmetrical, $v_z$ is independent from $\theta$. With these two conditions, the above system shrinks down to:
				\begin{IEEEeqnarray}{rCl}
					0 &=& 0 \label{eq_tmp1}\\
					0 &=& 0 \label{eq_tmp2}\\
					0 &=& \	- \partialderivative{p}{z} + \mu \left[ \frac{1}{r} \partialderivative{}{r} \left(r \partialderivative{v_z}{r} \right)\right] \label{eq_tmp3}
				\end{IEEEeqnarray}

		Now, with equation~\ref{eq_tmp3}, we work towards obtaining an expression for $v_z$ by integrating twice our expression for $\partial v_z / \partial r$:
			\begin{IEEEeqnarray}{rCl}
					\partialderivative{}{r} \left(r \partialderivative{v_z}{r} \right) &=& \frac{r}{\mu} \partialderivative{p}{z} \nonumber\\
					\left(r \partialderivative{v_z}{r} \right) &=& \frac{r^2}{2\ \mu} \left(\partialderivative{p}{z} \right) + k_1 \nonumber\\
					v_z &=& \frac{r^2}{4\ \mu} \left(\partialderivative{p}{z} \right) + k_1 \ln r + k_2 \label{eq_pipe_lam_tmp}
			\end{IEEEeqnarray}
	
		We have to use boundary conditions so as to unburden ourselves from integration constants $k_1$ and~$k_2$.\\
		By setting $v_{z @ r=0}$ as finite, we deduce that $k_1 = 0$ (because $\ln (0) \to -\infty$).\\
		By setting $v_{z @ r=R} = 0$ (no-slip condition), we obtain $k_2 = -\frac{R^2}{4\ \mu} \partialderivative{p}{z}$.

		This simplifies eq.~(\ref{eq_pipe_lam_tmp}) and brings us to our objective, an extensive expression for the velocity profile across a pipe of radius $R$ when the flow is laminar:
			\begin{IEEEeqnarray}{rCl}
				v_z = u_{(r)}	&=& - \frac{1}{4 \mu} \left(\partialderivative{p}{z} \right) (R^2 - r^2) \label{eq_u_lam}
			\end{IEEEeqnarray}
	
		This equation is parabolic (\cref{fig_speed_cylinder_lam}). It tells us that in a pipe of given length~$L$ and radius~$R$, a given velocity profile will be achieved which is a function only of the ratio~$\Delta p/\mu$.
		
			\begin{figure}[ht!]
				\begin{center}
					\includegraphics[width=0.8\textwidth]{velocity_distribution_laminar_pipe}
				\end{center}
				\supercaption{The velocity profile across a cylindrical pipe featuring laminar viscous flow.}{\wcfile{Pipe flow laminar velocity distributions.svg}{Figure} \cczero \oc}
				\label{fig_speed_cylinder_lam}
			\end{figure}
		
		We can also express the pressure gradient in the pipe as a function of the volume flow rate. This is done through integration of velocity with respect to density and cross-section area. We obtain:
			\begin{IEEEeqnarray}{rCl}
				\dot m 	&=& - \frac{\pi \rho D^4}{\num{128} \ \mu} \left(\partialderivative{p}{z} \right) \nonumber\\
				\Delta p_\text{loss} &=& -\frac{\num{128}}{\pi} \frac{\mu L \dot m}{\rho D^4} \label{eq_dotvol_lam}
			\end{IEEEeqnarray}

		This equation is interesting in several respects. For a given pipe length~$L$ and pressure drop~$\Delta p_\text{loss}$, the volume flow~$\dot \vol$ increases with the power~\num{4} of the diameter~$D$. In other words, the volume flow is multiplied by~\num{16} every time the diameter is doubled.
		
		We also notice that the pipe wall roughness does not appear in equation~\ref{eq_dotvol_lam}. In a laminar flow, increasing the pipe roughness has no effect on the velocity distribution in the pipe.
		
		Out of curiosity, we may translate the result in equation~\ref{eq_dotvol_lam} into a friction factor equation, with the help of definition~\ref{eq_def_darcy}, obtaining
	\begin{IEEEeqnarray}{rCcCcCl}
			f_\text{laminar cylinder flow} &=& \frac{32 V_\av \mu L}{\frac{L}{D} \frac{1}{2} \rho V_\av^2 D^2} &=& 64 \frac{\mu}{\rho V_\av \ D} &=& \frac{\num{64}}{\reD}\label{eq_f_pipe_laminar_flow}
	\end{IEEEeqnarray}
		\begin{equationterms}
			\item in which we inserted the average velocity $V_\av = \frac{\dot \vol}{\pi R^2} = -\frac{\Delta p \ D^2}{32 \mu L}$.
		\end{equationterms}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Turbulent flow in pipes}


	\subsection{When is a pipe flow turbulent?}

		\youtubethumb{56AyTIhNQBo}{very basic visualization of laminar and turbulent flow regimes in a transparent pipe}{Engineering Fundamentals (\styl)}
		It has long been observed that pipe flow can have different \vocab{regimes}. In some conditions, the flow is unable to remain laminar (one-directional, fully-steady); it becomes \vocab{turbulent}. Although the flow is steady when it is averaged over short time period (\eg a few seconds), it is subject to constant, small-scale, chaotic and spontaneous velocity field changes in all directions.
		\begin{figure}
			\begin{center}
				\includegraphics[width=8cm]{reynolds1}
			\end{center}
			\supercaption{Illustration published by Reynolds in 1883 showing the installation he set up to investigate the onset of turbulence. Water flows from a transparent rectangular tank down into a transparent drain pipe, to the right of the picture. Colored die is injected at the center of the pipe inlet, allowing for the visualization of the flow regime.}{\wcfile{Reynolds fluid turbulence experiment 1883.jpg}{Image} by Osborne Reynolds (1883, \pd)}
			\label{fig_reynolds_one}
		\end{figure}
		\begin{figure}
			\begin{center}
				\includegraphics[width=1\textwidth]{reynolds2}
			\end{center}
			\supercaption{Illustration published by Reynolds in 1883 showing two different flow regimes observed in the installation from \cref{fig_reynolds_one}.}{\wcfile{Reynolds observations turbulence 1883.svg}{Image} by Osborne Reynolds (1883, \pd)}
			\label{fig_reynolds_two}
		\end{figure}
		
		In 1883, \we{Osborne Reynolds} published the results of a meticulous investigation into the conditions in which the flow is able, or not, to remain laminar (\cref{fig_reynolds_one,fig_reynolds_two}). He showed that they could be predicted using a single non-dimensional parameter, later named \vocab{Reynolds number}, which, as we have seen already with eq.~\ref{eq_def_reynolds_number} p.~\pageref{eq_def_reynolds_number}, is expressed~as:
			\begin{IEEEeqnarray}{rCl}
					\re &\equiv& \frac{\rho \ V \ L}{\mu} \label{eq_def_reynolds_number_two}
			\end{IEEEeqnarray}
		In the case of pipe flow, the representative length $L$ is conventionally set to the pipe diameter $D$ and the velocity to the cross-section average velocity:
			\begin{IEEEeqnarray}{rCl}
					\reD &\equiv& \frac{\rho \ V_\av \ D}{\mu} \label{eq_def_red}
			\end{IEEEeqnarray}
			\begin{equationterms}
				\item where \tab $V_\av$ \tab is the average velocity in the pipe (\si{\metre\per\second}),
				\item and 	\tab $D$ \tab\tab is the pipe diameter (\si{\metre}).
			\end{equationterms}
	
		The occurrence of turbulence is very well documented. The following values are widely accepted:
		\begin{itemize}
			\item Pipe flow is laminar for $\reD \lesssim \num{2300}$ ;
			\item Pipe flow is turbulent for $\reD \gtrsim \num{4000}$.
		\end{itemize}
		
		The significance of the Reynolds number extends far beyond pipe flow; we shall explore this in \chaptereight.


	\subsection{Characteristics of turbulent flow}
	\coveredin{Tennekes \& Lumley \cite{tennekesetal1972}}
		
		\youtubethumb{5zI9sG3pjVU}{turbulence for those who don’t have time to read \chapternineshort}{Y:Veritasium (\styl)}
		Turbulence is a complex topic which is still not fully described analytically today. Although it may display steadiness when time-averaged, a turbulent flow is highly three-dimensional, unsteady, and chaotic in the sense that the description of its velocity field is carried out with statistical, instead of analytic, methods.
		
		In the scope of our study of fluid dynamics, the most important characteristics associated with turbulence are the following:
		\begin{itemize}
			\item A strong increase in mass and energy transfer within the flow. Slow and rapid fluid particles have much more interaction (especially momentum transfer) than within laminar flow;
			\item A strong increase in losses due to friction (typically by a factor 2). The increase in momentum exchange within the flow creates strong dissipation through viscous effects, and thus transfer (as heat) of macroscopic forms of energy (kinetic and pressure energy) into microscopic forms (internal energy, translating as temperature);
			\item Internal flow movements appear to be chaotic (though not merely \emph{random}, as would be white noise), and we do not have mathematical tools to describe them analytically.
		\end{itemize}
		
		Consequently, solving a turbulent flow requires taking account of flow in all three dimensions even for one-directional flow! We will come back to this topic in \chapternine.


	\subsection{Velocity profile in turbulent pipe flow}
	
		\youtubethumb{13tBWzKajqw}{highly-turbulent flow exiting the flood discharge ducts of the Tarbela dam in northeastern Pakistan}{Y:Beauty Of Pakistan (\styl)}
		In order to deal with the vastly-increased complexity of turbulent flow in pipes, we split each velocity component $v_i$ in two parts, a time-averaged component $\overline{v_i}$ and an instantaneous fluctuation $v’_i$:
		\begin{IEEEeqnarray*}{rCl}
				v_r 		&=& \overline{v_r} + v’_r\\
				v_\theta &=& \overline{v_\theta} + v’_\theta\\
				v_z 		&=& \overline{v_z} + v’_z
		\end{IEEEeqnarray*}
		In our case, $\overline{v_r}$ and $\overline{v_\theta}$ are both zero, but the fluctuations~$v’_r$ and~$v’_\theta$ are not, and will cause~$v_z$ to differ from the laminar flow case. The extent of turbulence is often measured with the concept of \vocab{turbulence intensity} $I$:
		\begin{IEEEeqnarray}{rCl}
				I \equiv \frac{\left[\overline{v_i'^2}\right]^{\frac{1}{2}}}{\overline{v_i}}
		\end{IEEEeqnarray}
		
		Regrettably, we have not found a general analytical solution to turbulent pipe flow — note that if we did, it would likely exhibit complexity in proportion to that of such flows. A widely-accepted average velocity profile constructed from experimental observations is:
		\begin{IEEEeqnarray}{rCl}
				\overline{u}_{(r)} = \overline{v_z} &=& \overline{v_{z~\text{max}}} \left(1 - \frac{r}{R} \right)^{\frac{1}{7}}
		\end{IEEEeqnarray}
	
		While it closely and neatly matches experimental observations, this model is nowhere as potent as an analytical one and must be seen only as an approximation. For example, it does not allow us to predict internal energy dissipation rates (because it describes only time-averaged velocity), or even wall shear stress (because it yields $(\partial\overline{u}/\partial r)_{r=R} = \infty$, which is not physical).
	
		The following points summarize the most important characteristics of turbulent velocity profiles:
		\begin{itemize}
			\item They continuously fluctuate in time and we have no means to predict them extensively;
			\item They are much “flatter” than laminar profiles (\cref{fig_speed_cylinder_turb});
			\item They depend on the wall roughness;
			\item They result in shear and dissipation rates that are markedly higher than laminar profiles.
		\end{itemize}
		
			\begin{figure}[ht!]
				\begin{center}
					\includegraphics[width=0.5\textwidth]{velocity_distributions_laminar_turbulent_2}
				\end{center}
				\supercaption{Velocity profiles for laminar (A), and turbulent (B and C) flows in a cylindrical pipe. B represents the time-averaged velocity distribution, while C shows several arbitrary instantaneous distributions (blurred) as well as their average in time. Turbulent flow in a pipe also features velocities in the radial and angular directions, which are not shown here.}{\wcfile{Pipe flow velocity distribution laminar turbulent.svg}{Figure} \cczero \oc}
				\label{fig_speed_cylinder_turb}
			\end{figure}
	
	
	\subsection{Pressure losses in turbulent pipe flow}
	
		Losses caused by turbulent flow depend on the wall roughness $\epsilon$ and on the diameter-based Reynolds number $\reD$.
		
		For lack of an analytical solution, we are not able to predict the value of the friction factor~$f$ anymore. Several empirical models can be built to obtain~$f$, the most important of which is known as the \vocab{Colebrook equation} expressed as:
			\begin{IEEEeqnarray}{rCl}
				\frac{1}{\sqrt{f}} &=& -\num{2} \log\left(\frac{1}{\num{3,7}}\frac{\epsilon}{D} + \frac{\num{2,51}}{\reD \sqrt{f}}\right)\label{eq_colebrook}
			\end{IEEEeqnarray}
		
		The structure of this equation makes it inconvenient to solve for $f$. To circumvent this difficulty, equation~\ref{eq_colebrook} can be solved graphically on the \vocab{Moody diagram}, \cref{fig_moody_diagram}. This classic document allows us to obtain numerical values for~$f$ (and thus predict the pressure losses~$\Delta p_\text{loss}$) if we know the diameter-based Reynolds number~$\reD$ and the relative roughness~$\epsilon/D$.
		\begin{figure}
			\begin{center}
				\includegraphics[width=\textwidth]{moody}
			\end{center}
			\supercaption{A Moody diagram, which presents values for $f$ measured experimentally, as a function of the diameter-based Reynolds number $\reD$, for different values of the relative roughness~$\epsilon/D$. This figure is reproduced with a larger scale as figure~\ref{fig_moody} p.~\pageref{fig_moody}.}{\wcfile{Moody diagram.jpg}{Diagram} \ccbysa S Beck and R Collins, University of Sheffield}
			\label{fig_moody_diagram}
		\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Engineer’s guide to pipe flows}

	\subsection{Summary so far}

		The world of pipe flows is a good playground for us to experiment with practical fluid mechanics. The main lessons learned in this chapter are as follows:
		\begin{itemize}
			\item From an engineering point of view, pipe flows are one-dimensional. We push a volume flow of fluid in on one side and expect to receive it on the other, perhaps with different properties. The integral “cost” of doing so is quantified with a single value, $\Delta p_\text{loss}$.
			\item When the flow is laminar, we can solve for the flow in the pipe. For this, we write the Navier-Stokes equations and apply known boundary conditions. We obtain the velocity \emph{everywhere} in the pipe, which gives us a wealth of information about the flow, including the $\Delta p_\text{loss}$.
			\item When the flow is turbulent, this analysis method does not work anymore. Even though the conditions are in principle simple, the flow is mesmerizingly complex. We resort to building models based on \emph{time-averaged measurement data}. Those models work for a wide number of pipes and conditions because they relate parameters which are \emph{non-dimensionalized}. We obtain the desired $\Delta p_\text{loss}$ with a single diagram reading and a simple algebraic manipulation.
		\end{itemize}

		These broad trends apply to many other sub-areas of fluid mechanics.


	\subsection{Choosing laminar or turbulent flow}
	
		When designing a pipe system, an engineer may have the opportunity to create laminar or turbulent flow. Inserting an expression for mass flow $\dot m = \rho V_\av (\pi/4) D^2$ into the definition of the Reynolds number, we obtain, for pipe flow:
		\eq{
			\re &=& \frac{\dot m}{D} \frac{4}{\pi \mu}
		}

		This equation is telling: for a given fluid ($\mu$) and a given mass flow ($\dot m$), the only way to ensure that the flow is laminar (with low Reynolds number) is to increase the diameter $D$. Doing so also increases installation costs; therefore, there is a balance to strike between initial installation costs (increasing with diameter) and operating costs due to pressure losses (which decrease as the diameter increases). In practice, except for cases where very small mass flows and velocities are involved (\eg medical fluid flows), most piping installations feature turbulent flow.

	\subsection{Pressure losses in laminar flow}
	
		The pressure losses in laminar flow are summed up with equation~\ref{eq_dotvol_lam} p.~\pageref{eq_dotvol_lam}, which we repeat here with the most important terms positioned first:
		\eq{
			\Delta p_\text{loss} &=& - L \frac{\dot m}{D^4} \frac{\num{128} \mu}{\pi \rho}
		}
		\begin{equationterms}
			\item for laminar pipe flow.
		\end{equationterms}
		
		Again, it is visible here that in laminar pipe flow, losses per unit pipe length increase linearly with mass flow $\dot m$, and with the power \num{-4} (!) of the diameter~$D$.

	\subsection{Pressure losses in turbulent flow}
	
		The picture for losses in turbulent flow is harder to draw. Re-arranging the definition ~\ref{eq_def_darcy} p.~\pageref{eq_def_darcy} to include the mass flow, we obtain:
		\eq{
			\Delta p_\text{loss} &=& - L \frac{\dot m^2}{D^5} \ f \ \frac{8}{\pi^2 \rho}
		}
		\begin{equationterms}
			\item for turbulent pipe flow.
		\end{equationterms}
		The factor $f$ in this equation generally varies according to the Reynolds number and to the roughness of the pipe, as described in the Moody diagram. At very turbulent regimes (in the upper right area of the diagram), $f$ becomes independent of $\reD$ and proportional to the relative roughness, so that we can write:
		\eq{
			\Delta p_\text{loss} &=& - L \frac{\dot m^2}{D^5} \ \left( c_1 + c_2 \frac{\epsilon}{D}\right) \ \frac{8}{\pi^2 \rho}
		}
		\begin{equationterms}
			\item for turbulent pipe flow in very turbulent regimes,
			\item where $c_1$ and $c_2$ are approximately constant.
		\end{equationterms}
		
		Based on this equation, we can see that pressure losses in highly-turbulent pipe flow increase approximately with the square of mass flow $\dot m$ and the power \num{-5} of the diameter $D$. In between this regime and the laminar regime, a variety of intermediary states are quantified using the Moody diagram.
	
	\subsection{Calculating pumping and turbining power}
	
		The tools above are all we need to calculate, given the geometry of an installation, how much pumping or turbining power is involved in moving a given mass flow of liquid though a pipe system. The steps are as follows:
		\begin{enumerate}
			\item Calculate the hydrostatic pressure drop across the device.\\
				This is done by imagining that there is no flow, and calculating the static pressure which would then exert on each side of the device, using equation~\ref{eq_gradp} p.~\pageref{eq_gradp}.
			\item Calculate the pressure losses due to friction.\\
				This is done by calculating the Reynolds number, using the Moody diagram to read the corresponding friction factor $f$, and calculating the corresponding $\Delta p_\text{loss}$ using the definition~\ref{eq_def_darcy}. Care must be taken with the sign of $\Delta p_\text{loss}$, which is \emph{always negative} by definition, but very often expressed as a positive number in the literature.\\
				Pressure losses induced by bends, junctions and obstacles are likewise calculated using their $K_L$ values and the definition~\ref{eq_def_loss_coeff}.
			\item Summing up the pressure differences.\\
				The complete pressure difference across the device is $\Delta p_\text{device} = -\Delta p_\text{losses} + \Delta p_\text{hydrostatic}$. Pressure losses due to friction are always negative, and hydrostatic pressure differences may have either sign.\\
				The power is recovered using equation~\ref{eq_power_pressure_net} p.~\pageref{eq_power_pressure_net}, $\dot P_\text{device} = \Delta p_\text{device} \ \dot m / \rho$. If this power is negative, the liquid is losing energy, and the device is acting as a turbine. If the power is positive, the liquid is gaining energy, and the device is acting as a pump.
		\end{enumerate}


\section{Solved problems}

\begin{youtubesolution}

	\begin{center}
		\textbf{Hydrostatic pressure on a turbine}
	\end{center}

	\begin{center}
	\includegraphics[width=\textwidth]{water_pipe_layout_201907}
	\end{center}

	A turbine is installed as shown above. What is the hydrostatic pressure difference available to the turbine?
	
	~
	
	\seesolution{3lyKby0fS-8}

\end{youtubesolution}

\begin{youtubesolution}

	\begin{center}
		\textbf{Pressure loss in a pipe}
	\end{center}

	In the piping installation from the previous example, water at \SI{20}{\degreeCelsius} is flowing with a volume flow of \SI{800}{\liter\per\second}.
	
	The pipe has roughness $\epsilon = \SI{0.25}{\milli\metre}$ and a diameter $D= \SI{1,1}{\metre}$. The bends each induce a loss coefficient $K_L = \num{0,75}$.

	What is the pressure drop due to friction losses in the pipe?

	\seesolution{Tp6a_50uqUc}

\end{youtubesolution}

\begin{youtubesolution}

	\begin{center}
		\textbf{Turbining power}
	\end{center}

	In the piping installation from the previous examples, what is the power made available to the turbine?
	\seesolution{BOew8INdQ54}

\end{youtubesolution}

\begin{youtubesolution}

	\begin{center}
		\textbf{Pressure distribution in a pipe}
	\end{center}

	In the piping installation from the previous examples, what is the pressure distribution along the pipe?

	\seesolution{q1xOmWaYZLg}

\end{youtubesolution}


\atendofchapternotes

\renewcommand{\lastedityear}{2020}
 \renewcommand{\lasteditmonth}{09}
   \renewcommand{\lasteditday}{14}
\renewcommand{\numberofthischapter}{7}
\renewcommand{\titleofthischapter}{\namechapterseven}
\atstartofexercises

\fluidmechexercisestitle

\mecafluexboxen


\begin{boiboiboite}
In cylindrical pipe flow, we assume the flow is always laminar for $\reD \lesssim \num{2300}$, and always turbulent for $\reD \gtrsim \num{4000}$. The Darcy friction factor $f$ is defined as:
					\begin{IEEEeqnarray}{rCl}
						f &\equiv& \frac{|\Delta p_\text{loss}|}{\frac{L}{D} \frac{1}{2} \rho V_\av^2} \ztag{\ref{eq_def_darcy}}
					\end{IEEEeqnarray}
			The loss coefficient $K_L$ is defined as:
				\begin{IEEEeqnarray}{rCl}
						K_L &\equiv& \frac{|\Delta p_\text{loss}|}{\frac{1}{2} \rho V_\av^2}	\ztag{\ref{eq_def_loss_coeff}}
				\end{IEEEeqnarray}
	Viscosities of various fluids are given in \cref{fig_viscosity_values_ex2}. Pressure losses in cylindrical pipes can be calculated with the help of the Moody diagram presented in \cref{fig_moody} p.~\pageref{fig_moody}.
\end{boiboiboite}\vspace{-1cm}
\begin{figure}
	\begin{center}
		\includegraphics[width=0.9\textwidth]{viscosity_small.pdf}
	\end{center}\vspace{-0.8cm}
	\supercaption{The viscosity of four fluids (crude oil, water, air, and C02) as a function of temperature. The scale for liquids is logarithmic and displayed on the left; the scale for gases is linear and displayed on the right.\vspace{-0.2cm}}{Figure reproduced from figure~\ref{fig_viscosity_values} p.~\pageref{fig_viscosity_values}; \ccby by Arjun Neyyathala \& \olivier\vspace{-1cm}}
	\label{fig_viscosity_values_ex2}
\end{figure}
\begin{figure}
	\begin{center}
		\includegraphics[width=\textwidth]{moody_rotated}
	\end{center}
	\supercaption{A Moody diagram, which presents values for $f$ measured experimentally, as a function of the diameter-based Reynolds number $\reD$, for different relative roughness values.}{\wcfile{Moody diagram.jpg}{Diagram} \ccbysa S Beck and R Collins, University of Sheffield}
	\label{fig_moody}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Reading quiz}

	\quiz


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Revision questions}
\wherefrom{non-examinable}

	The Moody diagram (\cref{fig_moody} p.~\pageref{fig_moody}) is simple to use, yet it takes practice to understand it fully… here are three questions to guide your exploration. They can perhaps be answered as you work through the other examples.
	\begin{enumerate}
		\item Why is there no zero on the diagram?
		\item Why are the curves sloped downwards — should friction losses not instead \emph{increase} with increasing Reynolds number?
		\item Why can the pressure losses $\Delta p_\text{losses}$ be calculated given the volume flow $\dot \vol$, but not the other way around?
	\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Air flow in a small pipe}
\wherefrom{Munson \& al. \smallcite{munsonetal2013} E8.5}
\label{exo_air_flow_pipe}

	A machine designed to assemble micro-components uses an air jet. This air is driven through a \SI{10}{\cm}-long cylindrical pipe with a~\SI{4}{\milli\metre} diameter, roughness~\SI{0,0025}{\milli\meter}, at an average speed of~\SI{50}{\metre\per\second}. 
	
	The inlet air pressure and temperature are~\SI{1,2}{\bar} and~\SI{60}{\degreeCelsius}; the viscosity of air is quantified in \cref{fig_viscosity_values_ex2} p.~\pageref{fig_viscosity_values_ex2}.
	\begin{enumerate}
		\item What is the pressure loss caused by the flow through the pipe?
		\item What is the maximum average flow speed for which the flow would remain laminar?
		\item What would this speed be if the pipe diameter was~\SI{4}{\centi\metre} instead of~\SI{4}{\milli\metre}?
	\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Water piping}
%\wherefrom{\cczero \oc}
\label{exo_water_piping}

	A long pipe is installed to carry water from one large reservoir to another (\cref{fig_water_piping}). The total length of the pipe is \SI{10}{\kilo\metre}, its diameter is \SI{0,5}{\metre}, and its roughness is $\epsilon = \SI{0,5}{\milli\metre}$. It must climb over a hill, so that the altitude changes along with distance.
	
		\begin{figure}[ht!]
			\begin{center}
				\includegraphics[width=\textwidth]{water_pipe_layout.png}
			\end{center}
			\supercaption{Layout of the water pipe. For clarity, the vertical scale is greatly exaggerated. The diameter of the pipe is also exaggerated.}{\wcfile{Pipe flow between reservoirs, a pump or turbine.svg}{Figure} \cczero \oc}
			\label{fig_water_piping}
		\end{figure}
	
	The pump must be powerful enough to push \SI{1}{\metre\cubed\per\second} of water at \SI{20}{\degreeCelsius}. 
	
	\Cref{fig_viscosity_values_ex2} p.~\pageref{fig_viscosity_values_ex2} quantifies the viscosity of various fluids, and \cref{fig_moody} p.~\pageref{fig_moody} quantifies losses in cylindrical pipes.
			
	\begin{enumerate}
		\item Will the flow in the water pipe be turbulent?
		\item What is the pressure loss caused by the flow through the pipe?
		\item What is the pumping power required to meet the design requirements?
		\item What would be the power required for the same volume flow if the pipe diameter was doubled?
	\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Design of a water piping system}
%\wherefrom{\ccbysa \oc}
\label{exo_design_water_piping}

	You start your career as a junior engineer in a company that designs piping and pumping systems.
	
	Your first assignment is to choose the dimensions of a system which should carry \SI{3}{\metre\cubed\per\hour} of water (\SI{e-3}{\pascal\second}) across a horizontal distance of \SI{1}{\kilo\metre}. Fresh from reading through \chaptersevenshort, you design the system to feature laminar flow only.
	
	You begin with a revision of the relevant theory, starting, of course, from the Navier-Stokes equations and a simple diagram (\cref{fig_cylinder_lam_exercise}), obtaining the velocity distribution for laminar flow in a circular pipe:
		\begin{IEEEeqnarray}{rCl}
			v_z = u_{(r)}	&=& - \frac{1}{4 \mu} \left(\partialderivative{p}{z} \right) (R^2 - r^2) \ztag{\ref{eq_u_lam}}
		\end{IEEEeqnarray}
	\begin{figure}[h]
		\begin{center}\vspace{-0.5cm}%handmade
			\includegraphics[width=6cm]{pipe_flow_coordinates}
		\end{center}\vspace{-0.5cm}%handmade
		\supercaption{A cylindrical coordinate system to study laminar flow in a horizontal cylindrical duct.}{\wcfile{Pipe flow coordinate system.svg}{Figure} \cczero \oc}
		\label{fig_cylinder_lam_exercise}\vspace{-0.5cm}%handmade
	\end{figure}
	\begin{enumerate}\vspace{-0.5cm}%handmade
		\item What is the minimum pipe diameter for which the flow will remain laminar?
		\item Starting from equation~\ref{eq_u_lam}, show that the pressure loss per unit length in the pipe with laminar flow is expressed as a function of the volume flow $\dot \vol$ and the diameter $D$ as:
		\begin{IEEEeqnarray}{rCl}
			\Delta p_\text{loss} &=& -\frac{\num{128}}{\pi} \frac{\mu L \dot m}{\rho D^4} \ztag{\ref{eq_dotvol_lam}}
		\end{IEEEeqnarray}
		\item With the diameter chosen above, what is the pressure loss in the pipe?
		\item What is the pumping power required?
	\end{enumerate}
	
	With those results in hand, you turn to your colleagues — but with a smile, they suggest you try a design with turbulent flow instead.
	\begin{enumerate}
		\shift{4}
		\item What would be the pressure loss if an \SI{8}{\centi\metre}-diameter plastic pipe was used?
		\item What would then be the pumping power required?
		\item What is one advantage of using a pipe with smaller diameter? (briefly justify your answer, \eg in 30 words or less)
	\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
\subsubsection{Major oil pipeline}
%\wherefrom{\cczero \oc}
\label{exo_pipeline}

	Strong from your experience working through problem~\ref{exo_design_water_piping}, you join the team in charge of designing one very large oil pipeline system (\cref{fig_trans_alaska}).
	\begin{figure}
		\begin{center}
			\includegraphics[width=8cm]{pipeline0}
		\end{center}
		\supercaption{The \we{Trans-Alaska Pipeline System}, which inspired this problem. It was built in the 1970s, at tremendous financial, political and social cost.}{\wcfile{Trans-Alaska Pipeline System Luca Galuzzi 2005.jpg}{Photo} \ccbysa by Luca Galuzzi -- www.galuzzi.it}
		\label{fig_trans_alaska}
	\end{figure}

	Your design must safely carry 700 thousand barrels of oil (\SI{110 000}{\metre\cubed}) per day along a length of \SI{1200}{\kilo\metre}.  The crude oil has density \SI{900}{\kilogram\per\metre\cubed} and its viscosity is quantified in \cref{fig_viscosity_values_ex2} p.~\pageref{fig_viscosity_values_ex2}. The average temperature of the oil during the transit is \SI{60}{\degreeCelsius}.
	
	The landscape is flat for most of the journey, with a \SI{200}{\kilo\metre}-wide mountain range in the middle that reaches \SI{1400}{\metre} altitude.
	
	 Your team selects a cylindrical, smooth steel duct with \SI{1,22}{\metre} diameter, average roughness $\epsilon = \SI{0,15}{\milli\metre}$. Because the pipeline passes through ecologically fragile areas, as well as a seismically-active region, you decide to never exceed \SI{200}{psi} (\SI{13,8}{\bar}) of gauge pressure in the pipeline. To prevent oil cavitation (a change of state with destructive consequences), you decide to never reach below \SI{0,8}{\bar} of absolute pressure in the pipeline.
	
	\begin{enumerate}
		\item How much time does the average oil particle need to travel across the line?
		\item How much pumping power is required in total?
		\item How far apart should the pumping stations be laid out in the flat sections of the pipeline?
		\item How far apart should the pumping stations be laid out in the ascending section of the pipeline?
		\item Propose a pumping station arrangement, and calculate the power required for each pump.
	\end{enumerate}
	
	Before you start building the pipeline, the operator would like to know how the system would perform at half-capacity (\ie with half the volume flow).
	\begin{enumerate}
		\shift{5}
		\item If none of the other input data changes, what is the new pumping power?
		\item Propose one reason why in practice, the pumping power may be higher than you just calculated (briefly justify your answer, \eg in 30 words or less).
	\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearfloats
\subsubsection{Pump with pipe expansion}
\label{exo_pump_pipe_expansion}

	A pump is used to carry a volume flow of \SI{200}{\liter\per\second} from one large water reservoir to another (\cref{fig_water_piping_2}). The altitude of the water surface in both reservoirs is the same.
		\begin{figure}[h]
			\begin{center}
				\includegraphics[width=\textwidth]{water_pipe_layout2.png}
			\end{center}
			\supercaption{Layout of the water pipe. For clarity, the diameter of the pipe and the vertical scale are exaggerated.}{\wcfile{Pipe flow between reservoirs, a pump or turbine.svg}{Figure} \cczero \oc}
			\label{fig_water_piping_2}
		\end{figure}
	
	The pipe connecting the reservoirs is made of concrete ($\epsilon = \SI{0,25}{\milli\metre}$); it has a diameter of~\SI{50}{\centi\metre} on the first half, and~\SI{100}{\centi\metre} on the second half. In the middle, the conical expansion element induces a loss coefficient of~\num{0,8}. At the outlet (at point D), the pressure is approximately equal to the corresponding hydrostatic pressure in the outlet tank.
	
	The inlet is \SI{14}{\metre} below the surface. The total pipe length is~\SI{400}{\metre}; the altitude change between inlet and outlet is~\SI{12}{\metre}.
	
	\begin{enumerate}
		\item Represent qualitatively (that is to say, showing the main trends, but without displaying accurate values) the water pressure as a function of pipe distance, when the pump is turned~off.
		\item On the same graph, represent qualitatively the water pressure when the pump is switched on.
		\item What is the water pressure at points A, B, C and D?
	\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Piping and power of a water turbine}
\wherefrom{from 2018-07 final examination}
\label{exo_2018_exam}

	A water turbine is installed between two reservoirs in order to extract power from the flow of water. The water is guided to the turbine through a pipe, as shown in figure~\ref{fig_water_piping}.
	
	The pipe is made of reinforced concrete, with a total length $L = \SI{0,8}{\kilo\metre}$, a diameter $D = d = \SI{1,2}{\metre}$, and an interior surface roughness $\epsilon = \SI{6}{\milli\metre}$. The pipe has altitude variations along its length, as indicated in figure~\ref{fig_water_piping}. 	The turbine is designed to handle \SI{5000}{\liter\per\second} of water at \SI{20}{\degreeCelsius}.
	
		\begin{figure}
			\begin{center}
				\includegraphics[width=\textwidth]{water_pipe_layout_201807}
			\end{center}
			\supercaption{Layout of the water pipe. For clarity, the vertical scale and the diameter of the pipe are greatly exaggerated.}{}
			\label{fig_water_piping}\vspace{-0.5cm}
		\end{figure}
			
	\begin{enumerate}
		\item On a diagram, represent qualitatively  (i.e.\ without numerical data) the pressure distribution along the length of the pipe.
		\item What is the pressure drop due to friction losses generated by the water flow in the pipe?
		\item What is the power developed by the turbine?
		\item What would be the new power developed by the turbine if the volume flow was divided by~two?
	\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Politically incorrect fluid mechanics}
\wherefrom{non-examinable}
\label{exo_dontexist}

	In spite of the advice of their instructor, a group of students attempts to apply fluid mechanics to incommendable activities. Their objective is to construct a drinking straw piping system that can mix a drink of vodka and tonic water in the correct proportions (\cref{fig_politically_incorrect_fluidmech}). They use a “\textit{Strawz}” kit of connected drinking straws, two bottles, and a glass full of ice and liquid water to cool the mix.
	
	\begin{figure}
			\begin{center}
				\includegraphics[width=0.5\textwidth]{vodkatonic.png}
			\end{center}
			\supercaption{Conceptual sketch of a student experiment.}{Figure \cczero \oc}
			\label{fig_politically_incorrect_fluidmech}
	\end{figure}
	
	\begin{table}[ht!]
		\begin{center}
			\begin{tabularx}{10cm}{S|S} % the @{something} kills the inter-column space and replaces it with "something"
			{Percentage of alcohol by weight} & {Viscosity in \si{centipoise}}\\
			\hline
			0	& 1.005\\
			10 & 1.538\\
			20	& 2.183\\
			30	& 2.71\\
			40 & 2.91\\
			50 & 2.87\\
			60 & 2.67\\
			70 & 2.37\\
			80 & 2.008\\
			90 & 1.61\\
			100 & 1.2\\
			\hline
			\end{tabularx}
		\end{center}
		\supercaption{Viscosity of a mix of ethanol and water at \SI{20}{\degreeCelsius}.}{data from Bingham, Trans. Chem. Soc, 1902, 81, 179.}
			\label{table_water_alcohol}
	\end{table}
	
	For simplicity, the following information is assumed about the setup:
	\begin{itemize}
		\item Vodka is modeled as \SI{40}{\percent} pure alcohol (ethanol) with \SI{60}{\percent} water by volume;
		\item Ethanol density is \SI{0,8}{\kilogram\per\metre\cubed};
		\item Tonic water is modeled as pure water;
		\item The viscosity of water and alcohol mixes is described in \cref{table_water_alcohol} (use the nearest relevant value);
		\item The pipe bends induce a loss coefficient factor $K_{L \text{bend}} = \num{0,5}$ each;
		\item The pipe T-junction induces a loss coefficient factor $K_L = \num{0,3}$ in the line direction and~\num{1} in the branching flow;
		\item The pipe has inner diameter $D = \SI{3}{\milli\metre}$ and roughness $\eta = \SI{0,0025}{\milli\metre}$.
	\end{itemize}
	
	The students wish to obtain the correct mix: one quarter vodka, three quarters tonic water. For given levels of liquid in the bottles, is there a straw pipe network configuration that will yield the correct mix, and if so, what is it?

\clearpage
\subsubsection*{Answers}
\NumTabs{2}
\begin{description}
	\item [\ref{exo_air_flow_pipe}]%
				\tab 1) Calculating inlet density with the perfect gas model, $\reD = \num{14263}$ (turbulent), a Moody diagram read gives $f \approx \num{0,029}$, so $\Delta p_\text{friction} = -\SI{1292,6}{\pascal} = \SI{-0,0129}{\bar}$.
	\item [\ref{exo_water_piping}]%
				\tab $|\Delta p_\text{alt.}| = \rho g (26 - 8 + 5 - 7) = \SI{1,57}{\bar}$ and $|\Delta p_\text{friction}| = \SI{51,87}{\bar}$ : $\dot W_\text{pump} = \SI{5,345}{\mega\watt}$.
	\item [\ref{exo_design_water_piping}]%
				\tab 1) $D_\min = \SI{0,46}{\metre}$\\
				\tab 2) Follow the process used p.~\pageref{eq_tmp4} to go from eq.~\ref{eq_tmp4} to eq.~\ref{eq_pressure_gradient_couette}: the only difference is the use of cylindrical (instead of rectangular) coordinates.\\
				\tab 3) $|\Delta p|\text{loss} = \SI{0,75}{\pascal}$\\
				\tab 4) $\dot W = \SI{0,63}{\milli\watt}$\\
				\tab 5) $|\Delta p|\text{loss 2} = \SI{4,82}{\kilo\pascal}$ (\num{8000} times more)\\
				\tab 6) $\dot W_2 = \SI{4,2}{\watt}$
	\item [\ref{exo_pipeline}]%
				\tab 1) Approximately 12 days 18 hours\\
				\tab 2) $\dot W_\text{total} = \SI{10,02}{\mega\watt}$\\
				\tab 3) $\Delta L_\text{flat terrain} = \SI{213}{\kilo\metre}$\\
				\tab 4) $\Delta L_\text{ascending terrain} = \SI{10,8}{\kilo\metre}$\\
				\tab 7) Hint: in normal operation, what happens with the \SI{10}{\mega\watt} of power — in which form is this energy converted?
	\item [\ref{exo_pump_pipe_expansion}]
				\tab 3) $\Delta p_{f \fromatob} = \SI{-3735}{\pascal}$, $\Delta p_\frombtoc = \SI{+71}{\pascal}$ (the sum of losses due to friction and gains due to decrease in kinetic energy), $\Delta p_{f \fromctod} = \SI{-117}{\pascal}$. Add hydrostatic pressure changes, working backwards from D to A, to obtain pressure in all four points.
	\item [\ref{exo_2018_exam}]
				\tab 2) $\Delta p_\text{friction losses} = \SI{-2,0196}{\bar}$\\
				\tab 3) $\dot W_\text{turbine} = \SI{-1,0503}{\mega\watt}$\\
				\tab 4) The power will decrease by \SI{14}{\percent}
	\item [\ref{exo_dontexist}] \tab The author cannot remember which exercise you are referring to.
\end{description}
\atendofexercises

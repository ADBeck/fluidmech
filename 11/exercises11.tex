\renewcommand{\lastedityear}{2019}
 \renewcommand{\lasteditmonth}{06}
   \renewcommand{\lasteditday}{26}
\renewcommand{\numberofthischapter}{11}
\renewcommand{\titleofthischapter}{\namechaptereleven}
\atstartofexercises

\fluidmechexercisestitle

\mecafluboxen

\mecafluexboxen

\begin{boiboiboite}
	In a highly-viscous (creeping) steady flow, the drag~$F_\D$ exerted on a spherical body of diameter~$D$ at by flow at velocity~$U_\infty$ is quantified as:
		\begin{IEEEeqnarray}{rCl}
				F_\text{D sphere} &=& 3 \pi \mu U_\infty D	\ztag{\ref{eq_drag_creeping_sphere}}
		\end{IEEEeqnarray}
\end{boiboiboite}

%%%%%%
\subsubsection{Volcanic ash from the Eyjafjallajökull}
\wherefrom{Çengel \& al. \smallcite{cengelcimbala2010} E10.2}
\label{exo_eyjafthing}

	In 2010, a volcano with a complicated name and unpredictable mood decided to ground the entire European airline industry for five days.
	
	We consider a microscopic ash particle released at very high altitude (\SI{-50}{\degreeCelsius}, \SI{0,55}{\bar},\\ \SI{1,474e-5}{\newton\second\per\metre\squared}). We model it as a sphere with \SI{50}{\micro\metre} diameter. The density of volcanic ash is \SI{1240}{\kilogram\per\metre\cubed}.
	
	\begin{enumerate}
		\item What is the terminal velocity of the particle?
		\item Will this terminal velocity increase or decrease as the particle progresses towards the ground? (briefly justify your answer, \eg in 30 words or less)
	\end{enumerate}


%%%%%%
\subsubsection{Water drop}
\wherefrom{Çengel \& al. \smallcite{cengelcimbala2010} 10-21}
\label{exo_water_drop}
	
	A rainy day provides yet another opportunity for exploring fluid dynamics (\cref{fig_water_drops}). A water drop with diameter \SI{42,4}{\micro\metre} is falling through air at \SI{25}{\degreeCelsius} and \SI{1}{\bar}.
	\begin{enumerate}
		\item Which terminal velocity will it reach?
		\item Which velocity will it reach once its diameter will have doubled?
	\end{enumerate}

	\begin{figure}[h]
		\begin{center}
			\includegraphics[height=8cm]{images/water_drops.png}
		\end{center}
		\supercaption{A sketched diagram showing the geometry of water drops of various sizes in free fall. When their diameter is lower than \SI{2}{\milli\metre}, water drops are approximately spherical (B). As they grow beyond this size, their shape changes and they eventually break-up (C-E). They never display the “classical” shape displayed in A, which is caused only by surface tension effects when they drip from solid surfaces.}{\wcfile{Raindrops sizes.svg}{Figure} \ccbysa by \wcun{broks13}{Ryan Wilson}}
		\label{fig_water_drops}
	\end{figure}


%%%%%%
\subsubsection{Idealized flow over a hangar roof}
\wherefrom{based on White \smallcite{white2008} P8.54}
\label{exo_hangar_roof}

	Certain flows in which both compressibility and viscosity effects are negligible can be described using the potential flow assumption (the hypothesis that the flow is everywhere irrotational). If we compute the two-dimensional laminar steady fluid flow around a cylinder profile, we obtain the velocities in polar coordinates as:
	\begin{IEEEeqnarray}{rCl}
			v_r			&=& V_\infty \cos \theta \left(1 - \frac{R^2}{r^2}\right)\ztag{\ref{eq_ur_cylinder_nolift}}\\
			v_\theta	&=& - V_\infty \sin \theta \left(1 + \frac{R^2}{r^2}\right)\ztag{\ref{eq_utheta_cylinder_nolift}}
	\end{IEEEeqnarray}
	\begin{equationterms}
		\item where \tab the origin ($r = 0$) is at the center of the cylinder profile;
		\item		\tab $\theta$ \tab\tab\tab is measured relative to the free-stream velocity vector;
		\item		\tab $V_\infty$ \tab is the incoming free-stream velocity;
		\item and	\tab $R$ \tab\tab\tab is the (fixed) cylinder radius.
	\end{equationterms}
	
	In this exercise, we study the air flow over a hangar roof with this model. We use the equations above to describe the air velocity everywhere, pretending the as the wind blows about a large semi-cylindrical solid structure — an idealized description of an otherwise complex flow.

		\begin{figure}
			\begin{center}
				\includegraphics[width=\textwidth]{images/hangar.png}
			\end{center}
			\supercaption{A semi-cylindrical hangar roof. Wind with uniform velocity $U$ flows perpendicular to the cylinder axis.}{\wcfile{Semi-circular hangar potential flow.svg}{Figure} \cczero \oc}
			\label{fig_hangar}
		\end{figure}
	
	Wind with a nearly-uniform velocity $U_\infty = \SI{100}{\kilo\metre\per\hour}$ is blowing across a \SI{50}{\metre}-long hangar with a semi-cylindrical geometry, as shown in \cref{fig_hangar}. The radius of the hangar is~$R = \SI{20}{\metre}$.


	\begin{enumerate}
		\item Starting from eqs.~\ref{eq_ur_cylinder_nolift} and~\ref{eq_utheta_cylinder_nolift}, show that the pressure $p_s$ on the surface on the roof is distributed as:
			\begin{IEEEeqnarray}{rCl}
				p_s &=& p_\infty + \frac{1}{2} \rho \left(V_\infty^2 - 4 V_\infty^2 \sin^2 \theta\right)
			\end{IEEEeqnarray}
		\item The pressure inside the hangar is set to $p_\infty$. What is the total lift force on the hangar?\\
		(see also problem~\ref{exo_pressure_force_cylinder} p.~\pageref{exo_pressure_force_cylinder})\\
		(a couple of hints to help with the algebra: $\int \sin x \diff x = -\cos x + k$ and $\int \sin^3 x \diff x = \frac{1}{3} \cos^3 x - \cos x + k$).
		\item At which position on the roof is the $p_s = p_\infty$?
		\item Describe briefly (\eg in 30 words or less) two reasons why the results above would not correspond to reality.
	\end{enumerate}



%%%%%%
\subsubsection{Cabling of the Wright Flyer}
\wherefrom{derived from Munson \& al. \smallcite{munsonetal2013} 9.106}
\label{exo_cabling_wright_flyer}

	The \textit{\we{Wright Flyer I}}, the first powered and controlled aircraft in history, was subjected to multiple types of drag. We have already studied viscous friction on its thin wings in exercise~7.4. The data in figure~\ref{fig_sphere_cylinder_drag} provides the opportunity to quantify drag due to pressure.

	A network of metal cables with diameter \SI{1,27}{\milli\metre} criss-crossed the aircraft in order to provide structural rigidity. The cables were positioned perpendicularly to the air flow, which came at \SI{40}{\kilo\metre\per\hour}. The total cable length was approximately~\SI{60}{\metre}.

	What was the drag generated by the cables?

		\begin{figure}
			\begin{center}
				\includegraphics[width=\textwidth]{images/cylinder_sphere_drag_munson_1}
				\includegraphics[width=0.6\textwidth]{images/cylinder_sphere_drag_munson_2}
			\end{center}
			\supercaption{Experimental measurements of the drag coefficient applying to a cylinder and to a sphere as a function of the diameter-based Reynolds number $\reD$, shown together with schematic depictions of the flow around the cylinder. By convention, the \vocab{drag coefficient} $C_\D \equiv C_{F\ \D} \equiv \frac{F_\D}{\frac{1}{2} \rho S U_\infty^2}$ (eq.~\ref{eq_def_force_coefficient} p.~\pageref{eq_def_force_coefficient}) compares the drag force~$F_\D$ with the frontal area~$S$.}{Both figures \copyright~from Munson \& al.\cite{munsonetal2013}}
			\label{fig_sphere_cylinder_drag}
		\end{figure}

%%%%%%
\subsubsection{Ping pong ball}
\wherefrom{Munson \& al. \smallcite{munsonetal2013} E9.16}
\label{exo_ping_pong_ball}

	A series of experiments is conducted in a wind tunnel on a large cast iron ball with a smooth surface; the results are shown in \cref{fig_sphere_lift}. These measurement data are used to predict the behavior of a ping pong ball. Table tennis regulations constrain the mass of the ball to \SI{2,7}{\gram} and its diameter to \SI{40}{\milli\metre}.
	
	\begin{enumerate}
		\item Is it possible for a ball thrown at a speed of \SI{50}{\kilo\metre\per\hour} to have a perfectly horizontal trajectory?
		\item If so, what would be its deceleration?
		\item How would the drag and lift applying on the ball evolve if the air viscosity was progressively decreased to zero?
	\end{enumerate}

	\begin{figure}
		\begin{center}
			\includegraphics[width=0.7\textwidth]{images/rotating_sphere_munson}
		\end{center}
		\supercaption{Experimental measurements of the lift and drag coefficients applying on a rotating sphere in an steady uniform flow.}{Figure \copyright~from Munson \& al.\cite{munsonetal2013}}
		\label{fig_sphere_lift}
	\end{figure}


%%%%%%
\clearpage
\subsubsection{Flow field of a tornado}
\wherefrom{Çengel \& al. \smallcite{cengelcimbala2010} E9-5, E9-14 \& E10-3}
\label{exo_tornado}

	In this problem, we attempt to model a very large-scale flow: that of a tornado (\cref{fig_tornado}). We begin by pretending the tornado is one perfectly straight, stationary structure. We divide the flow into two regions: a core cylinder that rotates almost like a solid body, and an outer region where flow spins in an irrotational matter. This model is called the \we{Rankine vortex} (displayed in \cref{fig_rankine_vortex}) and is used widely as a simple, first approximation to model flows as large as a hurricane and as small as turbulence-induced vortices.
	\begin{figure}[h]
		\begin{center}
			\includegraphics[width=0.5\textwidth]{F5_tornado_Elie_Manitoba_2007}
		\end{center}\vspace{-0.5cm}%handmade
		\supercaption{Photo of an approaching tornado in Manitoba, Canada}{\wcfile{F5 tornado Elie Manitoba 2007.jpg}{Photo} \ccbysa by \wcu{Grhu}}
		\label{fig_tornado}
	\end{figure}
	\begin{figure}[h]
		\begin{center}\vspace{-1cm}%handmade
			\includegraphics[width=0.5\textwidth]{RankineVortex}
		\end{center}\vspace{-0.5cm}%handmade
		\supercaption{Modeled angular velocity in a vortex, according to the \we{Rankine vortex} model}{\wcfile{RankineVortex.svg}{Figure} \ccbysa by \weu{Justin1569}}
		\label{fig_rankine_vortex}
	\end{figure}
		
	We are first interested in the outer region of the tornado flow field. We model the flow as being steady, two-dimensional (neglecting any movement in the vertical, $z$-direction), and having a rotational velocity $v_\theta$ such that:
	\begin{equation}
		v_\theta = \frac{\Gamma}{2 \pi r}
	\end{equation}
	\begin{equationterms}
		\item in which $\Gamma$ is the \vocab{circulation} (measured in \si{\per\second}) and remains constant and uniform.
	\end{equationterms}
	
	
	\begin{enumerate}
		\item The mass balance equation for incompressible flow (eq.~\ref{eq_continuity_der_inc} p.~\pageref{eq_continuity_der_inc}) is developed in cylindrical coordinates as follows:
		\begin{IEEEeqnarray}{rCl}
			 \frac{1}{r} \partialderivative{r v_r}{r} + \frac{1}{r} \partialderivative{v_\theta}{\theta} + \partialderivative{v_z}{z} = 0
		\end{IEEEeqnarray}
		According to this mass balance equation, what form must the radial velocity $v_r$ take?
	\end{enumerate}
	
	Among all the possibilities for $v_r$, we choose the simplest form, so that from now on, we model radial velocity as:
	\begin{IEEEeqnarray}{rCl}
		v_r &=& 0
	\end{IEEEeqnarray}
	
	\begin{enumerate}
	\shift{1}
	\item The momentum balance equation for incompressible flow (eq.~\ref{eq_navierstokes} p.~\pageref{eq_navierstokes}) is developed in cylindrical coordinates are as follows:
		\begin{IEEEeqnarray}{lr}
			\rho \left[ \partialtimederivative{v_r} + v_r \partialderivative{v_r}{r} + \frac{v_\theta}{r} \partialderivative{v_r}{\theta} - \frac{v_\theta^2}{r} + v_z \partialderivative{v_r}{z} \right]  &\nonumber\\
			 \ \ \ \ \ = \	\rho g_r - \partialderivative{p}{r} + \mu \left[ \frac{1}{r} \partialderivative{}{r} \left(r \partialderivative{v_r}{r} \right) - \frac{v_r}{r^2} + \frac{1}{r^2} \secondpartialderivative{v_r}{\theta} -\frac{2}{r^2} \partialderivative{v_\theta}{\theta} + \secondpartialderivative{v_r}{z} \right] \nonumber \\
			\\
			\rho \left[ \partialtimederivative{v_\theta} + v_r \partialderivative{v_\theta}{r} + \frac{v_\theta}{r} \partialderivative{v_\theta}{\theta} + \frac{v_r v_\theta}{r} + v_z \partialderivative{v_\theta}{z} \right]  &\nonumber\\
			 \ \ \ \ \ = \	\rho g_\theta - \frac{1}{r} \partialderivative{p}{\theta} + \mu \left[ \frac{1}{r} \partialderivative{}{r} \left(r \partialderivative{v_\theta}{r} \right) - \frac{v_\theta}{r^2} + \frac{1}{r^2} \secondpartialderivative{v_\theta}{\theta} + \frac{2}{r^2} \partialderivative{v_r}{\theta} + \secondpartialderivative{v_\theta}{z} \right] \nonumber \\
			\\
			\rho \left[ \partialtimederivative{v_z} + v_r \partialderivative{v_z}{r} + \frac{v_\theta}{r} \partialderivative{v_r}{\theta} + v_z \partialderivative{v_z}{z} \right]  &\nonumber\\
			 \ \ \ \ \ = \	\rho g_z - \partialderivative{p}{z} + \mu \left[ \frac{1}{r} \partialderivative{}{r} \left(r \partialderivative{v_z}{r} \right) + \frac{1}{r^2} \secondpartialderivative{v_z}{\theta} + \secondpartialderivative{v_z}{z} \right] \nonumber \\
		\end{IEEEeqnarray}
		Starting from those equations, show that the pressure distribution in the outer region of the tornado can be expressed as:
		\begin{IEEEeqnarray}{rCl}
			p &=& p_\infty -\frac{1}{2} \rho \ \Gamma^2 \frac{1}{r^2}
		\end{IEEEeqnarray}
			\begin{equationterms}
				\item where $p_\infty$ is the atmospheric pressure far away from the tornado.
			\end{equationterms}
	\end{enumerate}

	We now turn to the core of the tornado, which we model as if it were a rotating solid (a \vocab{vortex core}).

	\begin{enumerate}
		\shift{2}
		\item What is the radial velocity $v_\theta$ distribution?
		\item What is the pressure field within the rotational core of the tornado?\\
			  (hint: you may start directly from an energy balance equation, eq.~\ref{eq_sfee} p.~\pageref{eq_sfee}, without having to use the Navier-Stokes equations above).
		\item Make a simple, qualitative sketch (\ie without numerical data) of the pressure as a function of radius throughout the entire tornado flow field.		
	\end{enumerate}

	It is finally time to calibrate and exploit our model. We estimate the tornado diameter to be \SI{50}{\metre} and the maximum wind velocity to be \SI{180}{\kilo\metre\per\hour}.
	
	\begin{enumerate}
		\shift{5}
		\item According to the model, what is the lowest pressure attained by the air?
		\item According to the model, at what distance from the core are winds lower than \SI{50}{\kilo\metre\per\hour}?
	\end{enumerate}
	
	(curious students may play with the above model by adding a non-zero radial velocity, and look up the phenomenon of \vocab{vortex stretching})
	

%%%%%%
\subsubsection{Lift on a symmetrical object}
\wherefrom{non-examinable}

	Briefly explain (\eg with answers 30 words or less) how lift can be generated on a sphere or a cylinder,
	\begin{itemize}
		\item with differential control boundary layer control;
		\item with the effect of rotation.
	\end{itemize}
	Draw a few streamlines in a two-dimensional sketch of the phenomenon.


%%%%%%
\subsubsection{Air flow over a wing profile}
\wherefrom{From Munson \& al. \smallcite{munsonetal2013} 9.109}

	The characteristics of a thin, flat-bottomed airfoil are examined by a group of students in a wind tunnel. The first investigations focus on the boundary layer, and the research group evaluate the boundary layer thickness and make sure that it is fully attached.
	
	Once this is done, the group proceeds with speed measurements all around the airfoil. Measurements of the longitudinal speed $u$ just above the boundary layer on the top surface are tabulated below:
	\begin{center}
		\begin{tabularx}{6cm}{S|S|S}
		{$x/c$ (\si{\percent})} & {$y/c$ (\si{\percent})} & {$u/U$}\\
		\hline
		0		& 0		& 0\\
		2.5		& 3.72	& 0.971\\
		5		& 5.3		& 1.232\\
		7.5		& 6.48	& 1.273\\
		10		& 7.43	& 1.271\\
		20		& 9.92	& 1.276\\
		30		& 11.14	& 1.295\\
		40		& 10.49	& 1.307\\
		50		& 10.45	& 1.308\\
		60		& 9.11	& 1.195\\
		70		& 6.46	& 1.065\\
		80		& 3,62	& 0.945\\
		90		& 1,26	& 0.856\\
		100		& 0		& 0.807\\
		\hline
		\end{tabularx}
	\end{center}
	
	On the bottom surface, the speed is measured as being constant ($u=U$) to within experimental error.
	
	What is the lift coefficient of the airfoil?





%%%%%%
\clearpage
\subsubsection*{Answers}
\NumTabs{2}
\begin{description}
	\item [\ref{exo_eyjafthing}]%
		\tab 1) At terminal velocity, the weight of the sphere equals the drag. This allows us to obtain $U = g \rho_\text{sphere} \frac{D^2}{18 \mu} = \SI{0,1146}{\metre\per\second}$: unbearably slow when you are stuck in an airport! With $U$, check that the Reynolds number indeed corresponds to creeping flow: $\reD = \num{0,334}$.
	\item [\ref{exo_water_drop}]%
		\tab Same as previous exercise: $U_1 = \SI{4,578e-2}{\metre\per\second}$ and $U_2 = \SI{0,183}{\metre\per\second}$, with Reynolds numbers of \num{0,113} and \num{0,906} respectively (thus creeping flow hypothesis valid).
	\item [\ref{exo_hangar_roof}]%
		\tab 1) Integrate the vertical component of force due to pressure: $F_\text{L roof} = \SI{1,575}{\mega\newton}$.
		%\tab 2) $\left.\theta\right|_{F=0} = \SI{54,7}{\degree}$
	\item [\ref{exo_cabling_wright_flyer}]%
		\tab A simple reading of \cref{fig_sphere_cylinder_drag} gives $F_\D = \SI{6,9}{\newton}$, $\dot W = \SI{76}{\watt}$.
	\item [\ref{exo_ping_pong_ball}]
		\tab Yes — a reading of \cref{fig_hangar} gives $\omega = \SI{83}{rev/s}$.
\end{description}
\atendofexercises

Fluid Dynamics
==============

The lecture notes and exercise sheets of a one-semester course in fluid mechanics, designed for students of the Chemical Energy and Engineering program at the Otto-von-Guericke-Universität Magdeburg.

These documents can be downloaded from the course homepage, at https://fluidmech.ninja/


## Author

Main author: <a href="https://ariadacapo.net/">Olivier Cleynen</a>, based on the works by White [1], Çengel & al. [2], Munson & al. [3], and de Nevers [4] cited below.


## License

The text of the lecture notes is licensed under a <a href="https://creativecommons.org/licenses/by-nc/4.0/deed.en">Creative Commons BY-NC</a> license.

Figures used in this document are sometimes quoted from fully-copyrighed works and may not be re-licensable. Other figures are published under free licenses, from authors not associated with this work. The copyright license of each figure is indicated next to its caption.


## References

[1] White, Frank M.: Fluid Mechanics, 7th ed. 2008 McGraw-Hill, ISBN 9780071311212
[2] Munson, Bruce R., Okiishi, Theodore H., Huebsch, Wade W. and Rothmayer, Alric P.: Fluid Mechanics, 7th ed. 2013 Wiley, ISBN 9781118318676
[3] Çengel, Yunus A. and Cimbala, John M.: Fluid Mechanics, 2nd ed. 2010 McGraw-Hill, ISBN 9780070700345
[4] de Nevers, Noel: Fluid Mechanics for Chemical Engineers, 3rd ed. 2004 McGraw-Hill, ISBN 9781259002380

\contentsline {chapter}{Contents}{2}{section*.1}%
\contentsline {chapter}{About this course (syllabus)}{6}{section*.2}%
\babel@toc {english}{}
\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Basic flow quantities}{11}{chapter.1}%
\contentsline {section}{\numberline {1.1}Concept of a fluid}{11}{section.1.1}%
\contentsline {section}{\numberline {1.2}Fluid dynamics}{11}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Solution of a flow}{11}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Modeling of fluids}{12}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Theory, numerics, and experiment}{13}{subsection.1.2.3}%
\contentsline {section}{\numberline {1.3}Important concepts in mechanics}{14}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Position, velocity, acceleration}{14}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Forces and moments}{14}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}Energy}{15}{subsection.1.3.3}%
\contentsline {section}{\numberline {1.4}Properties of fluids}{16}{section.1.4}%
\contentsline {subsection}{\numberline {1.4.1}Density}{16}{subsection.1.4.1}%
\contentsline {subsection}{\numberline {1.4.2}Phase}{16}{subsection.1.4.2}%
\contentsline {subsection}{\numberline {1.4.3}Temperature}{17}{subsection.1.4.3}%
\contentsline {subsection}{\numberline {1.4.4}Perfect gas model}{17}{subsection.1.4.4}%
\contentsline {subsection}{\numberline {1.4.5}Speed of sound}{18}{subsection.1.4.5}%
\contentsline {subsection}{\numberline {1.4.6}Viscosity}{19}{subsection.1.4.6}%
\contentsline {section}{\numberline {1.5}Forces on fluids}{20}{section.1.5}%
\contentsline {subsection}{\numberline {1.5.1}Gravity}{20}{subsection.1.5.1}%
\contentsline {subsection}{\numberline {1.5.2}Pressure}{20}{subsection.1.5.2}%
\contentsline {subsection}{\numberline {1.5.3}Shear}{20}{subsection.1.5.3}%
\contentsline {section}{\numberline {1.6}Basic flow quantities}{21}{section.1.6}%
\contentsline {section}{\numberline {1.7}Four balance equations}{22}{section.1.7}%
\contentsline {section}{\numberline {1.8}Classification of fluid flows}{23}{section.1.8}%
\contentsline {section}{\numberline {1.9}Limits of fluid dynamics}{25}{section.1.9}%
\contentsline {section}{\numberline {1.10}Solved problems}{26}{section.1.10}%
\contentsline {section}{\numberline {1.11}Problems}{29}{section.1.11}%
\contentsline {subsubsection}{\numberline {1.1}Reading quiz}{29}{subsubsection.1.11.0.1}%
\contentsline {subsubsection}{\numberline {1.2}Compressibility effects}{29}{subsubsection.1.11.0.2}%
\contentsline {subsubsection}{\numberline {1.3}Pressure-induced force}{29}{subsubsection.1.11.0.3}%
\contentsline {subsubsection}{\numberline {1.4}Shear-induced force}{30}{subsubsection.1.11.0.4}%
\contentsline {subsubsection}{\numberline {1.5}Speed of sound}{30}{subsubsection.1.11.0.5}%
\contentsline {subsubsection}{\numberline {1.6}Wind on a truck}{30}{subsubsection.1.11.0.6}%
\contentsline {subsubsection}{\numberline {1.7}Go-faster exhaust pipe}{31}{subsubsection.1.11.0.7}%
\contentsline {subsubsection}{\numberline {1.8}Acceleration of a particle}{32}{subsubsection.1.11.0.8}%
\contentsline {subsubsection}{\numberline {1.9}Flow classifications}{32}{subsubsection.1.11.0.9}%
\contentsline {chapter}{\numberline {2}Analysis of existing flows with\nobreakspace {}one\nobreakspace {}dimension}{35}{chapter.2}%
\contentsline {section}{\numberline {2.1}Motivation}{35}{section.2.1}%
\contentsline {section}{\numberline {2.2}One-dimensional flow problems}{35}{section.2.2}%
\contentsline {section}{\numberline {2.3}Balance of mass}{37}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Mass balance equation}{37}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Problems with the mass balance equation}{38}{subsection.2.3.2}%
\contentsline {section}{\numberline {2.4}Balance of momentum}{39}{section.2.4}%
\contentsline {section}{\numberline {2.5}Balance of energy}{41}{section.2.5}%
\contentsline {section}{\numberline {2.6}The Bernoulli equation}{43}{section.2.6}%
\contentsline {subsection}{\numberline {2.6.1}Theory}{43}{subsection.2.6.1}%
\contentsline {subsection}{\numberline {2.6.2}Reality}{44}{subsection.2.6.2}%
\contentsline {section}{\numberline {2.7}Solved problems}{46}{section.2.7}%
\contentsline {section}{\numberline {2.8}Problems}{47}{section.2.8}%
\contentsline {subsubsection}{\numberline {2.1}Reading quiz}{47}{subsubsection.2.8.0.1}%
\contentsline {subsubsection}{\numberline {2.2}Pipe expansion without losses}{47}{subsubsection.2.8.0.2}%
\contentsline {subsubsection}{\numberline {2.3}Pipe flow with losses}{48}{subsubsection.2.8.0.3}%
\contentsline {subsubsection}{\numberline {2.4}Combustor from a jet engine}{48}{subsubsection.2.8.0.4}%
\contentsline {subsubsection}{\numberline {2.5}Water jet on a truck}{49}{subsubsection.2.8.0.5}%
\contentsline {subsubsection}{\numberline {2.6}High-speed gas flow}{50}{subsubsection.2.8.0.6}%
\contentsline {chapter}{\numberline {3}Analysis of existing flows with\nobreakspace {}three\nobreakspace {}dimensions}{53}{chapter.3}%
\contentsline {section}{\numberline {3.1}Motivation}{53}{section.3.1}%
\contentsline {section}{\numberline {3.2}The Reynolds transport theorem}{53}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Control volume}{53}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Rate of change of an additive property}{54}{subsection.3.2.2}%
\contentsline {section}{\numberline {3.3}Balance of mass}{56}{section.3.3}%
\contentsline {section}{\numberline {3.4}Balance of momentum}{58}{section.3.4}%
\contentsline {section}{\numberline {3.5}Balance of angular momentum}{59}{section.3.5}%
\contentsline {section}{\numberline {3.6}Balance of energy}{61}{section.3.6}%
\contentsline {section}{\numberline {3.7}Limits of integral analysis}{61}{section.3.7}%
\contentsline {section}{\numberline {3.8}Solved problems}{62}{section.3.8}%
\contentsline {section}{\numberline {3.9}Problems}{65}{section.3.9}%
\contentsline {subsubsection}{\numberline {3.1}Reading quiz}{65}{subsubsection.3.9.0.1}%
\contentsline {subsubsection}{\numberline {3.2}Pipe bend}{65}{subsubsection.3.9.0.2}%
\contentsline {subsubsection}{\numberline {3.3}Exhaust gas deflector}{65}{subsubsection.3.9.0.3}%
\contentsline {subsubsection}{\numberline {3.4}Pelton water turbine}{66}{subsubsection.3.9.0.4}%
\contentsline {subsubsection}{\numberline {3.5}Snow plow}{67}{subsubsection.3.9.0.5}%
\contentsline {subsubsection}{\numberline {3.6}Inlet of a pipe}{67}{subsubsection.3.9.0.6}%
\contentsline {subsubsection}{\numberline {3.7}Drag on a cylindrical profile}{68}{subsubsection.3.9.0.7}%
\contentsline {subsubsection}{\numberline {3.8}Drag on a flat plate}{70}{subsubsection.3.9.0.8}%
\contentsline {subsubsection}{\numberline {3.9}Drag measurements in a wind tunnel}{71}{subsubsection.3.9.0.9}%
\contentsline {subsubsection}{\numberline {3.10}Moment on gas deflector}{72}{subsubsection.3.9.0.10}%
\contentsline {subsubsection}{\numberline {3.11}Helicopter tail moment}{72}{subsubsection.3.9.0.11}%
\contentsline {chapter}{\numberline {4}Effects of pressure}{75}{chapter.4}%
\contentsline {section}{\numberline {4.1}Motivation}{75}{section.4.1}%
\contentsline {section}{\numberline {4.2}Pressure forces on walls}{75}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Magnitude of the pressure force}{75}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Position of the pressure force}{76}{subsection.4.2.2}%
\contentsline {section}{\numberline {4.3}Pressure fields in fluids}{77}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}The direction of pressure}{77}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Pressure on an infinitesimal volume}{78}{subsection.4.3.2}%
\contentsline {section}{\numberline {4.4}Special case: pressure in static fluids}{81}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Forces in static fluids}{81}{subsection.4.4.1}%
\contentsline {subsection}{\numberline {4.4.2}Pressure and depth}{82}{subsection.4.4.2}%
\contentsline {subsection}{\numberline {4.4.3}Buoyancy}{84}{subsection.4.4.3}%
\contentsline {section}{\numberline {4.5}Solved problems}{85}{section.4.5}%
\contentsline {section}{\numberline {4.6}Problems}{87}{section.4.6}%
\contentsline {subsubsection}{\numberline {4.1}Reading quiz}{87}{subsubsection.4.6.0.1}%
\contentsline {subsubsection}{\numberline {4.2}Pressure in a static fluid}{87}{subsubsection.4.6.0.2}%
\contentsline {subsubsection}{\numberline {4.3}Pressure measurement with a U-tube}{87}{subsubsection.4.6.0.3}%
\contentsline {subsubsection}{\numberline {4.4}Straight water tank door}{88}{subsubsection.4.6.0.4}%
\contentsline {subsubsection}{\numberline {4.5}Access door on a water channel wall}{88}{subsubsection.4.6.0.5}%
\contentsline {subsubsection}{\numberline {4.6}Pressure force on a cylinder}{89}{subsubsection.4.6.0.6}%
\contentsline {subsubsection}{\numberline {4.7}Buoyancy of a barge}{89}{subsubsection.4.6.0.7}%
\contentsline {subsubsection}{\numberline {4.8}Atmospheric pressure distribution}{90}{subsubsection.4.6.0.8}%
\contentsline {chapter}{\numberline {5}Effects of shear}{93}{chapter.5}%
\contentsline {section}{\numberline {5.1}Motivation}{93}{section.5.1}%
\contentsline {section}{\numberline {5.2}Shear forces on walls}{93}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Magnitude of the shear force}{93}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Direction and position of the shear force}{94}{subsection.5.2.2}%
\contentsline {section}{\numberline {5.3}Shear fields in fluids}{94}{section.5.3}%
\contentsline {subsection}{\numberline {5.3.1}The direction of shear}{95}{subsection.5.3.1}%
\contentsline {subsection}{\numberline {5.3.2}Shear on an infinitesimal volume}{95}{subsection.5.3.2}%
\contentsline {section}{\numberline {5.4}Resistance to shear: viscosity}{99}{section.5.4}%
\contentsline {subsection}{\numberline {5.4.1}Viscosity}{99}{subsection.5.4.1}%
\contentsline {subsection}{\numberline {5.4.2}Kinematic viscosity}{100}{subsection.5.4.2}%
\contentsline {subsection}{\numberline {5.4.3}Turbulent viscosity}{101}{subsection.5.4.3}%
\contentsline {subsection}{\numberline {5.4.4}Non-Newtonian fluids}{102}{subsection.5.4.4}%
\contentsline {subsection}{\numberline {5.4.5}The no-slip condition}{102}{subsection.5.4.5}%
\contentsline {section}{\numberline {5.5}Special case: shear in simple laminar flows}{103}{section.5.5}%
\contentsline {section}{\numberline {5.6}Solved problems}{105}{section.5.6}%
\contentsline {section}{\numberline {5.7}Problems}{107}{section.5.7}%
\contentsline {subsubsection}{\numberline {5.1}Quiz}{108}{subsubsection.5.7.0.1}%
\contentsline {subsubsection}{\numberline {5.2}Flow in between two plates}{108}{subsubsection.5.7.0.2}%
\contentsline {subsubsection}{\numberline {5.3}Friction on a plate}{109}{subsubsection.5.7.0.3}%
\contentsline {subsubsection}{\numberline {5.4}Viscometer}{109}{subsubsection.5.7.0.4}%
\contentsline {subsubsection}{\numberline {5.5}Boundary layer}{109}{subsubsection.5.7.0.5}%
\contentsline {subsubsection}{\numberline {5.6}Clutch}{111}{subsubsection.5.7.0.6}%
\contentsline {chapter}{\numberline {6}Prediction of fluid flows}{113}{chapter.6}%
\contentsline {section}{\numberline {6.1}Motivation}{113}{section.6.1}%
\contentsline {section}{\numberline {6.2}Organizing calculations}{113}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Problem description}{113}{subsection.6.2.1}%
\contentsline {subsection}{\numberline {6.2.2}The total time derivative}{114}{subsection.6.2.2}%
\contentsline {section}{\numberline {6.3}Equations for all flows}{117}{section.6.3}%
\contentsline {subsection}{\numberline {6.3.1}Balance of mass}{117}{subsection.6.3.1}%
\contentsline {subsection}{\numberline {6.3.2}Balance of linear momentum}{119}{subsection.6.3.2}%
\contentsline {subsection}{\numberline {6.3.3}Balance of energy}{122}{subsection.6.3.3}%
\contentsline {subsection}{\numberline {6.3.4}Other terms and equations}{123}{subsection.6.3.4}%
\contentsline {subsection}{\numberline {6.3.5}Interlude: where does this leave us?}{124}{subsection.6.3.5}%
\contentsline {section}{\numberline {6.4}Equations for incompressible flow}{125}{section.6.4}%
\contentsline {subsection}{\numberline {6.4.1}Balance of mass}{125}{subsection.6.4.1}%
\contentsline {subsection}{\numberline {6.4.2}Balance of linear momentum}{126}{subsection.6.4.2}%
\contentsline {subsection}{\numberline {6.4.3}The Bernoulli equation (again)}{129}{subsection.6.4.3}%
\contentsline {section}{\numberline {6.5}CFD: the Navier-Stokes equations in practice}{129}{section.6.5}%
\contentsline {section}{\numberline {6.6}Solved problems}{130}{section.6.6}%
\contentsline {section}{\numberline {6.7}Problems}{133}{section.6.7}%
\contentsline {subsubsection}{\numberline {6.1}Quiz}{133}{subsubsection.6.7.0.1}%
\contentsline {subsubsection}{\numberline {6.2}Revision questions}{133}{subsubsection.6.7.0.2}%
\contentsline {subsubsection}{\numberline {6.3}Acceleration field}{133}{subsubsection.6.7.0.3}%
\contentsline {subsubsection}{\numberline {6.4}Volumetric dilatation rate}{134}{subsubsection.6.7.0.4}%
\contentsline {subsubsection}{\numberline {6.5}Incompressibility}{134}{subsubsection.6.7.0.5}%
\contentsline {subsubsection}{\numberline {6.6}Missing components}{134}{subsubsection.6.7.0.6}%
\contentsline {subsubsection}{\numberline {6.7}Another acceleration field}{134}{subsubsection.6.7.0.7}%
\contentsline {subsubsection}{\numberline {6.8}Vortex}{134}{subsubsection.6.7.0.8}%
\contentsline {subsubsection}{\numberline {6.9}Pressure fields}{135}{subsubsection.6.7.0.9}%
\contentsline {chapter}{\numberline {7}Pipe flows}{137}{chapter.7}%
\contentsline {section}{\numberline {7.1}Motivation}{137}{section.7.1}%
\contentsline {section}{\numberline {7.2}Frictionless flow in pipes}{137}{section.7.2}%
\contentsline {section}{\numberline {7.3}Parameters to quantify losses in pipes}{139}{section.7.3}%
\contentsline {section}{\numberline {7.4}Laminar flow in pipes}{139}{section.7.4}%
\contentsline {subsection}{\numberline {7.4.1}Laminar flow between plates}{139}{subsection.7.4.1}%
\contentsline {subsection}{\numberline {7.4.2}Laminar flow in pipes}{141}{subsection.7.4.2}%
\contentsline {section}{\numberline {7.5}Turbulent flow in pipes}{144}{section.7.5}%
\contentsline {subsection}{\numberline {7.5.1}When is a pipe flow turbulent?}{144}{subsection.7.5.1}%
\contentsline {subsection}{\numberline {7.5.2}Characteristics of turbulent flow}{145}{subsection.7.5.2}%
\contentsline {subsection}{\numberline {7.5.3}Velocity profile in turbulent pipe flow}{146}{subsection.7.5.3}%
\contentsline {subsection}{\numberline {7.5.4}Pressure losses in turbulent pipe flow}{146}{subsection.7.5.4}%
\contentsline {section}{\numberline {7.6}Engineer’s guide to pipe flows}{148}{section.7.6}%
\contentsline {subsection}{\numberline {7.6.1}Summary so far}{148}{subsection.7.6.1}%
\contentsline {subsection}{\numberline {7.6.2}Choosing laminar or turbulent flow}{149}{subsection.7.6.2}%
\contentsline {subsection}{\numberline {7.6.3}Pressure losses in laminar flow}{149}{subsection.7.6.3}%
\contentsline {subsection}{\numberline {7.6.4}Pressure losses in turbulent flow}{149}{subsection.7.6.4}%
\contentsline {subsection}{\numberline {7.6.5}Calculating pumping and turbining power}{150}{subsection.7.6.5}%
\contentsline {section}{\numberline {7.7}Solved problems}{150}{section.7.7}%
\contentsline {section}{\numberline {7.8}Problems}{153}{section.7.8}%
\contentsline {subsubsection}{\numberline {7.1}Reading quiz}{153}{subsubsection.7.8.0.1}%
\contentsline {subsubsection}{\numberline {7.2}Revision questions}{155}{subsubsection.7.8.0.2}%
\contentsline {subsubsection}{\numberline {7.3}Air flow in a small pipe}{155}{subsubsection.7.8.0.3}%
\contentsline {subsubsection}{\numberline {7.4}Water piping}{155}{subsubsection.7.8.0.4}%
\contentsline {subsubsection}{\numberline {7.5}Design of a water piping system}{155}{subsubsection.7.8.0.5}%
\contentsline {subsubsection}{\numberline {7.6}Major oil pipeline}{157}{subsubsection.7.8.0.6}%
\contentsline {subsubsection}{\numberline {7.7}Pump with pipe expansion}{158}{subsubsection.7.8.0.7}%
\contentsline {subsubsection}{\numberline {7.8}Piping and power of a water turbine}{158}{subsubsection.7.8.0.8}%
\contentsline {subsubsection}{\numberline {7.9}Politically incorrect fluid mechanics}{159}{subsubsection.7.8.0.9}%
\contentsline {chapter}{\numberline {8}Engineering models}{163}{chapter.8}%
\contentsline {section}{\numberline {8.1}Motivation}{163}{section.8.1}%
\contentsline {section}{\numberline {8.2}Comparing influences: the weighted momentum balance}{163}{section.8.2}%
\contentsline {subsection}{\numberline {8.2.1}Principle}{163}{subsection.8.2.1}%
\contentsline {subsection}{\numberline {8.2.2}The non-dimensional Navier-Stokes equation}{164}{subsection.8.2.2}%
\contentsline {subsection}{\numberline {8.2.3}The flow parameters of Navier-Stokes}{166}{subsection.8.2.3}%
\contentsline {subsection}{\numberline {8.2.4}Flow parameters obtained as force ratios}{168}{subsection.8.2.4}%
\contentsline {subsection}{\numberline {8.2.5}The Reynolds number in practice}{168}{subsection.8.2.5}%
\contentsline {section}{\numberline {8.3}Making models}{169}{section.8.3}%
\contentsline {section}{\numberline {8.4}Comparing results: coefficients}{171}{section.8.4}%
\contentsline {subsection}{\numberline {8.4.1}Principle}{171}{subsection.8.4.1}%
\contentsline {subsection}{\numberline {8.4.2}Force coefficients}{171}{subsection.8.4.2}%
\contentsline {subsection}{\numberline {8.4.3}Power coefficient, and other coefficients}{172}{subsection.8.4.3}%
\contentsline {subsection}{\numberline {8.4.4}Non-dimensionalizing all the problems}{173}{subsection.8.4.4}%
\contentsline {section}{\numberline {8.5}Solved problems}{174}{section.8.5}%
\contentsline {section}{\numberline {8.6}Problems}{175}{section.8.6}%
\contentsline {subsubsection}{\numberline {8.1}Reading quiz}{175}{subsubsection.8.6.0.1}%
\contentsline {subsubsection}{\numberline {8.2}Scaling a golf ball}{176}{subsubsection.8.6.0.2}%
\contentsline {subsubsection}{\numberline {8.3}Fluid mechanics of a giant airliner}{177}{subsubsection.8.6.0.3}%
\contentsline {subsubsection}{\numberline {8.4}Scale effects on a dragonfly}{178}{subsubsection.8.6.0.4}%
\contentsline {subsubsection}{\numberline {8.5}Formula One testing}{178}{subsubsection.8.6.0.5}%
\contentsline {chapter}{\numberline {9}Dealing with\nobreakspace {}turbulence}{181}{chapter.9}%
\contentsline {section}{\numberline {9.1}Motivation}{181}{section.9.1}%
\contentsline {section}{\numberline {9.2}Recognizing turbulence}{182}{section.9.2}%
\contentsline {subsection}{\numberline {9.2.1}A brief definition}{182}{subsection.9.2.1}%
\contentsline {subsection}{\numberline {9.2.2}Chaos, not randomness}{182}{subsection.9.2.2}%
\contentsline {subsection}{\numberline {9.2.3}Growth and decay}{183}{subsection.9.2.3}%
\contentsline {subsection}{\numberline {9.2.4}A cascade of vortices}{184}{subsection.9.2.4}%
\contentsline {subsection}{\numberline {9.2.5}Not turbulence}{184}{subsection.9.2.5}%
\contentsline {section}{\numberline {9.3}The effects of turbulence}{185}{section.9.3}%
\contentsline {subsection}{\numberline {9.3.1}Dissipation (losses)}{185}{subsection.9.3.1}%
\contentsline {subsection}{\numberline {9.3.2}Main flow patterns}{185}{subsection.9.3.2}%
\contentsline {subsection}{\numberline {9.3.3}Mixing}{186}{subsection.9.3.3}%
\contentsline {section}{\numberline {9.4}Quantifying turbulence}{186}{section.9.4}%
\contentsline {subsection}{\numberline {9.4.1}Average and fluctuation}{186}{subsection.9.4.1}%
\contentsline {subsection}{\numberline {9.4.2}Turbulence intensity}{187}{subsection.9.4.2}%
\contentsline {subsection}{\numberline {9.4.3}The size of eddies}{187}{subsection.9.4.3}%
\contentsline {subsection}{\numberline {9.4.4}Turbulent kinetic energy and dissipation rate}{188}{subsection.9.4.4}%
\contentsline {subsection}{\numberline {9.4.5}Turbulence anisotropy and inhomogeneity}{190}{subsection.9.4.5}%
\contentsline {section}{\numberline {9.5}Computing turbulent flow}{191}{section.9.5}%
\contentsline {subsection}{\numberline {9.5.1}Basic premise}{191}{subsection.9.5.1}%
\contentsline {subsection}{\numberline {9.5.2}Accounting for turbulence}{191}{subsection.9.5.2}%
\contentsline {section}{\numberline {9.6}Commented bibliography}{192}{section.9.6}%
\contentsline {section}{\numberline {9.7}Problems}{195}{section.9.7}%
\contentsline {subsubsection}{\numberline {9.1}Hypothetical flow}{196}{subsubsection.9.7.0.1}%
\contentsline {subsubsection}{\numberline {9.2}Turbulent channel flow}{196}{subsubsection.9.7.0.2}%
\contentsline {subsubsection}{\numberline {9.3}Cumulus cloud}{197}{subsubsection.9.7.0.3}%
\contentsline {subsubsection}{\numberline {9.4}Reactor tank}{197}{subsubsection.9.7.0.4}%
\contentsline {chapter}{\numberline {10}Flow near walls}{201}{chapter.10}%
\contentsline {section}{\numberline {10.1}Motivation}{201}{section.10.1}%
\contentsline {section}{\numberline {10.2}The concept of boundary layer}{201}{section.10.2}%
\contentsline {subsection}{\numberline {10.2.1}Rationale}{201}{subsection.10.2.1}%
\contentsline {subsection}{\numberline {10.2.2}Why do we study the boundary layer?}{203}{subsection.10.2.2}%
\contentsline {subsection}{\numberline {10.2.3}Characterization of the boundary layer}{203}{subsection.10.2.3}%
\contentsline {section}{\numberline {10.3}Laminar boundary layers}{205}{section.10.3}%
\contentsline {subsection}{\numberline {10.3.1}Governing equations}{205}{subsection.10.3.1}%
\contentsline {subsection}{\numberline {10.3.2}Blasius’ solution}{206}{subsection.10.3.2}%
\contentsline {section}{\numberline {10.4}Boundary layer transition}{207}{section.10.4}%
\contentsline {section}{\numberline {10.5}Turbulent boundary layers}{208}{section.10.5}%
\contentsline {section}{\numberline {10.6}Flow separation}{209}{section.10.6}%
\contentsline {section}{\numberline {10.7}Solved problems}{212}{section.10.7}%
\contentsline {section}{\numberline {10.8}Problems}{215}{section.10.8}%
\contentsline {subsubsection}{\numberline {10.1}Water and air flow}{215}{subsubsection.10.8.0.1}%
\contentsline {subsubsection}{\numberline {10.2}Boundary layer sketches}{216}{subsubsection.10.8.0.2}%
\contentsline {subsubsection}{\numberline {10.3}Shear force due to boundary layer}{217}{subsubsection.10.8.0.3}%
\contentsline {subsubsection}{\numberline {10.4}Wright Flyer I}{217}{subsubsection.10.8.0.4}%
\contentsline {subsubsection}{\numberline {10.5}Power lost to shear on an airliner fuselage}{217}{subsubsection.10.8.0.5}%
\contentsline {subsubsection}{\numberline {10.6}Laminar wing profile}{218}{subsubsection.10.8.0.6}%
\contentsline {subsubsection}{\numberline {10.7}Separation mechanism}{218}{subsubsection.10.8.0.7}%
\contentsline {chapter}{\numberline {11}Large- and small-scale\nobreakspace {}flows}{223}{chapter.11}%
\contentsline {section}{\numberline {11.1}Motivation}{223}{section.11.1}%
\contentsline {section}{\numberline {11.2}Flow at large scales}{223}{section.11.2}%
\contentsline {subsection}{\numberline {11.2.1}Problem statement}{223}{subsection.11.2.1}%
\contentsline {subsection}{\numberline {11.2.2}Investigation of inviscid flows}{224}{subsection.11.2.2}%
\contentsline {section}{\numberline {11.3}Plotting velocity with functions}{225}{section.11.3}%
\contentsline {subsection}{\numberline {11.3.1}Kinematics that fit conservation laws}{225}{subsection.11.3.1}%
\contentsline {subsection}{\numberline {11.3.2}Strengths and weaknesses of potential flow}{226}{subsection.11.3.2}%
\contentsline {subsection}{\numberline {11.3.3}Superposition: the lifting cylinder}{227}{subsection.11.3.3}%
\contentsline {subsection}{\numberline {11.3.4}Circulating cylinder}{229}{subsection.11.3.4}%
\contentsline {subsection}{\numberline {11.3.5}Modeling lift with circulation}{231}{subsection.11.3.5}%
\contentsline {section}{\numberline {11.4}Flow at very small scales}{234}{section.11.4}%
\contentsline {section}{\numberline {11.5}Problems}{237}{section.11.5}%
\contentsline {subsubsection}{\numberline {11.1}Volcanic ash from the Eyjafjallajökull}{237}{subsubsection.11.5.0.1}%
\contentsline {subsubsection}{\numberline {11.2}Water drop}{237}{subsubsection.11.5.0.2}%
\contentsline {subsubsection}{\numberline {11.3}Idealized flow over a hangar roof}{237}{subsubsection.11.5.0.3}%
\contentsline {subsubsection}{\numberline {11.4}Cabling of the Wright Flyer}{239}{subsubsection.11.5.0.4}%
\contentsline {subsubsection}{\numberline {11.5}Ping pong ball}{239}{subsubsection.11.5.0.5}%
\contentsline {subsubsection}{\numberline {11.6}Flow field of a tornado}{242}{subsubsection.11.5.0.6}%
\contentsline {subsubsection}{\numberline {11.7}Lift on a symmetrical object}{244}{subsubsection.11.5.0.7}%
\contentsline {subsubsection}{\numberline {11.8}Air flow over a wing profile}{244}{subsubsection.11.5.0.8}%
\contentsline {chapter}{Appendix}{247}{section*.143}%
\contentsline {section}{\numberline {A1}Notation}{248}{section.Alph0.1}%
\contentsline {section}{\numberline {A2}Vector operations}{249}{section.Alph0.2}%
\contentsline {subsection}{\numberline {A2.1}Vector dot product}{249}{subsection.Alph0.2.1}%
\contentsline {subsection}{\numberline {A2.2}Vector cross product}{249}{subsection.Alph0.2.2}%
\contentsline {section}{\numberline {A3}Field operators}{252}{section.Alph0.3}%
\contentsline {subsection}{\numberline {A3.1}Gradient}{252}{subsection.Alph0.3.1}%
\contentsline {subsection}{\numberline {A3.2}Divergent}{252}{subsection.Alph0.3.2}%
\contentsline {subsection}{\numberline {A3.3}Advective}{253}{subsection.Alph0.3.3}%
\contentsline {subsection}{\numberline {A3.4}Laplacian}{253}{subsection.Alph0.3.4}%
\contentsline {subsection}{\numberline {A3.5}Curl}{254}{subsection.Alph0.3.5}%
\contentsline {section}{\numberline {A4}Derivations of the Bernoulli equation}{255}{section.Alph0.4}%
\contentsline {subsection}{\numberline {A4.1}The Bernoulli equation from the energy equation}{255}{subsection.Alph0.4.1}%
\contentsline {subsection}{\numberline {A4.2}The Bernoulli equation from the integral momentum equation}{255}{subsection.Alph0.4.2}%
\contentsline {subsection}{\numberline {A4.3}The Bernoulli equation from the Navier-Stokes equation}{255}{subsection.Alph0.4.3}%
\contentsline {section}{\numberline {A5}Flow parameters as force ratios}{258}{section.Alph0.5}%
\contentsline {subsection}{\numberline {A5.1}Acceleration vs. viscous forces: the Reynolds number}{258}{subsection.Alph0.5.1}%
\contentsline {subsection}{\numberline {A5.2}Acceleration vs. gravity force: the Froude number}{259}{subsection.Alph0.5.2}%
\contentsline {subsection}{\numberline {A5.3}Acceleration vs. elastic forces: the Mach number}{259}{subsection.Alph0.5.3}%
\contentsline {subsection}{\numberline {A5.4}Other force ratios}{260}{subsection.Alph0.5.4}%
\contentsline {section}{\numberline {A6}Details of the winter 2020-2021 final examination (updated February 2021)}{261}{section.Alph0.6}%
\contentsline {section}{\numberline {A7}Example of previous examinations}{263}{section.Alph0.7}%
\contentsline {section}{\numberline {A8}List of references}{294}{section.Alph0.8}%

\renewcommand{\lastedityear}{2020}
 \renewcommand{\lasteditmonth}{04}
   \renewcommand{\lasteditday}{26}
\renewcommand{\numberofthischapter}{2}
\renewcommand{\titleofthischapter}{\namechaptertwotitle}
\atstartofexercises

\fluidmechexercisestitle

\mecafluexboxen

\begin{boiboiboite}

Balance of mass in a fixed control volume with steady flow:
\begin{IEEEeqnarray}{rCl}
	0 & = & \Sigma \left[\rho V_\perp A \right]_\text{incoming} + \Sigma \left[\rho V_\perp A \right]_\text{outgoing} \ztag{\ref{eq_mass_oned}}
\end{IEEEeqnarray}
\begin{equationterms}
	\item where $V_\perp$ is negative inwards, positive outwards.
\end{equationterms}

Balance of momentum in a fixed control volume with steady flow:
\begin{IEEEeqnarray}{rCl}
	\vec F_\text{net on fluid} & = & \Sigma \left[\rho V_\perp A \vec V\right]_\text{incoming} + \Sigma \left[\rho V_\perp A \vec V\right]_\text{outgoing}\ztag{\ref{eq_linearmom_oned}}
\end{IEEEeqnarray}
\begin{equationterms}
	\item where $V_\perp$ is negative inwards, positive outwards.
\end{equationterms}

Balance of energy in a fixed control volume with steady flow:
\begin{IEEEeqnarray}{rCcl}
	\dot Q_{\net} + \dot W_\text{shaft, net} &=& & \Sigma \left[\dot m \left(i + \frac{p}{\rho} + \frac{1}{2} V^2 + g z \right) \right]_\inn \nonumber\\
		&& +&  \Sigma \left[\dot m \left(i + \frac{p}{\rho} + \frac{1}{2} V^2 + g z \right) \right]_\out \ztag{\ref{eq_sfee}}
\end{IEEEeqnarray}
\begin{equationterms}
	\item where $\dot m$ is negative inwards, positive outwards.
\end{equationterms}

\end{boiboiboite}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Reading quiz}

	\quiz

%%%%
\subsubsection{Pipe expansion without losses}
%\wherefrom{\cczero \oc}
\label{exo_pipe_expansion}

	Water flows from left to right in a pipe, as shown in fig.~\ref{fig_simple_pipe_expansion}. On the left, the diameter is~\SI{8}{\centi\metre}, the water arrives with a uniform velocity of \SI{1,5}{\metre\per\second}. The diameter increases gently until it reaches \SI{16}{\centi\metre}; the expansion is smooth, so that losses (specifically, energy losses due to wall friction and flow separation) are negligible.
	\begin{figure}
		\begin{center}
		\includegraphics[width=7.5cm]{simple_pipe_expansion}
		\vspace{-0.5cm}
		\end{center}
		\supercaption{A simple pipe expansion, with water flowing from left to right.}{\cczero \oc}
		\label{fig_simple_pipe_expansion}
	\end{figure}
	\begin{enumerate}
		\item What are the mass and volume flows at inlet and outlet?
		\item What is the average velocity of the water at the right end of the expansion?
		\item What is the pressure change in the water across the expansion?
	\end{enumerate}
	
	The volume flow of water in the pipe is now doubled.
	\begin{enumerate}
		\shift{3}
		\item What is the new pressure change?
	\end{enumerate}
	
	The water is drained from the pipe, and instead, air with density \SI{1,225}{\kilogram\per\metre\cubed} is flowed in the pipe, incoming with a uniform velocity of \SI{1,5}{\metre\per\second}.
	\begin{enumerate}
		\shift{4}
		\item What is the new pressure change?
	\end{enumerate}

%%%%
\subsubsection{Pipe flow with losses}
%\wherefrom{\cczero \oc}
\label{exo_pipe_with_losses}
	
	Water flows in a long pipe which has constant diameter; a valve is installed in the middle of the pipe length (fig.~\ref{fig_simple_straight_pipe_valve}). Water arrives the pipe with a uniform velocity of \SI{1,5}{\metre\per\second} and the pipe diameter is \SI{250}{\milli\metre}.
	\begin{figure}
		\begin{center}
		\includegraphics[width=0.8\textwidth]{simple_straight_pipe_valve}
		\vspace{-0.5cm}
		\end{center}
		\supercaption{A simple, straight pipe, featuring a partially-open valve in the center}{\cczero \oc}
		\label{fig_simple_straight_pipe_valve}
	\end{figure}	
		
	The pipe itself and the valve, together, induce a pressure loss which can be quantified using the dimensionless \vocab{loss coefficient} $K_\text{valve}$ (we later will later encounter it as eq.~\ref{eq_def_loss_coeff} p.~\pageref{eq_def_loss_coeff}). With this tool, the pressure loss $\delta p_\text{valve}$ is related to the mean incoming speed $V_\text{incoming}$ as:
	\begin{IEEEeqnarray}{rCcCl}
		K_\text{valve} &\equiv& \frac{|\Delta p_\text{valve}|}{\frac{1}{2} \rho V_\text{incoming}^2} &=& \num{2,6}\label{eq_k_valve}
	\end{IEEEeqnarray}
	% Note: K = 2,6 is guesstimate: 2 for swing check valve (from White p.401) + {f L/D = 0.05 * 3 / 0.25 = 0,6}

	\begin{enumerate}
		\item What is the outlet velocity of the water?\\
			(note: this is a classical “trick” question! :-)
		\item What is the pressure drop of the water across the pipe?
		\item What is the power required to pump the water across the pipe?
		\item If the heat losses of the pipe and valve are negligible, what is the temperature increase of the water?
	\end{enumerate}


%%%%
\subsubsection{Combustor from a jet engine}
%\wherefrom{\cczero \oc}
\label{exo_combustor}

	A jet engine is equipped with several \vocab{combustors} (sometimes also called \vocab{combustion chambers}). We are interested in fluid flow through one such combustor, shown in fig.~\ref{fig_combustor}. Air from the compressor enters the combustor, is mixed with fuel, and combustion occurs, which greatly increases the temperature and specific volume of the mix, before it is run through the turbine.
	\begin{figure}
		\begin{center}
		\includegraphics[width=0.8\textwidth]{Core_section_of_a_sectioned_Rolls-Royce_Turboméca_Adour_turbofan_cropped_flipped.jpg}
		\vspace{-0.5cm}
		\end{center}
		\supercaption{A combustor in a sectioned jet engine (here, a Turboméca Adour). Air enters from the left, out of the compressor (whose blades are painted blue). It leaves the combustor on the right side, into the turbine. In the combustor, high-temperature, steady combustion takes place.}{\wcfile{Core_section_of_a_sectioned_Rolls-Royce_Turboméca_Adour_turbofan.jpg}{Photo} \ccbysa \oc}
		\label{fig_combustor}
	\end{figure}
	
	The conditions at inlet are as follows:
	\begin{itemize}
		\item Air mass flow: \SI{0,5}{\kilogram\per\second};
		\item Air properties: \SI{25}{\bar}, \SI{1050}{\degreeCelsius}, \SI{12}{\metre\per\second}
		\item Fuel mass flow: \SI{5}{\gram\per\second}.
	\end{itemize}
	% Fuel mass flow rough calculation: \dot Q = (\dot m * c_p * \Delta T)_air = 250 kW
	% \dot Q  /  c_comb = 0,005 kg/s
	
	At the outlet, the hot gases have pressure \SI{24,5}{\bar} and temperature \SI{1550}{\degreeCelsius}, and exit with a speed of~\SI{50}{\metre\per\second}.

	We assume that the incoming air and outgoing gas have the same thermodynamic properties: $c_\text{v} = \SI{718}{\joule\per\kilogram\per\kelvin}$, $R_\air = \SI{287}{\joule\per\kilogram\per\kelvin}$.
	
	\begin{enumerate}
		\item What are the mass flows at inlet and outlet?
		\item What are the volume flows at inlet and at outlet?
		\item What is the power provided to the flow as heat?
		\item What is the net force exerted on the gas as it travels through the combustor?
	\end{enumerate}

%%%%
\subsubsection{Water jet on a truck}
%\wherefrom{from White \smallcite{white2008} Ex3.9}
\label{exo_water_jet}
	
	A water nozzle shoots water towards the back of a small stationary van. It has a~\SI{3}{\centi\metre\squared} cross-sectional area, and the water speed at the nozzle outlet is $V_\text{jet} = \SI{20}{\metre\per\second}$.
	As the horizontal water jet hits the back of the van, it is split in two symmetrical vertical flows (\cref{fig_water_wall}). The two opposite vertical jets have same mass flow and same velocity ($V_2=V_3=\SI{20}{\metre\per\second}$).
	\begin{figure}
		\begin{center}
		\includegraphics[width=0.9\textwidth]{nozzle_truck}
		\vspace{-0.5cm}
		\end{center}
		\supercaption{A water jet flowing out of a nozzle (left), and impacting the vertical back surface of a small electric truck, on the right.}{\wcfile{Nozzle flow case 1.svg}{Figure} \cczero \oc}
		\label{fig_water_wall}
	\end{figure}
	\begin{enumerate}
		\item What is the net force exerted on the water by the truck?
		\item What is the net force exerted on the truck by the water?
	\end{enumerate}
	
	Now, the truck moves longitudinally in the same direction as the water jet, with a speed $V_\text{truck} = \SI{15}{\metre\per\second}$. \\
	(This is a crude conceptual setup, which allows us to approach conceptually the case where water acts on the blades of a turbine.)
	\begin{enumerate}
		\shift{2}
		\item What is the new force exerted by the water on the truck?
		\item What is the mechanical power transmitted to the truck?
		\item How would the power be modified if the volume flow was kept constant, but the diameter of the nozzle was reduced? (briefly justify your answer, \eg in 30 words or less)
	\end{enumerate}


%%%%
\subsubsection{High-speed gas flow}
%\wherefrom{\ccbysa \oc}
\label{exo_high_speed_gas_flow}

	Scientists build a very high-speed wind tunnel. For this, they build a large compressed air tank. Air escapes from the tank into a pipe which decreasing cross-section, as shown in fig.~\ref{fig_simple_converging_diverging_nozzle}. The pipe diameter reaches a minimum (at the tunnel \vocab{throat}), and then it expands again, before discharging into the atmosphere. 
	\begin{figure}[ht]
		\begin{center}
		\includegraphics[width=0.8\textwidth]{simple_converging_diverging_nozzle}
		\vspace{-0.5cm}
		\end{center}
		\supercaption{A converging-diverging nozzle. Air flows from the left tank to the right outlet, with a contraction in the middle.}{\wcfile{Simple converging diverging nozzle.svg}{Figure} \cczero \oc}
		\label{fig_simple_converging_diverging_nozzle}
	\end{figure}
	
	For simplicity, we assume that heat losses through the tunnel walls are negligible, and that the fluid has uniformly-distributed velocity in cross-sections of the pipe. 
	
	In the tank (point 1), the air is stationary, with pressure \SI{7,8}{\bar} and temperature \SI{246,6}{\degreeCelsius}.
	
	At the throat (point 2), the pressure and temperature have dropped to \SI{4,2}{\bar} and \SI{160}{\degreeCelsius}. The velocity has reached \SI{417,2}{\metre\per\second}. The throat cross-section is \SI{0,01}{\metre\squared}.
	\begin{enumerate}
		\item What is the mass flow through the tunnel?
		%\item What is the Mach number in the tank and at the throat?
		%\item What is the net force exerted on the fluid between the tank and the throat?
		\item What is the kinetic energy per unit mass of the air at the throat?
	\end{enumerate}
	
	Downstream of the throat, the pressure keeps dropping. By the time it reaches a point 3, the air has seen its pressure and temperature drop to \SI{1,38}{\bar} and \SI{43}{\degreeCelsius}.
	\begin{enumerate}
		\shift{2}
		\item What is the fluid velocity at point~3?\\
			(if you need to convince yourself that $A_3>A_1$, you may also calculate the cross-section area)
		%\item What is the Mach number at point~3?
		\item What is the net force exerted on the fluid between the points 2 and~3?
		\item What is the kinetic energy per unit mass of the air at point~3?
	\end{enumerate}
	
	Once it has passed point 3, the air undergoes complex loss-inducing evolutions (including going through a \vocab{shock wave}, where its properties change very suddenly), before it discharges into the atmosphere (point~4) with pressure \SI{1}{\bar} and temperature~\SI{165}{\degreeCelsius}.	
	\begin{enumerate}
		\shift{5}
		\item What is the fluid velocity at outlet?
		\item What is the outlet cross-section area?
		%\item What is the Mach number at outlet?
		\item What is the net force exerted on the fluid between section 3 and the outlet?
		\item What is the kinetic energy per unit mass of the air at the outlet?
	\end{enumerate}
	

\clearpage	
\subsubsection*{Answers}
\startofanswers
\begin{enumerate}
	\shift{1}
	\item p.~\pageref{exo_pipe_expansion}
		\begin{enumerate}
			\item At both inlet and outlet, $\dot m = \SI{7,53}{\kilogram\per\second}$ and $\dot \vol = \SI{7,53}{\liter\per\second}$
			\itme $V_2 = \SI{0,375}{\metre\per\second}$
			\item $\Delta p_{1\to 2} = \SI{+1054}{\pascal}$
			\item $\Delta p_{3\to 4} = \SI{+4218}{\pascal}$
			\item $\Delta p_{5\to 6} = \SI{+1,29}{\pascal}$
		\end{enumerate}
	\item p.~\pageref{exo_pipe_with_losses}
		\begin{enumerate}
			\item $V_2 = V_1 = \SI{1,5}{\metre\per\second}$ by application of the mass balance equation; although a mis-application of the energy equation would suggest otherwise
			\item With eq.~\ref{eq_k_valve}, $\Delta p_\text{valve} = \SI{-2925}{\pascal}$
			\item $\dot W_\text{injection} = \SI{-215,37}{\watt}$
			\item With eq.~\ref{eq_sfee}, $\Delta T = \SI{+0,7}{\milli\kelvin}$ (very small!)
		\end{enumerate}
	\item p.~\pageref{exo_combustor}
		\begin{enumerate}
			\item $|\dot m_1| = \num{0,005} + \SI{0,5}{\kilogram\per\second}$ and $|\dot m_2| = \SI{0,505}{\kilogram\per\second}$
			\item $\dot \vol_1 = \SI{0,0759}{\metre\cubed\per\second}$ \& $\dot \vol_2 = \SI{0,1078}{\metre\cubed\per\second}$ (there is no volume balance equation!)
			\item $\dot Q = \SI{+261}{\kilo\watt}$ (using $V_2 = \SI{50}{\metre\per\second}$)
			\item $F_\net = \SI{+19,25}{\newton}$ (in flow-wise direction)
		\end{enumerate}
	\item p.~\pageref{exo_water_jet}
		\begin{enumerate}
			\item $F_\text{net on water} = \SI{-120}{\newton}$
			\item $\vec F_\text{water/truck} = -\vec F_\text{net on water}$
			\item $F_\text{net on water} = \SI{-7,5}{\newton}$
			\item $\dot W_\text{truck} = \SI{112,5}{\watt}$
		\end{enumerate}
	\item p.~\pageref{exo_high_speed_gas_flow}
		\begin{enumerate}
			\item With eq.~\ref{eq_sfee}, $V_2 = \SI{417,2}{\metre\per\second}$, and so $\dot m_2 = \dot m = \SI{14,1}{\kilogram\per\second}$
			%\item $\ma_1 = \num{0}$ (zero velocity) and $\ma = \num{1}$ (a classical feature of compressible flow expansions)
			%\item $F_{\net 1 \to 2} = \SI{+5,882}{\kilo\newton}$ (flow-wise)
			\item $e_{k2} = \SI{87,03}{\kilo\joule\per\kilogram}$
			\item With eq.~\ref{eq_sfee}, $V_3 = \SI{638,71}{\metre\per\second}$ (you may then calculate $\rho_3$ and obtain $A_3 > A_2$ even though $V_3 > V_2$, a classical feature of supersonic flows)
			%\item $\ma_3 = \num{1,795}$
			\item $F_{\net 2 \to 3} = \SI{+3,137}{\kilo\newton}$
			\item $e_{k3} = \SI{204,61}{\kilo\joule\per\kilogram}$
			\item $V_4 = \SI{405}{\metre\per\second}$
			\item $\rho_4 = \SI{0,7952}{\kilogram\per\second}$ and so $A_4 = \SI{0,0438}{\metre\squared}$
			%\item $\ma_4 = \num{0,96}$
			\item $F_{\net 3 \to 4} = \SI{-3,309}{\kilo\newton}$ (so, against the flow direction)
			\item $e_{k4} = \SI{82,01}{\kilo\joule\per\kilogram}$
		\end{enumerate}
\end{enumerate}

\atendofexercises

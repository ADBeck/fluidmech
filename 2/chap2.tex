\renewcommand{\lastedityear}{2020}
 \renewcommand{\lasteditmonth}{05}
   \renewcommand{\lasteditday}{22}
\renewcommand{\numberofthischapter}{2}
\renewcommand{\titleofthischapter}{\namechaptertwotitle}

\fluidmechchaptertitle
\label{chap_two}

\mecafluboxen

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Motivation}

	In this chapter, we learn to analyze fluid flows for which a lot of information is already available. We want, when confronted to a simple flow (for example, flow entering and leaving a machine), to be able to answer three questions:
	\begin{itemize}
		\item What is the mass flow in each inlet and outlet?
		\item What is the force required to move the flow?
		\item What energy transfers are required for this movement?
	\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{One-dimensional flow problems}

	\agthumb{237}{how many dimensions are enough for you?}{0.5}{}
	The method we develop here is called \vocab{integral analysis}, because it involves calculating the overall (integral) effect of the fluid flowing through a considered volume. In this chapter, we consider one-dimensional flows (at least in a loose definition); we will consider more advanced cases in \chapterthree.

	For now, we are interested in flows where four conditions are met:
	\begin{enumerate}
		\item There is a clearly identified inlet and outlet;
		\item At inlet and outlet, the fluid properties are uniform, so that they can be evaluated in bulk (\eg the inlet has only one velocity, one temperature etc.);
		\item There are no significant changes in flow direction;
		\item A lot of information is available about the fluid properties at inlet.
	\end{enumerate}
	Providing that those conditions are met, we can answer the question: what is the \emph{net} effect of the fluid flow through the considered volume?
	
	In order to write useful equations, we need to begin with rigorous definitions, with the help of figure~\ref{fig_cv_simple}:
	\begin{itemize}
		\item We call \vocab{control volume} a certain volume we are interested in. Fluid flows through the control volume.\\
				In this chapter, the control volume does not change with time. The fluid flow does not change in time, either (\ie the flow is steady). The fluid enters and leaves the control volume at a clearly-identifiable inlet and outlet.
		\item At a certain instant, the mass of fluid that is inside the control volume is called the \vocab{system}. The system is traveling. At a later point in time, it has moved and deformed.		
	\end{itemize}	
	\begin{figure}
		\begin{center}
			\includegraphics[width=0.6\textwidth]{concept_control_volume_system_simple.png}
		\end{center}
		\supercaption{A control volume within a flow. The \vocab{system} is the amount of mass included within the control volume at a given time. Because mass enters and leaves the control volume, the system is being moved and deformed (bottom).}{\wcfile{System control volume integral analysis.svg}{Figure} \cczero \oc}
		\label{fig_cv_simple}
	\end{figure}
	The three equations that we write in this chapter state that basic physical laws apply to the system. They are \vocab{balance equations} (see \S\ref{ch_conservation_equations} p.~\pageref{ch_conservation_equations}). Each time, we will express \textbf{what is happening to the system, as a function of the fluid properties at the inlet and outlet of the control volume}. This will allow us to answer three questions:
	\begin{itemize}
		\item What is the mass flow entering and leaving the control volume?
		\item What is the force required to move the flow through the control volume?
		\item What energy transfer is required to move the flow through the control volume?
	\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Balance of mass}
\label{ch_balance_mass_int_simple}

%%%%
	\subsection{Mass balance equation}

	How much mass flow is coming in and out of the control volume? We answer this question by writing a mass balance equation. It compares the rate of change of the system’s mass (which by definition is zero, see eq.~\ref{eq_massconservation} p.~\pageref{eq_massconservation}), to the flow of mass through the borders of the control volume:
	\begin{IEEEeqnarray}{CCCCCCC}
		\timederivative{m_\sys} & = & 0 & = & \timederivative{} m_\cv  & + & \dot m_\net \label{eq_mass_oned_one}\\\nonumber\\
		\begin{array}{c}	{\scriptstyle \text{the rate of change of}} \\
							{\scriptstyle \text{the fluid’s mass}}\\
							{\scriptstyle \text{as it transits}}
		\end{array}	& = &  0	& = & 
		\begin{array}{c} 	{\scriptstyle \text{the rate of change}} \\
							{\scriptstyle \text{of mass inside}} \\
							{\scriptstyle \text{the considered volume}}
		\end{array}	& + & 
		\begin{array}{c} 	{\scriptstyle \text{the net mass flow}} \\
							{\scriptstyle \text{at the borders}} \\
							{\scriptstyle \text{of the considered volume}}
		\end{array} \nonumber
	\end{IEEEeqnarray}

	Since we are here interested only in steady flows, $\diff m_\cv / \diff t = 0$. Furthermore, we have clearly-identified inlets and outlets allowing us to re-express the net mass flow $\dot m_\net$. Equation~\ref{eq_mass_oned_one} becomes:
	\begin{IEEEeqnarray}{CCCCCCC}
		0 & = & \Sigma \dot m_\text{incoming} &+& \Sigma \dot m_\text{outgoing} \label{eq_mass_oned_two}\\\nonumber\\
		\begin{array}{c} {\scriptstyle \text{time rate of creation}} \\
						 {\scriptstyle \text{or destruction of mass}} \end{array}	& = & 
		\begin{array}{c} 	{\scriptstyle \text{the sum of}} \\
		 					{\scriptstyle \text{incoming mass flows}} \\
		 					{\scriptstyle \text{(negative terms)}} \end{array}	& + & 
		\begin{array}{c} 	{\scriptstyle \text{the sum of}} \\
							{\scriptstyle \text{outgoing mass flows}} \\
							{\scriptstyle \text{(positive terms)}} \end{array} \nonumber
	\end{IEEEeqnarray}
	\begin{equationterms}
		\item For steady flow through a fixed considered volume.
	\end{equationterms}
	
	The sign convention is counter-intuitive: mass flows are negative inwards and positive outwards. For example, in a case where there were two inlets and two outlets, we could write:
	\begin{IEEEeqnarray*}{rCCCCCCCl}
		0 & = & \dot m_\text{in 1} &+& \dot m_\text{in 2} &+& \dot m_\text{out 1} &+& \dot m_\text{out 2}\\
		0 & = & -|\dot m|_\text{in 1} &-&|\dot m|_\text{in 2} &+& |\dot m|_\text{out 1} &+& |\dot m|_\text{out 2}
	\end{IEEEeqnarray*}
	
	
	We can substitute $\dot m = \rho V_\perp A$	(eq.~\ref{eq_basic_mass_flow} p.~\pageref{eq_basic_mass_flow}) into the equations above, obtaining:
	\begin{mdframed}
		\begin{IEEEeqnarray}{rCl}
			0 & = & \Sigma \left[\rho V_\perp A \right]_\text{incoming} + \Sigma \left[\rho V_\perp A \right]_\text{outgoing} \label{eq_mass_oned}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item For steady flow through a fixed considered volume.
		\end{equationterms}
	\end{mdframed}
	
	Looking again at an example case where there were two inlets and two outlets, this equation~\ref{eq_mass_oned} would become:
	\begin{IEEEeqnarray*}{rCCCCCCCC}
		0 & = & \rho_\text{in 1} V_{\perp \text{ in 1}} A_\text{in 1} &+& \rho_\text{in 2} V_{\perp \text{ in 2}} A_\text{in 2} &+& \rho_\text{out 1} V_{\perp \text{ out 1}} A_\text{out 1} &+& \rho_\text{out 2} V_{\perp \text{ out 2}} A_\text{out 2}\nonumber\\
		0 & = & \left(\rho V_\perp A \right)_\text{in 1} &+& \left(\rho V_\perp A \right)_\text{in 2} &+& \left(\rho V_\perp A \right)_\text{out 1} &+& \left(\rho V_\perp A \right)_\text{out 2}\nonumber\\
		0 & = & -\left(\rho |V_\perp| A \right)_\text{in 1} &-& \left(\rho |V_\perp| A \right)_\text{in 2} &+& \left(\rho |V_\perp| A \right)_\text{out 1} &+& \left(\rho |V_\perp| A \right)_\text{out 2}
	\end{IEEEeqnarray*}
		\begin{equationterms}
			\item For steady flow through a fixed considered volume with two inlets and two outlets.
		\end{equationterms}

	In a simple case where there is only one inlet and one outlet, this last equation can be rewritten as
	\begin{IEEEeqnarray}{rCl}
		\left(\rho |V_\perp| A \right)_1 &=& \left(\rho |V_\perp| A \right)_2 \label{eq_rho_vee_a}
	\end{IEEEeqnarray}
		\begin{equationterms}
			\item For steady flow through a fixed considered volume with one inlet and one outlet.
		\end{equationterms}

%%%%
	\subsection{Problems with the mass balance equation}
	
	\youtubethumb{M1Dw6qU-FOU}{Dangers associated with the mass balance equation}{\oc (\ccby)}
	
	The equation~\ref{eq_rho_vee_a} above is interesting, but also treacherous. The best way to mis-use this equation is to draw the conclusion that “if $A$ decreases, then $V$ must increase”. This is only true some of the time, and here are two reasons why:
	\begin{enumerate}
		\item The density $\rho$ may change between inlet and outlet. In low-speed flows without heat transfer, $\rho$ does not vary significantly (see \S\ref{ch_classification_of_fluid_flows} p.~\ref{ch_classification_of_fluid_flows}). But in compressible flows (for example when combustion is involved, or when compressed air is expanded), $\rho$ may vary together with $A$ and~$V$. Typically, in supersonic flows (where $\ma$>1, and the fluid moves faster than the speed of sound), increases in $A$ lead to \emph{increases} in $V$, because of a decrease in $\rho$.
		\item There is no causal relationship in equation~\ref{eq_rho_vee_a}. In an incompressible flow, it may well be that reducing $A_2$ leads to an increase in $V_2$, but nothing guarantees that the product of the two remains constant. In other words, reducing $A_2$ may both increase $V_2$ \emph{and} decrease $\dot m$. Increases in velocity are not “for free”: they require force be applied and energy be spent. The mass balance equation cannot account for those phenomena.
	\end{enumerate}

	\protip{portrait21}{0.21}{0.21}{7}{Remember the title of the chapter: the tools here are for analyzing \emph{existing} flows: those for which we can, if needed, gather more information by making measurements. If you find yourself \emph{predicting} velocity in a machine you design with just a mass balance equation, then you might quickly find yourself making unrealistic assumptions. Immediately check what force and power are required to generate this velocity. For this, you need a momentum balance equation, and an energy balance equation.}

\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Balance of momentum}
\label{ch_balance_momentum_simple}

	What force is applied to the fluid for it to travel through the control volume? We answer this question by writing a momentum balance equation. It compares the rate of change of the system’s momentum (which by definition is the net force applying to it, see eq.~\ref{eq_secondlaw} p.~\pageref{eq_secondlaw}), to the flow of momentum through the borders of the control volume:
	\begin{IEEEeqnarray}{CCCCCCC}
		\timederivative{(m \vec V_\text{sys})} & = & \vec F_\net & = & \timederivative{} \left(m \vec V\right)_\cv  & + & \left(\dot m \vec V\right)_\net \nonumber\\\label{eq_linearmom_oned_one}\\
		\begin{array}{c}	{\scriptstyle \text{the rate of change of}} \\
							{\scriptstyle \text{the fluid’s momentum}}\\
							{\scriptstyle \text{as it transits}}
			\end{array}	& = & \vec F_\net & = & 
			\begin{array}{c}	{\scriptstyle \text{the rate of change}} \\
								{\scriptstyle \text{of momentum}} \\
								{\scriptstyle \text{within the considered volume}}
			\end{array}	& + & 
			\begin{array}{c} 	{\scriptstyle \text{the net flow of momentum}} \\
								{\scriptstyle \text{through the boundaries}} \\
								{\scriptstyle \text{of the considered volume}}
			\end{array} \nonumber
	\end{IEEEeqnarray}

	Since we are here interested only in steady flows, and we have clearly-identified inlets and outlets, this becomes:
	\begin{IEEEeqnarray}{CCCCCCC}
		\vec F_\net & = & \Sigma \left(\dot m \vec V\right)_\text{incoming} &+& \Sigma \left(\dot m \vec V\right)_\text{outgoing} \nonumber\\\label{eq_linearmom_oned_two}\\
		\begin{array}{c}	
							{\scriptstyle \text{the vector sum}} \\
							{\scriptstyle \text{of forces}} \\
							{\scriptstyle \text{on the fluid}}
		\end{array}&=& 
		\begin{array}{c} 	{\scriptstyle \text{the sum of incoming}} \\
		 					{\scriptstyle \text{momentum flows}} \\
							{\scriptstyle \text{(with negative } \dot m \text{ terms)}}
		\end{array}	& + & 
		\begin{array}{c} 	{\scriptstyle \text{the sum of outgoing}} \\
							{\scriptstyle \text{momentum flows}} \\
							{\scriptstyle \text{(with positive } \dot m \text{ terms)}}
		\end{array} \nonumber
	\end{IEEEeqnarray}

	The same convention as above is applied for the sign of the mass flow $\dot m$. For example, in a case where there were one inlet and one outlet, we would write:
	\begin{IEEEeqnarray*}{rCCCCCCCl}
		\vec F_\net & = & \left(\dot m \vec V\right)_\inn &+& \left(\dot m \vec V\right)_\out\\
		\vec F_\net & = & -\left(|\dot m| \vec V\right)_\inn &+& \left(|\dot m| \vec V\right)_\out
	\end{IEEEeqnarray*}
		\begin{equationterms}
			\item For steady flow through a fixed considered volume with one inlet and one outlet.
		\end{equationterms}
	
	As before, we can substitute $\dot m = \rho V_\perp A$	(eq.~\ref{eq_basic_mass_flow} p.~\pageref{eq_basic_mass_flow}) into the equations above, obtaining:
	\begin{mdframed}
		\begin{IEEEeqnarray}{rCl}
			\vec F_\text{net on fluid} & = & \Sigma \left[\rho V_\perp A \vec V\right]_\text{incoming} + \Sigma \left[\rho V_\perp A \vec V\right]_\text{outgoing}\label{eq_linearmom_oned}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item For steady flow through a fixed considered volume,
			\item where $V_\perp$ is negative inwards, positive outwards.
		\end{equationterms}
	\end{mdframed}
	
	In the example case where there is one inlet and one outlet, we would write:
	\begin{IEEEeqnarray*}{rCCCCCCCC}
		\vec F_\net & = & \left(\rho V_\perp A \vec V\right)_\inn &+& \left(\rho V_\perp A \vec V \right)_\out\\
		\vec F_\net & = & -\left(\rho |V_\perp| A \vec V \right)_\inn &+& \left(\rho |V_\perp| A \vec V \right)_\out
	\end{IEEEeqnarray*}
	
	To make clear a few things, let us focus on the simple case where a considered volume is traversed by a steady flow with mass flow $\dot m$, with one inlet (point~1) and one outlet (point~2). The net force $\vec F_\net$ applying on the fluid is
	\begin{IEEEeqnarray}{rCl}
		\vec F_\net & = & |\dot m| \left(\vec V_2 - \vec V_1 \right)\label{eq_fnet_twovectors}
	\end{IEEEeqnarray}

	\youtubetopthumb{AOcGNeY9ad0}{The net force thing in the momentum balance equation}{\oc (\ccby)}	
	Three remarks can be made about this equation. 
	First, we need to be aware that this is not one, but \emph{three} equations, one for each dimension. In order to express $\vec F_\net$, we need to calculate its three components:
		\begin{IEEEeqnarray}{rCl}
			\left\{ \begin{array}{rcl}
				F_{\net x} 	& = & |\dot m| \left(V_{2x} - V_{1x} \right)\\
				F_{\net y} 	& = & |\dot m| \left(V_{2y} - V_{1y} \right)\\
				F_{\net z} 	& = & |\dot m| \left(V_{2z} - V_{1z} \right)\\
			\end{array}\right.
		\end{IEEEeqnarray}
	
	Second, there are \emph{two} reasons why we could calculate a non-zero net force on the fluid in equation~\ref{eq_fnet_twovectors}, as illustrated in fig.~\ref{fig_two_net_forces}.
	\begin{enumerate}
		\item Even if $\vec V_2$ is aligned and in the same direction as $\vec V_1$, they can be of different magnitude. A force is required to accelerate or decelerate the fluid (more precisely, an acceleration or deceleration of the fluid is equivalent to a force);
		\item Even if $\vec V_2$ has the same magnitude as $\vec V_1$, they can have different directions. A force is required to change the direction in which a flow is flowing (or more precisely, a change of direction is equivalent to a force).
	\end{enumerate}	
	\begin{figure}
		\begin{center}
			\includegraphics[width=\textwidth]{two_net_forces}
		\end{center}
		\supercaption{Two reasons can explain why a net force $\vec F_\net$ appears in eq.~\ref{eq_fnet_twovectors}. On the left, $\vec V_2$ and $\vec V_1$ are aligned, but have different lengths. On the right $\vec V_2$ and $\vec V_1$ have the same length, but different directions. We will look at the second case in \chapterthree.}{Figure \cczero \oc}
		\label{fig_two_net_forces}
	\end{figure}
	We will explore these phenomena in greater detail in \chapterthree. In this current chapter, we are mostly interested in one-dimensional flows, and it will suffice for us to solve equation~\ref{eq_fnet_twovectors} in one suitable direction only, for example,
	\begin{IEEEeqnarray}{rCl}
		F_{\net x} 	& = & |\dot m| \left(V_{2x} - V_{1x} \right)
	\end{IEEEeqnarray}
	
	The final remark is that the equation does not describe a cause-effect relationship. The net force does not cause the change in velocity any more than the change in velocity causes the net force: they are both equivalent and simultaneous. Similarly, we have no way to know what $\vec F_\net$ is made of. The exact mechanism which adds up to a net force (pressure change, shear applied through a static wall, the movement of a turbine, \etc) is “hidden” in the control volume, and unknown to us. In order to find out what happens in the control volume, we need a different type of analysis, which we will approach in \chaptersix.

\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Balance of energy}

	What power is applied to the fluid for it to travel through the control volume? We answer this question by writing an energy balance equation. It compares the rate of change of the system’s energy, to the flow of energy through the borders of the control volume.
	
	For this we prefer to express the energy $E$ as the specific energy $e$ (in \si{\joule\per\kilogram}) multiplied by the mass $m$ (\si{\kilogram}). The time rate change of $m e$ is measured in \si{watts} ($\SI{1}{\watt} \equiv \SI{1}{\joule\per\second}$). The energy balance equation is then:
	\begin{IEEEeqnarray}{CCCCCCC}
			\timederivative{m e_\sys} &=& \Sigma \left(\dot Q + \dot W\right) &=& \timederivative{} \left(m e\right)_\cv  & + & \left(\dot m e\right)_\net \nonumber\\\label{eq_energy_oned_one}\\
		\begin{array}{c}	{\scriptstyle \text{the rate of change of}} \\
							{\scriptstyle \text{the fluid’s energy}}\\
							{\scriptstyle \text{as it transits}}
		\end{array}	& = & 
		\begin{array}{c}	{\scriptstyle \text{the sum of}} \\
							{\scriptstyle \text{powers as}} \\
							{\scriptstyle \text{heat and work}}
		\end{array}	& = & 
		\begin{array}{c}	{\scriptstyle \text{the rate of change}} \\
							{\scriptstyle \text{of energy within}} \\
							{\scriptstyle \text{the considered volume}}
		\end{array}	& + & 
		\begin{array}{c} 	{\scriptstyle \text{the net flow of energy}} \\
							{\scriptstyle \text{through the boundaries}} \\
							{\scriptstyle \text{of the considered volume}}
		\end{array} \nonumber
	\end{IEEEeqnarray}
	\begin{equationterms}
		\item where \tab $\dot Q$ 		\tab\tab is the power transferred as heat (\si{\watt})
		\item 		\tab $\dot W$	 	\tab is the power transferred  as work (\si{\watt})
	\end{equationterms}
	
	Let us examine the terms of this equation.
	
	The sum of powers as heat and work can be broken down as three components:
	\begin{itemize}
		\item the net power transferred as heat $\dot Q_\net$ (positive inwards);
		\item the net power transferred as work with moving solid surfaces $\dot W_\text{surfaces, net}$ (for example, a moving piston, turbine blade, or rotating shaft, positive inwards);
		\item and the net power transferred to and from the fluid \emph{by the fluid itself}, in order to enter and leave the considered volume. This power is called \vocab{power to cross a surface} (see \S\ref{ch_basic_flow_quantities} p.~\ref{ch_basic_flow_quantities}); for each inlet or outlet we have $\dot W_\text{pressure} = -\dot m (p / \rho)$.
	\end{itemize}
	We can thus write:
	\begin{IEEEeqnarray}{rCl}
		\Sigma \left(\dot Q + \dot W\right) &=& \dot Q_{\net} + \dot W_\text{shaft, net} + \dot W_\text{pressure}\\
											&=& \dot Q_{\net} + \dot W_\text{shaft, net} - \left(\dot m \ \frac{p}{\rho} \right)_\net
	\end{IEEEeqnarray}
	
	Turning now to the specific energy $e$, we break it down into three components (see also \S\ref{ch_energy} p.~\pageref{ch_energy}):
	\begin{itemize}
		\item the specific internal energy $i$, which represents the energy per unit mass contained as stored heat within the fluid itself. In thermodynamics, this is often noted $u$, but in fluid mechanics we reserve this symbol to note the $x$-component of velocity. In a perfect gas, $i$ is simply proportional to absolute temperature ($i = c_v T$), but for other fluids such as water, it cannot be easily measured, and precomputed tables relating $i$ to other properties must be used;
		\item the specific kinetic energy $e_k$,
			\begin{IEEEeqnarray}{rCl}
				e_k &\equiv& \frac{1}{2} V^2
			\end{IEEEeqnarray}
		\item the specific potential energy $e_p$, related to gravity $g$ (\si{\metre\per\second\squared}) and altitude $z$ (\si{\metre}) as:
			\begin{IEEEeqnarray}{rCl}
				e_p &\equiv& g z
			\end{IEEEeqnarray}
	\end{itemize}
	We thus write out specific energy $e$ (in \si{\joule\per\kilogram}) as:
	\begin{IEEEeqnarray}{rCl}
			e &\equiv& i + e_k + e_p
	\end{IEEEeqnarray}
	
	Now, we focus on steady flows (for which energy in the control volume does not change with time), and we can come back to eq.~\ref{eq_energy_oned_one} to rewrite it as:	
	\begin{IEEEeqnarray}{rCl}
		\dot Q_{\net} + \dot W_\text{shaft, net} + \dot W_\text{presure} &=& \left(\dot m \ e\right)_\net \nonumber\\
		\dot Q_{\net} + \dot W_\text{shaft, net} &=& \left(\dot m \ e\right)_\net + \left(\dot m \ \frac{p}{\rho}\right)_\net \nonumber\\
		\dot Q_{\net} + \dot W_\text{shaft, net} &=& \left[\dot m \left(i + \frac{p}{\rho} + \frac{1}{2} V^2 + g z \right) \right]_\net \label{eq_sfee_tmp}
	\end{IEEEeqnarray}
	
	Rewriting this into one general, usable form, we obtain:
	\begin{mdframed}
		\begin{IEEEeqnarray}{rCcl}
	\dot Q_{\net} + \dot W_\text{shaft, net} &=& & \Sigma \left[\dot m \left(i + \frac{p}{\rho} + \frac{1}{2} V^2 + g z \right) \right]_\inn \nonumber\\
		&& +&  \Sigma \left[\dot m \left(i + \frac{p}{\rho} + \frac{1}{2} V^2 + g z \right) \right]_\out \label{eq_sfee}
	\end{IEEEeqnarray}
		\begin{equationterms}
			\item For steady flow through a fixed considered volume,
			\item where $\dot m = \rho V_\perp A$ is negative inwards, positive outwards.
		\end{equationterms}
	\end{mdframed}
	
	This equation~\ref{eq_sfee} is known in thermodynamics as the \textit{steady flow energy equation} (in thermodynamics, it is usually expressed with the help of the concept of \vocab{enthalpy} $h \equiv i + p/\rho$, which we do not use here).
	
	As usual, let us focus on a case where there is only one inlet and one outlet. We obtain:
	\begin{IEEEeqnarray}{rCl}
		\dot Q_{\net} + \dot W_\text{shaft, net} &=& -\left[|\dot m| \left(i + \frac{p}{\rho} + \frac{1}{2} V^2 + g z \right) \right]_\inn +\left[|\dot m| \left(i + \frac{p}{\rho} + \frac{1}{2} V^2 + g z \right) \right]_\out \nonumber\\
		\dot Q_{\net} + \dot W_\text{shaft, net} &=& |\dot m| \left[ \Delta i + \Delta \frac{p}{\rho} + \Delta \left(\frac{1}{2} V^2\right) + \Delta (g z) \right]
	\end{IEEEeqnarray}
	
	This equation is very useful in principle, but not so much in practice, for two reasons:
	\begin{enumerate}
		\item It contains a lot of terms. There are five fluid properties at inlet and outlet which affect energy, and it is difficult to predict which one will be affected by a heat or work transfer. For example, consider a simple water pump with known powers $\dot Q_\inn$ and $\dot W_\text{shaft, in}$. An efficient pump will generate large increases in $p$ (or $V$ and $z$), while an inefficient pump will generate large increases in $i$ and $1/\rho$. The energy balance equation, in this form, tells us nothing about how energy input to the control volume is redistributed.
	
		\protip{portrait21}{0.23}{0.23}{7}{Again, remember the title of the chapter. To calculate the value of any one property in equation~\ref{eq_sfee}, you need to input the value of the \emph{eleven} other ones. It is tempting to take shortcuts while doing so (“oh, the pressure is probably the same”), with disastrous consequences. There is no solution to this. If you are attempting to predict fluid flow, and are missing information, better stop without a result than take hazardous attempts at using equation~\ref{eq_sfee}.}
		
		\item The terms have disproportionate values in practice. The heat capacity of ordinary fluids is very large, and so $i$ is usually hundreds of times larger than the four terms in the brackets of equation~\ref{eq_sfee}. In water for example, an increase of temperature of \SI{0,1}{\degreeCelsius} (with the term $\Delta i$) requires the same energy as increasing its velocity from \SI{30}{\kilo\metre\per\hour} to \SI{110}{\kilo\metre\per\hour} (with the term $\Delta e_k$). This is not an issue in thermodynamics, where heat, work and temperature are the most important parameters. But in fluid mechanics, velocity is of great interest, and the energy balance is not always useful to predict its changes.
		
		\protip{portrait17}{0.28}{0.25}{6}{In fluid dynamics, fluid movement usually involves relatively small amounts of energy. You can convince yourself of this by dropping milk into a bowl of water: minuscule amounts of potential energy as $\Delta (gz)$ are converted into an incredibly complex distribution of velocities, before slowly dissipating into internal energy as $\Delta i$.\\
		While \emph{in principle}, we could calculate pressure drops or velocity changes by measuring temperature differences (and thus $\Delta i$), in practice this only works when very high powers are involved, such as in a compressor or in a rocket engine nozzle. For ordinary flow (say, air flow around a car, or water flow in a pipe), the temperature changes are much too small to be measured. See exercise~\ref{exo_pipe_with_losses} p.~\pageref{exo_pipe_with_losses} for an example of this.}
	\end{enumerate}
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The Bernoulli equation}
\label{ch_bernoulli}

	\subsection{Theory}

		The Bernoulli equation is the energy equation applied to specific cases. 
		
		To derive the Bernoulli equation, we will start from equation~\ref{eq_sfee} and add five constraints:
			\begin{enumerate}
				\item Steady flow.\\
						(We had already implemented this restriction, when we set $\timederivative{(m e)_\cv}$ from eq.~\ref{eq_energy_oned_one} to zero in order to obtain eq.~\ref{eq_sfee}.)
				\item Incompressible flow.\\
						Thus, $\rho$ stays constant;
				\item No heat or work transfer.\\
						Thus, both $\dot Q_{\net}$ and $\dot W_\text{shaft, net}$ are zero;
				\item No friction.\\
						Thus, the fluid internal energy $i$ cannot increase;
				\item One-dimensional flow.\\
						Thus, our considered volume has only one inlet (labeled 1) and one outlet (labeled 2): all fluid particles move together with the same transit time, and the overall trajectory is already known.
			\end{enumerate}

		With these five restrictions, equation~\ref{eq_sfee} simply becomes:
			\begin{IEEEeqnarray*}{rCcl}
			0 + 0 &=& & \left[\dot m \left(i_\cst + \frac{p}{\rho_\cst} + \frac{1}{2} V^2 + g z \right) \right]_1 \\
			&& +&  \left[\dot m \left(i_\cst + \frac{p}{\rho_\cst} + \frac{1}{2} V^2 + g z \right) \right]_2
			\end{IEEEeqnarray*}
			Dividing by $|\dot m|$ and canceling $i_\cst$, as follows,
			\begin{IEEEeqnarray*}{rCl}
			0 &=& -\left(i_\cst + \frac{p}{\rho_\cst} + \frac{1}{2} V^2 + g z \right)_1 + \left(i_\cst + \frac{p}{\rho_\cst} + \frac{1}{2} V^2 + g z \right)_2\nonumber\\
			0 &=& -\left(\frac{p}{\rho_\cst} + \frac{1}{2} V^2 + g z \right)_1 + \left(\frac{p}{\rho_\cst} + \frac{1}{2} V^2 + g z \right)_2
			\end{IEEEeqnarray*}
			and multiplying by the (constant and uniform) density $\rho$, we obtain the \vocab{Bernoulli equation}, with all terms having dimensions of pressure:
			\begin{IEEEeqnarray}{rCl}
			\left(p + \frac{1}{2} \rho V^2 + \rho g z \right)_1 &=& \left(p + \frac{1}{2} \rho V^2 + \rho g z \right)_2 \label{eq_bernoulli}
			\end{IEEEeqnarray}
			
			This equation describes the properties of a fluid particle in a steady, incompressible, friction-less flow with no energy transfer.
		
	\subsection{Reality}

	\youtubethumb{4RBxVepjcQc}{The Bernoulli equation will kill you}{\oc (\ccby)}	

		Let us insist on the incredibly frustrating restrictions brought by the five conditions above:
		\begin{enumerate}
			\item Steady flow.\\
					This constrains us to continuous flows with no transition effects, which is a reasonable limit;
			\item Incompressible flow.\\
					We cannot use this equation to describe flow in compressors, turbines, diffusers, nozzles, nor in flows where $M > \num{0,6}$.
			\item No heat or work transfer.\\
					We cannot use this equation in a machine (\eg in pumps, turbines, combustion chambers, coolers).
			\item No friction.\\
					This is a tragic restriction! We cannot use this equation to describe a turbulent or viscous flow, \eg near a wall or in a wake.
			\item One-dimensional flow.\\
					This equation is only valid if we know precisely the trajectory of the fluid whose properties are being calculated.
		\end{enumerate}

		Of course, we can overcome those shortcomings by adding one extra (negative) term called “$\Delta p_\text{loss}$” to eq.~\ref{eq_bernoulli}, which lumps together all of the effects unaccounted for. In this way, we obtain the \vocab{Bernoulli equation with losses}:
		\begin{IEEEeqnarray}{rCl}
		\left(p + \frac{1}{2} \rho V^2 + \rho g z \right)_1 &=& \left(p + \frac{1}{2} \rho V^2 + \rho g z \right)_2 + \Delta p_\text{loss} \label{eq_bernoulli_losses}
		\end{IEEEeqnarray}

		There are indeed cases where the pressure losses due to the imperfection of the flow are well-understood, and can be easily quantified. This is true of flow in pipes, for example (we study those in \chaptersevenshort). In those cases, eq.~\ref{eq_bernoulli_losses} is extremely useful.
		
		Nevertheless, this approach is also easily misused. In a fluid flow where several of the restrictions above do not hold —and many such flows can be found in everyday life as well as engineering applications— equation~\ref{eq_bernoulli_losses} will betray its users. Convince yourself that \emph{any wrong equation} can be made correct by adding an unknown “bucket” term at the end: for example $2 + 3 = -18 + \Delta p_\text{loss}$.
		
		\protip{portrait35}{0.23}{0.23}{8}{In case you are not sure whether the Bernoulli equation applies, \emph{start from an energy balance equation}. Crossing out the terms that do not apply will force you to question their importance (\eg is heat transfer really negligible? etc.). If you do not come to a conclusive end, do not remove terms that are inconvenient. The unfortunate reality is that in fluid mechanics, the energy balance equation contains many terms, with disproportionate values, and using it alone is not enough to solve most practical problems.}
		
		\protip{portrait32}{0.24}{0.24}{8}{Among the five restrictions listed, the last is the most severe,  and the most often forgotten: \emph{the Bernoulli equation does not allow us to predict the trajectory of fluid particles}. Just like all of the other equations in this chapter, it requires a control volume with a known inlet and a known outlet. If you find yourself drawing out flow streamlines and interpreting the result with the Bernoulli equation, you are running astray. The tools you need to do this correctly are waiting for us in \chaptersix.}


\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Solved problems}

%\subsection{Force due to pressure on a plate}

\begin{youtubesolution}

	\begin{center}
		\textbf{Flow in a nozzle}
	\end{center}

	Water is flowing through a nozzle, where the diameter decreases gently from \SI{2}{\metre\squared} to \SI{1}{\metre\squared}. The flow is so smooth that energy dissipation due to wall friction is negligible.\\
	The water enters the nozzle with a uniform velocity of \SI{3}{\metre\per\second}.
	\begin{center}
	\includegraphics[width=0.6\textwidth]{simple_pipe_nozzle}
	\end{center}
	
	What is the mass flow? What is the outlet velocity? And what is the pressure change across the pipe?

	\seesolution{ZqvZTQu8SgA}

\end{youtubesolution}

\begin{youtubesolution}

	\begin{center}
		\textbf{Flow through a valve}
	\end{center}

	Water is flowing through a straight pipe with constant diameter. The mass flow entering the pipe is \SI{2}{\kilogram\per\second}, and it enters the pipe with a uniform velocity of \SI{2}{\metre\per\second}.\\
	In the middle of the pipe length, a valve is installed, which causes the pressure drop: $\Delta p_\text{valve} = \SI{-3,5}{\kilo\pascal}$.
	
	~~~~~~~~~~~\includegraphics[width=0.7\textwidth]{simple_straight_pipe_valve}
	
	What is the outlet velocity? What is the net force on the fluid as it transits? What is the power dissipated as friction?

	\seesolution{OEfMpXtkCQM}

\end{youtubesolution}


\atendofchapternotes
